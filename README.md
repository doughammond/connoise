# CONNOISE

## About

Connoise ("Connected Noise Machine") is a software modular audio synthesizer.

Get the code:

  - https://bitbucket.org/doughammond/connoise

Read the docs:

  - https://doug.lon.dev/connoise/

## Usage overview

### CLI

The configuration of the synthesizer is read from `.cno` files to form a System.

At the moment, the synthesis is run in real time, but for a limited and defined duration.

```
$ ./connoise -h
connoise version 0.1.0 2021-02-06 19:21
SPDX-License-Identifier: GPL-3.0-only
Copyright © 2020 Doug Hammond

Usage: connoise [options] filename

Positional arguments:
filename                connoise file to run

Optional arguments:
-h --help               show this help message and exit
-r --sample-rate        synthesis sample rate
-b --buffer-size        audio buffer size
-d --duration           synthesis run duration
-t --tail               synthesis tail duration
-g --dot-graph          output dot graph of the system
```

e.g.

```
$ ./connoise -r 44100 -d 5 -t 1 ./tests/data/inputs/07-pings.cno
```

### GUI

```
$ ./connoise-gui
```

There's no written user manual for the GUI, but most conventional UI operations work as expected.


### Tests

```
$ ./connoise-test
```

# .cno File Format

`connoise` implements its own text based file format for describing a synthesizer System.

The file parser is reasonably forgiving with regards to formatting known entities, you can use whitespace (including newlines) liberally (or not, if you prefer).

However, the parser will currently halt when met with unknown input. Keywords and blocks must use the exact syntax specified here. Comments are not yet supported.

Blocks are parsed in order as they are seen, therefore Modules must be instantiated before you can connect them, and before you can set the System output.

Statements which reference unknown IDs or Parameter names will be safely ignored, although your System may then be incompletely configured.

Illustrated here is the canonical formatting for known blocks.

## Strings

Text (string) values are mostly written literally, unquoted.

e.g.

```
  id my-module-id
```

Text values for Module params, are to be Quoted with double quote character ".
These `QuotedString` values may not contain any newline or " quote characters, unless 'escaped' with `\`, as per normal C string rules.

e.g.

```
  path "/this/is/my/quoted/string/path"
  metadata "This is some \"quoted\" text.\nAnd the next line."
```

Some strings permit different character sets, these are described for each string below.

## Numbers

Numbers can be written as either whole numbers (e.g. `42`) or with also a decimal point and further digits (e.g. `3.14152`). You can also use negative prefix (e.g. `-0.25`). No other formats are accepted.

Note that number ranges and/or suitability for use in a given parameter is not validated. You can for example happily specify a negative frequency and marvel that it works whilst you ponder what that really means.

## Module & Output Blocks

Describes the instantiation of a Module or System Output.

```
Module <TYPE> {
  id <ID>
  params {
    <PARAM NAME> <PARAM VALUE>
    ...
  }
}
```

```
Output <TYPE> {
  id <ID>
  params {
    <PARAM NAME> <PARAM VALUE>
    ...
  }
}
```

`params` describes the inital value of Module parameters, particularly useful if your Module does not receive those values via a connection to another Module.
Unconnected params will retain their initial value for the duration of rendering.

You need not specify values for all possible Module params, although it is a good idea to, since otherwise the Module will use its probably useless defaults.

If you do not wish to specify any initial `params`, you can omit the entire block from Module.

Every Module has and supports a `QuotedString` param called `label`, which is used by the GUI to help you identify things.

- `<TYPE>`, String `[a-zA-Z0-9_]`: The type of Module to instantiate
- `<ID>`, Number : Unique identifier of the Module in the System
- `<PARAM NAME>`, String `[a-zA-Z0-9]`: Module parameter name
- `<PARAM VALUE>`, Number or QuotedString: Module parameter initial value

## Connection

Creates a connection from one Module output port to one Module or Output input port.

You can connect a Module directly, or indirectly to itself. Don't blame me if you create an explosive feedback loop. Probably a good idea to (future: render to WAV, or) turn your headphones/speakers right down when testing new System configurations. Perhaps best to keep your System acyclic.

```
Connection {
  <ID> <OUTPUT NAME>
  <ID> <INPUT NAME>
}
```

- `<ID>`, Number: Unique identifier of a Module in the System
- `<OUTPUT NAME>`, String `[a-zA-Z0-9]`: Name of the first Module's output
- `<INPUT NAME>`, String `[a-zA-Z0-9]`: Name of the second Module's input

## System

Describes properties of the system as a whole.

```
System {
  metadata {
    <KEY> <VALUE>
  }
}
```

`metadata` describes additional information for the System; e.g. GUI layout data. This is entirely optional.

- `<KEY>`, String `[a-zA-Z0-9-]`: The name of the metadata item
- `<VALUE>`, QuotedString: Value of the metadata item


# Building

This is a standard cmake project. So far, it has only been compiled on Ubuntu Linux 20.
Other OSs or distros may or may not work.

```
$ mkdir build
$ cd build
build$ cmake ..
build$ make -j
```

## Dependencies

For build:

- cmake
- gcc or llvm
- doxygen for API documentation

For runtime:

- argparse
- boost::graph
- libsndfile
- pegtl
- SDL2
- tbb

For GUI:

- some kind of OpenGL back-end, e.g. gl3w
- imgui
- imgui bindings for: opengl3-glbinding, opengl3-glew, opengl3-gl3w, sdl2
- tinyfiledialogs
- imgui-node-editor

For test:

- catch2

I can recommend using [vcpkg](https://github.com/microsoft/vcpkg) for dependency package management.

# License

This software is licensed under GPL 3.0; See LICENSE file.

```
// SPDX-License-Identifier: GPL-3.0-only
// Copyright © 2020 Doug Hammond
```
