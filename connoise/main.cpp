/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <filesystem>
#include <iostream>
#include <sstream>

#include <SDL2/SDL.h>
#include <argparse/argparse.hpp>
#ifdef ANALYSE_GRAMMAR
#include <tao/pegtl/contrib/analyze.hpp>
#endif

#include <config.h>
#include <parser.h>
#include <system.h>
#include <transport.h>

int main(int argc, char *argv[])
{
  std::cerr << CONNOISE_NAME
            << " version " << CONNOISE_VER << " " << CONNOISE_VER_DATE
            << std::endl
            << CONNOISE_LICENSE
            << std::endl
            << CONNOISE_COPYRIGHT
            << std::endl
            << std::endl;

  argparse::ArgumentParser program(CONNOISE_NAME);

  program.add_argument("-r", "--sample-rate")
      .default_value(44100)
      .required()
      .action([](const std::string &value) { return std::stoi(value); })
      .help("synthesis sample rate");

  program.add_argument("-b", "--buffer-size")
      .default_value(4096)
      .required()
      .action([](const std::string &value) { return std::stoi(value); })
      .help("audio buffer size");

  program.add_argument("-d", "--duration")
      .default_value(10.0f)
      .required()
      .action([](const std::string &value) { return std::stof(value); })
      .help("synthesis run duration");

  program.add_argument("-g", "--dot-graph")
      .default_value(false)
      .implicit_value(true)
      .required()
      .help("output dot graph of the system");

  program.add_argument("filename")
      .help("connoise file to run");

  try
  {
    program.parse_args(argc, argv);
  }
  catch (const std::runtime_error &err)
  {
    std::cout << err.what() << std::endl;
    std::cout << program;
    exit(0);
  }

  const auto Fs = program.get<int>("-r");
  const auto bufsize = program.get<int>("-b");
  const auto dur = program.get<float>("-d");
  const auto dot = program.get<bool>("-g");
  const std::string filename = program.get<std::string>("filename");

  if (!std::filesystem::exists(filename))
  {
    std::cerr << "Input file " << filename << " doesn't exist" << std::endl;
    return 1;
  }

#ifdef ANALYSE_GRAMMAR
  const auto grammar_issues = connoise::grammar::p::analyze<connoise::grammar::grammar>();
  if (grammar_issues > 0)
  {
    std::cerr << "Grammar has issues" << std::endl;
  }
#endif

  connoise::System::shared_ptr system = std::make_shared<connoise::System>();

  // Parse in a new scope, ensure pd doens't keep a reference to System
  {
    connoise::grammar::ParserData pd{system};
    connoise::grammar::p::file_input in(filename);
    connoise::grammar::p::parse<
        connoise::grammar::grammar,
        connoise::grammar::action>(in, pd);
  }

  if (dot)
  {
    system->printDot(filename);
  }

  auto transport = std::make_shared<connoise::Transport>(system);

  const auto modules = system->getModules();
  const auto realtime = std::any_of(modules.cbegin(), modules.cend(), [](const std::pair<int, connoise::modules::Module::shared_ptr> p) -> bool {
    const auto type = p.second->type();
    return type == "MidiInput" || type == "AudioDevice";
  });

  transport->start(Fs, bufsize, realtime);
  while (transport->status()->t0 < dur)
  {
    if (realtime)
    {
      SDL_Delay(100);
    }
  }

  // Done with these now
  transport = nullptr;
  system = nullptr;

  return 0;
}
