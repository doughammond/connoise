/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <iostream>

#include <argparse/argparse.hpp>

#ifdef USE_JET_LIVE
#include <jet/live/Live.hpp>
#include <jet/live/Utility.hpp>
#endif

#include <config.h>

#include "contexts/application.h"
#include "contexts/imgui.h"
#include "contexts/sdl.h"

int main(int argc, char *argv[])
{
  std::cerr << CONNOISE_NAME
            << "-gui version " << CONNOISE_VER << " " << CONNOISE_VER_DATE
            << std::endl
            << CONNOISE_LICENSE
            << std::endl
            << CONNOISE_COPYRIGHT
            << std::endl
            << std::endl;

  argparse::ArgumentParser program(std::string(CONNOISE_NAME) + "-gui");

  program.add_argument("-s", "--scale")
      .default_value(1.0f)
      .required()
      .action([](const std::string &value) { return std::stof(value); })
      .help("GUI scale");

  try
  {
    program.parse_args(argc, argv);
  }
  catch (const std::runtime_error &err)
  {
    std::cout << err.what() << std::endl;
    std::cout << program;
    exit(0);
  }

  const auto scale = program.get<float>("-s");

  using namespace connoise::gui;

  auto sdl = std::make_shared<contexts::SDLContext>("Connoise");
  if (sdl->state() != 0)
  {
    std::cerr << "Did not initialise SDL" << std::endl;
    return -1;
  }

  auto imgui = std::make_shared<contexts::ImGuiContext>(sdl, scale);
  if (imgui->state() != 0)
  {
    std::cerr << "Did not initialise Dear Imgui" << std::endl;
    return -1;
  }

  contexts::Application::instance = std::make_shared<contexts::Application>(sdl, imgui);
  auto empty = std::make_shared<connoise::System>();
  contexts::Application::instance->system(empty);
  if (contexts::Application::instance->state() != 0)
  {
    std::cerr << "Did not initialise Application" << std::endl;
    return -1;
  }

#ifdef USE_JET_LIVE
  struct ExampleListener : public jet::ILiveListener
  {
    void onLog(jet::LogSeverity severity, const std::string &message) override
    {
      std::cout << "Live " << (int)severity << ": " << message << std::endl;
    }
    void onCodePreLoad() override
    {
      std::cout << "onCodePreload" << std::endl;
    }
    void onCodePostLoad() override
    {
      std::cout << "onCodePostload" << std::endl;
    }
  };
  auto listener = jet::make_unique<ExampleListener>();
  jet::LiveConfig config;
  config.workerThreadsCount = 6;
  auto *jl = new jet::Live(std::move(listener), config);
  while (!jl->isInitialized())
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    jl->update();
  }
  sdl->registerKeyCommand(
      {
          .sym = SDLK_r,
          .mod = KMOD_CTRL,
      },
      [jl]() {
        std::cout << "Start jet live reload" << std::endl;
        jl->tryReload();
      });
#endif

  const auto quitAction = []() {
    contexts::Application::instance->quit();
  };
  sdl->registerNamedAction("quit", quitAction);
  sdl->registerKeyCommand(
      {
          .sym = SDLK_q,
          .mod = KMOD_CTRL,
      },
      quitAction);
  sdl->registerKeyCommand(
      {
          .sym = SDLK_n,
          .mod = KMOD_CTRL,
      },
      []() {
        contexts::Application::instance->newDocument();
      });
  sdl->registerKeyCommand(
      {
          .sym = SDLK_o,
          .mod = KMOD_CTRL,
      },
      []() {
        contexts::Application::instance->openDocument();
      });
  sdl->registerKeyCommand(
      {
          .sym = SDLK_s,
          .mod = KMOD_CTRL,
      },
      []() {
        contexts::Application::instance->saveDocument(false);
      });
  sdl->registerKeyCommand(
      {
          .sym = SDLK_s,
          .mod = KMOD_CTRL | KMOD_SHIFT,
      },
      []() {
        contexts::Application::instance->saveDocument(true);
      });

#ifdef USE_JET_LIVE
  imgui->renderLoop([sdl, imgui, jl]() {
    jl->update();
#else
  imgui->renderLoop([sdl, imgui]() {
#endif
    sdl->onFrame();
    imgui->onFrame();
    contexts::Application::instance->onFrame();
  });

  contexts::Application::instance = nullptr;
  imgui = nullptr;
  sdl = nullptr;

  return 0;
}
