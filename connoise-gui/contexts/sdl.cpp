/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <iostream>

#include "sdl.h"

namespace connoise::gui::contexts
{
  SDLContext::SDLContext(const std::string &title)
      : m_state(-2)
  {
    // Setup SDL
    // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
    // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
      std::cerr << "Error: " << SDL_GetError() << std::endl;
      m_state = -1;
      return;
    }

    // Decide GL+GLSL versions
#ifdef __APPLE__
    // GL 3.2 Core + GLSL 150
    m_glslVersion = "#version 150";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
    // GL 3.0 + GLSL 130
    m_glslVersion = "#version 130";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

    // Create window with graphics context
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    m_glContext = SDL_GL_CreateContext(m_window);
    SDL_GL_MakeCurrent(m_window, m_glContext);
    SDL_GL_SetSwapInterval(1); // Enable vsync

    m_state = 0;
    std::cerr << "SDL context created" << std::endl;
  }

  SDLContext::~SDLContext()
  {
    SDL_GL_DeleteContext(m_glContext);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
    m_state = -2;
    std::cerr << "SDL context destroyed" << std::endl;
  }

  void SDLContext::onFrame()
  {
    // Poll and handle events (inputs, window resize, etc.)
    // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
    // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
    // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
    // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
      ImGui_ImplSDL2_ProcessEvent(&event);
      if (event.type == SDL_QUIT)
      {
        // std::cout << "Quit event" << std::endl;
        executeNamedAction("quit");
      }
      if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(m_window))
      {
        // std::cout << "Window close event" << std::endl;
        executeNamedAction("quit");
      }

      if (event.type == SDL_KEYDOWN)
      {
        for (auto &ka : m_keyActions)
        {
          const bool matchSym = event.key.keysym.sym == ka.key.sym;
          const bool useCtl = (KMOD_CTRL & ka.key.mod) != 0;
          const bool matchCtl = (KMOD_CTRL & event.key.keysym.mod) != 0;
          const bool useSft = (KMOD_SHIFT & ka.key.mod) != 0;
          const bool matchSft = (KMOD_SHIFT & event.key.keysym.mod) != 0;
          const bool useAlt = (KMOD_ALT & ka.key.mod) != 0;
          const bool matchAlt = (KMOD_ALT & event.key.keysym.mod) != 0;
          const bool useGui = (KMOD_GUI & ka.key.mod) != 0;
          const bool matchGui = (KMOD_GUI & event.key.keysym.mod) != 0;
          // (!a && !b) || (a && b) === !(a ^ b);
          const bool matchMod = ((!(useCtl ^ matchCtl)) &&
                                 (!(useSft ^ matchSft)) &&
                                 (!(useAlt ^ matchAlt)) &&
                                 (!(useGui ^ matchGui)));
          // std::cout << "SDL Key"
          //           << " sym=" << event.key.keysym.sym
          //           << " mod=" << event.key.keysym.mod
          //           << " : use"
          //           << " Ctl=" << useCtl
          //           << " Sft=" << useSft
          //           << " Alt=" << useAlt
          //           << " Gui=" << useGui
          //           << " : match"
          //           << " sym=" << matchSym
          //           << " Ctl=" << matchCtl
          //           << " Sft=" << matchSft
          //           << " Alt=" << matchAlt
          //           << " Gui=" << matchGui
          //           << " mod=" << matchMod
          //           << std::endl;
          if (matchSym && matchMod)
          {
            ka.action();
          }
        }
      }
    }
  }

} // namespace connoise::gui::contexts
