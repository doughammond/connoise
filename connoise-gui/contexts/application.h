/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <map>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <imgui.h>
#include <imgui-node-editor/imgui_node_editor.h>
#include <imgui_stdlib.h>

#include <parser.h>
#include <system.h>
#include <systemmodel.h>
#include <transport.h>

#include "imgui.h"
#include "sdl.h"

namespace ed = ax::NodeEditor;

namespace std
{
  /**
   * @brief Hash function for using ed::PinId as key type in std::unordered_map.
   *
   * @tparam void
   */
  template <>
  struct hash<ed::PinId>
  {
    /**
     * @brief Hash operation for ed::PinId.
     *
     * @param s
     * @return uintptr_t
     */
    uintptr_t operator()(ed::PinId const &s) const noexcept
    {
      return s.Get();
    }
  };

  /**
   * @brief Hash function for using ed::NodeId as key type in std::unordered_map.
   *
   * @tparam void
   */
  template <>
  struct hash<ed::NodeId>
  {
    /**
     * @brief Hash operation for ed::NodeId.
     *
     * @param s
     * @return uintptr_t
     */
    uintptr_t operator()(ed::NodeId const &s) const noexcept
    {
      return s.Get();
    }
  };

  /**
   * @brief Hash function for using ed::LinkId as key type in std::unordered_map.
   *
   * @tparam void
   */
  template <>
  struct hash<ed::LinkId>
  {
    /**
     * @brief Hash operation for ed::LinkId.
     *
     * @param s
     * @return uintptr_t
     */
    uintptr_t operator()(ed::LinkId const &s) const noexcept
    {
      return s.Get();
    }
  };
} // namespace std

/**
 * @brief Namespace for all GUI rendering contexts.
 *
 */
namespace connoise::gui::contexts
{
  /**
   * @brief GUI representation of a Module's port.
   *
   */
  struct ModulePort
  {
    /**
     * @brief The ID of this port.
     *
     */
    ed::PinId id;

    /**
     * @brief The name of this port.
     *
     */
    std::string name;

    /**
     * @brief The model NodeType of this port (IN or OUT).
     *
     */
    connoise::model::NodeType type;

    /**
     * @brief The underlying functional port object.
     *
     */
    connoise::modules::ModulePort::shared_ptr port;
  };

  /**
   * @brief GUI Node representation of a Module.
   *
   */
  struct ModuleNode
  {
    /**
     * @brief The ID of this GUI Node.
     *
     */
    ed::NodeId id;

    /**
     * @brief The underlying functional Module for this GUI node.
     *
     */
    connoise::modules::Module::shared_ptr m;

    /**
     * @brief The input/output ports in this Module/Node.
     *
     */
    std::vector<ModulePort> ports;

    /**
     * @brief Whether this Module's inspector params should be open.
     *
     */
    bool inspect;
  };

  /**
   * @brief GUI link representation of a System connection.
   *
   */
  struct ModuleLink
  {
    /**
     * @brief The ID of this link.
     *
     */
    ed::LinkId id;

    /**
     * @brief The input port pin ID of this link.
     *
     */
    ed::PinId inputId;

    /**
     * @brief The output port pin ID of this link.
     *
     */
    ed::PinId outputId;

    connoise::modules::ModulePort::shared_ptr in;

    connoise::modules::ModulePort::shared_ptr out;
  };

  /**
   * @brief GUI item label type.
   *
   */
  enum LabelType
  {
    Text,
    Icon
  };

  /**
   * @brief Information for constructing a new node.
   *
   */
  struct NewNodeInfo
  {
    /**
     * @brief Whether to open the new node popup.
     *
     */
    bool open;

    /**
     * @brief The pin ID for future new node to connect to.
     *
     */
    ed::PinId pinId;

    /**
     * @brief The kind of pin for the future new node to connect to.
     *
     */
    ed::PinKind pinKind;
  };

  /**
   * @brief Callback function for a Prompt choice.
   *
   */
  using PromptCallback = std::function<bool()>;

  /**
   * @brief User-prompt UI.
   *
   */
  struct Prompt
  {
    /**
     * @brief Text to display to the user for this prompt.
     *
     */
    std::string text;

    /**
     * @brief Map of user-choices for this prompt.
     *
     */
    std::unordered_map<std::string, PromptCallback> choices;
  };

  /**
   * @brief Application GUI renderer implementation.
   *
   */
  class Application
  {
  public:
    /**
     * @brief Shared pointer type for this context.
     *
     */
    using shared_ptr = std::shared_ptr<Application>;

    /**
     * @brief Construct a new Application object.
     *
     * @param sdl
     * @param imgui
     */
    Application(
        SDLContext::shared_ptr sdl,
        ImGuiContext::shared_ptr imgui);

    /**
     * @brief Destroy the Application object.
     *
     */
    ~Application();

    /**
     * @brief Getter for the context initialisation state.
     *
     * @return const int
     */
    const int state() const
    {
      return m_state;
    }

    /**
     * @brief Load a System from a cno file.
     *
     * @param cno
     */
    void loadCnoFile(const std::string &cno);

    /**
     * @brief Setter for currently active System.
     *
     * @param next
     */
    void system(connoise::System::shared_ptr next);

    /**
     * @brief Getter for the currently active System.
     *
     * @return connoise::System::shared_ptr
     */
    connoise::System::shared_ptr system() const
    {
      return m_system;
    }

    /**
     * @brief To be called whenever we know that m_system has changed or been replaced.
     *
     */
    void onSystemChanged();

    /**
     * @brief Context onFrame handler; called for every rendering frame.
     *
     */
    void onFrame();

    /**
     * @brief Start a new document.
     *
     */
    void newDocument();

    /**
     * @brief Open a cno file as document.
     *
     */
    void openDocument();

    /**
     * @brief Save the current document as cno file.
     *
     * @param asNew whether to save over the last opened file, or new file
     */
    bool saveDocument(const bool asNew);

    /**
     * @brief Quit the application.
     *
     */
    void quit();

    /**
     * @brief Getter for the document saved state.
     *
     * @return true
     * @return false
     */
    const bool documentSaved() const
    {
      return m_documentSaved;
    }

    /**
     * @brief Setter for the document saved state.
     *
     * @param saved
     */
    void documentSaved(const bool saved)
    {
      // std::cout << "Application::documentSaved = " << saved << std::endl;
      m_documentSaved = saved;
    }

  public:
    /**
     * @brief Singleton global app context.
     *
     * This is only here because imgui_node_editor cannot use bound functions
     * for its state load/save functions :(
     *
     */
    static Application::shared_ptr instance;

  private:
    /**
     * @brief Configure the current node editor context.
     *
     */
    void configureEditor();

    /**
     * @brief Un-configure the current node editor context.
     *
     */
    void destroyEditor();

    /**
     * @brief Prompt the user for action if the current document is not saved.
     *
     * Callback will be called if the document is successfully saved.
     * Callback will not be called if the user dismisses the prompt or the save dialog.
     *
     * @param callback
     */
    void promptIfUnsaved(PromptCallback callback);

    /**
     * @brief Render the main application menu.
     *
     */
    void render_MainMenu();

    /**
     * @brief Render the main application command Palette.
     *
     * @return true if the system was changed
     */
    bool render_Palette();

    /**
     * @brief Render the view controls in the Palette.
     *
     */
    void render_PaletteView();

    /**
     * @brief Render the Transport controls in the Palette.
     *
     */
    void render_PalettePlay();

    /**
     * @brief Render all nodes/modules.
     *
     */
    void render_Nodes();

    /**
     * @brief Render all links/connections.
     *
     */
    void render_Links();

    /**
     * @brief Render the Inspector panel.
     *
     * @return true if the system was changed
     */
    bool render_Inspector();

    /**
     * @brief Render the status panel.
     *
     */
    void render_Status();

    /**
     * @brief Render the list of known Modules, as buttons. Create the Module when clicked.
     *
     * @param ltype
     * @return true when a Module was created from the list
     * @return false when a Module was not created
     */
    bool render_NewNodeList(const LabelType ltype);

    /**
     * @brief Render a graphical icon for button to create a new Node.
     *
     * @param type
     * @param tl
     * @param br
     * @param size
     * @param scale
     */
    void render_NewNodeIcon(const std::string &type, const ImVec2 &tl, const ImVec2 &br, const float &size, const float &scale);

    /**
     * @brief Detect whether item deletion was requested.
     *
     * @return true if the system was changed
     */
    bool handle_deleteItems();

    /**
     * @brief Detect whether to create a new link/connection.
     *
     * @return true if the system was changed
     */
    bool handle_createLink();

    /**
     * @brief Conditional: Render the new node popup; if user interacted with a pin/port.
     *
     * @param info
     * @param ltype
     *
     * @return true if the system was changed
     */
    bool render_NewNodePopup(const NewNodeInfo &info, const LabelType ltype);

    /**
     * @brief Conditional: Render a user prompt; if m_prompt contains choices.
     *
     */
    void render_Prompt();

    // Internal state

    /**
     * @brief This context's initialisation state.
     *
     */
    int m_state;

    /**
     * @brief The SDL context.
     *
     */
    SDLContext::shared_ptr m_sdl;

    /**
     * @brief The Dear ImGui context.
     *
     */
    ImGuiContext::shared_ptr m_imgui;

    /**
     * @brief The currently active System.
     *
     */
    connoise::System::shared_ptr m_system;

    /**
     * @brief The currently active System Transport.
     *
     */
    connoise::Transport::shared_ptr m_transport;

    /**
     * @brief The Node editor context.
     *
     */
    ed::EditorContext *m_edContext;

    /**
     * @brief Whether the inspector panel is open.
     *
     */
    bool m_inspector;

    /**
     * @brief Ever-increasing ID value for nodes/links/ports because these items don't have static IDs in the System
     *
     */
    size_t m_elementId;

    /**
     * @brief Map of known ModulePorts.
     *
     */
    std::unordered_map<ed::PinId, ModulePort> m_ports;

    /**
     * @brief Map of known Modules (GUI Nodes).
     *
     */
    std::unordered_map<ed::NodeId, ModuleNode> m_nodes;

    /**
     * @brief Map of known Module connections (GUI Links).
     *
     */
    std::unordered_map<ed::LinkId, ModuleLink> m_links;

    /**
     * @brief Set of Pin IDs which have links/connetions.
     *
     */
    std::unordered_set<ed::PinId> m_linkedPinIds;

    /**
     * @brief The currently open file name. New/unsaved docs have an empty name.
     *
     */
    std::string m_openfilename;

    /**
     * @brief Whether the current document is saved or not.
     *
     */
    bool m_documentSaved;

    /**
     * @brief User prompt information.
     *
     */
    Prompt m_prompt;

    /**
     * @brief The rendering sample rate
     *
     */
    int m_Fs;

    /**
     * @brief The rendering buffer size
     *
     */
    int m_bufsize;
  };

} // namespace connoise::gui::contexts
