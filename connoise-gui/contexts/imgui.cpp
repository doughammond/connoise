/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <iostream>

#include "imgui.h"

namespace connoise::gui::contexts
{
  ImGuiContext::ImGuiContext(SDLContext::shared_ptr sdl, const float dpiScale)
      : m_state(-2),
        m_dpiScale(dpiScale),
        m_sdl(sdl)
  {
    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2)
    // glad2 recommend using the windowing library loader instead of the (optionally) bundled one.
    bool err = gladLoadGL((GLADloadfunc)SDL_GL_GetProcAddress) == 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
    bool err = false;
    glbinding::Binding::initialize();
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
    bool err = false;
    glbinding::initialize([](const char *name) { return (glbinding::ProcAddress)SDL_GL_GetProcAddress(name); });
#else
    // ... something custom is not implemented ...
    bool err = false;
#endif
    if (err)
    {
      fprintf(stderr, "Failed to initialize OpenGL loader!\n");
      m_state = -1;
      return;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForOpenGL(m_sdl->window(), m_sdl->glContext());
    ImGui_ImplOpenGL3_Init(m_sdl->glslVersion().c_str());

    setupIO(dpiScale);
    setupFonts(dpiScale);
    setupStyle(dpiScale);

    m_state = 0;
    std::cerr << "Dear Imgui context created" << std::endl;
  }

  ImGuiContext::~ImGuiContext()
  {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    m_state = -2;
    std::cerr << "Dear Imgui context destroyed" << std::endl;
  }

  void ImGuiContext::onFrame()
  {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(m_sdl->window());
    ImGui::NewFrame();
  }

  void ImGuiContext::renderLoop(RenderLoopFn fn)
  {
    const ImVec4 bg = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    m_done = false;
    while (!m_done)
    {
      fn();

      ImGui::Render();
      glViewport(0, 0, (int)m_io->DisplaySize.x, (int)m_io->DisplaySize.y);
      glClearColor(bg.x, bg.y, bg.z, bg.w);
      glClear(GL_COLOR_BUFFER_BIT);
      ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
      SDL_GL_SwapWindow(m_sdl->window());
    }
  }

  void ImGuiContext::setupIO(const float dpiScale)
  {
    m_io = &ImGui::GetIO();
    m_io->DisplayFramebufferScale = ImVec2(dpiScale, dpiScale);
    m_io->IniFilename = NULL;                                // do not persist UI state
    m_io->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
  }

  void ImGuiContext::setupFonts(const float dpiScale)
  {
    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'docs/FONTS.md' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    ImFontConfig fntCfg;
    fntCfg.SizePixels = 16.f;
    m_io->FontGlobalScale = dpiScale;
    fntCfg.OversampleH = dpiScale;
    fntCfg.OversampleV = dpiScale;
    m_io->Fonts->AddFontDefault(&fntCfg);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);
  }

  void ImGuiContext::setupStyle(const float dpiScale)
  {
    ImGuiStyle *style = &ImGui::GetStyle();
    ImGui::StyleColorsDark(style);
    style->ScaleAllSizes(dpiScale);

    ImVec4 *colors = style->Colors;
    colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
    colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
    colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.06f, 0.06f, 0.94f);
    colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
    colors[ImGuiCol_Border] = ImVec4(0.50f, 0.46f, 0.43f, 0.50f);
    colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_FrameBg] = ImVec4(0.48f, 0.27f, 0.16f, 0.54f);
    colors[ImGuiCol_FrameBgHovered] = ImVec4(0.98f, 0.52f, 0.26f, 0.40f);
    colors[ImGuiCol_FrameBgActive] = ImVec4(0.98f, 0.52f, 0.26f, 0.67f);
    colors[ImGuiCol_TitleBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
    colors[ImGuiCol_TitleBgActive] = ImVec4(0.48f, 0.27f, 0.16f, 1.00f);
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
    colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
    colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
    colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
    colors[ImGuiCol_CheckMark] = ImVec4(0.98f, 0.52f, 0.26f, 1.00f);
    colors[ImGuiCol_SliderGrab] = ImVec4(0.88f, 0.47f, 0.24f, 1.00f);
    colors[ImGuiCol_SliderGrabActive] = ImVec4(0.98f, 0.52f, 0.26f, 1.00f);
    colors[ImGuiCol_Button] = ImVec4(0.98f, 0.52f, 0.26f, 0.40f);
    colors[ImGuiCol_ButtonHovered] = ImVec4(0.98f, 0.52f, 0.26f, 1.00f);
    colors[ImGuiCol_ButtonActive] = ImVec4(0.98f, 0.39f, 0.06f, 1.00f);
    colors[ImGuiCol_Header] = ImVec4(0.98f, 0.52f, 0.26f, 0.31f);
    colors[ImGuiCol_HeaderHovered] = ImVec4(0.98f, 0.52f, 0.26f, 0.80f);
    colors[ImGuiCol_HeaderActive] = ImVec4(0.98f, 0.52f, 0.26f, 1.00f);
    colors[ImGuiCol_Separator] = ImVec4(0.50f, 0.46f, 0.43f, 0.50f);
    colors[ImGuiCol_SeparatorHovered] = ImVec4(0.75f, 0.33f, 0.10f, 0.78f);
    colors[ImGuiCol_SeparatorActive] = ImVec4(0.75f, 0.33f, 0.10f, 1.00f);
    colors[ImGuiCol_ResizeGrip] = ImVec4(0.98f, 0.52f, 0.26f, 0.25f);
    colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.98f, 0.52f, 0.26f, 0.67f);
    colors[ImGuiCol_ResizeGripActive] = ImVec4(0.98f, 0.52f, 0.26f, 0.95f);
    colors[ImGuiCol_Tab] = ImVec4(0.58f, 0.32f, 0.18f, 0.86f);
    colors[ImGuiCol_TabHovered] = ImVec4(0.98f, 0.52f, 0.26f, 0.80f);
    colors[ImGuiCol_TabActive] = ImVec4(0.68f, 0.37f, 0.20f, 1.00f);
    colors[ImGuiCol_TabUnfocused] = ImVec4(0.15f, 0.10f, 0.07f, 0.97f);
    colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.42f, 0.24f, 0.14f, 1.00f);
    colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
    colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
    colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
    colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
    colors[ImGuiCol_TextSelectedBg] = ImVec4(0.98f, 0.52f, 0.26f, 0.35f);
    colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
    colors[ImGuiCol_NavHighlight] = ImVec4(0.98f, 0.52f, 0.26f, 1.00f);
    colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
    colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
    colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
  }

} // namespace connoise::gui::contexts
