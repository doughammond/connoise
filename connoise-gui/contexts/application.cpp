/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <fstream>
#include <iostream>
#include <thread>

#include <tbb/task_arena.h>
#include <tinyfiledialogs/tinyfiledialogs.h>

#include "application.h"

namespace colours
{
  namespace vec
  {
    const ImVec4 green(0.1f, 0.8f, 0.1f, 1.0f);
    const ImVec4 grey(0.5f, 0.5f, 0.5f, 1.0f);
    const ImVec4 red(0.8f, 0.1f, 0.1f, 1.0f);
    const ImVec4 orange(0.9f, 0.45f, 0.35f, 1.0f);
    const ImVec4 white(1.0, 1.0, 1.0, 1.0);

    ImVec4 fromLevel(const connoise::Value &level)
    {
      return ImVec4(0.75f * level, 1.0 - level, 0.35f, 1.0f);
    }
  }
  namespace u32
  {
    // ImU32 as colour is in 0xAABBGGRR order
    const ImU32 white = 0xFFFFFFFF;
    const ImU32 green = 0xFF11CC11;
    const ImU32 grey = 0xFF888888;
    const ImU32 orange = 0xFFE57259;

    ImU32 fromLevel(const connoise::Value &level)
    {
      return ImGui::GetColorU32(ImVec4(0.75f * level, 1.0 - level, 0.35f, 1.0f));
    }
  }
} // namespace colours

namespace connoise::gui::contexts
{
  Application::shared_ptr Application::instance;

  /**
   * @brief Handler for loading node editor context settings from the System metadata.
   *
   * @param data
   * @param userPointer
   * @return size_t
   */
  size_t LoadSystemSettings(char *data, void *userPointer)
  {
    if (!Application::instance)
    {
      return 0;
    }
    auto settings = Application::instance->system()->metadata("gui");
    // std::cout << "LoadSystemSettings with " << settings << std::endl;

    // This function will be called _twice_ per load;
    // first time with data == nullptr; so that we can return the string size
    // second time with an allocated char* data of the correct length;
    // therefore we can only copy to data if a valid pointer is provided.
    if (data != nullptr)
    {
      memcpy(data, settings.data(), settings.size());
    }
    return settings.size();
  }

  /**
   * @brief Handler for saving the node editor settings into the System metadata.
   *
   * @param data
   * @param size
   * @param reason
   * @param userPointer
   * @return true
   * @return false
   */
  bool SaveSystemSettings(const char *data, size_t size, ed::SaveReasonFlags reason, void *userPointer)
  {
    if (!Application::instance)
    {
      return false;
    }
    // std::cout << "SaveSettings because " << (int)reason << " with data " << data << std::endl;
    const std::string json(data);
    Application::instance->system()->metadata("gui", json);

    return true;
  }

  Application::Application(
      SDLContext::shared_ptr sdl,
      ImGuiContext::shared_ptr imgui)
      : m_state(-2),
        m_sdl(sdl),
        m_imgui(imgui),
        m_edContext(nullptr),
        m_inspector(false),
        m_elementId(1000000), // arbitrary number larger than the max likely number of modules in the system :/
        m_openfilename(""),
        m_documentSaved(true),
        m_Fs(48000),
        m_bufsize(2048)
  {
    m_state = 0;
    std::cerr << "Application context created "
              << std::endl;
  }

  Application::~Application()
  {
    destroyEditor();
    std::cerr << "Application context destroyed" << std::endl;
  }

  void Application::loadCnoFile(const std::string &cno)
  {
    connoise::System::shared_ptr next = std::make_shared<connoise::System>();
    connoise::grammar::ParserData pd{next};

    connoise::grammar::p::file_input in(cno);
    connoise::grammar::p::parse<
        connoise::grammar::grammar,
        connoise::grammar::action>(in, pd);

    system(next);
  }

  void Application::system(connoise::System::shared_ptr next)
  {
    destroyEditor();
    m_system.swap(next);
    m_transport = std::make_shared<Transport>(m_system);
    onSystemChanged();
    configureEditor();
  }

  /**
   * @brief Note that this function invalidates all node references!
   *
   * Avoid doing something like
   * for (auto &n : m_nodes) { ... onSystemChanged(); n.access ... }
   *
   */
  void Application::onSystemChanged()
  {
    std::unordered_map<ed::PinId, ModulePort> nextPorts;
    std::unordered_map<ed::NodeId, ModuleNode> nextNodes;
    std::unordered_map<ed::LinkId, ModuleLink> nextLinks;
    std::unordered_set<ed::PinId> nextLinkedPinIds;

    m_transport->onSystemChanged();

    const auto modules = m_system->getModules();
    const bool isEmpty = modules.size() == 0;
    documentSaved(isEmpty);

    for (const auto &[mid, m] : modules)
    {
      ModuleNode n;
      n.id = mid; // stable IDs for modules
      n.m = m;
      // std::cout << "Application::onSystemChanged create node ID " << n.id.Get() << " for module " << m->name() << std::endl;
      for (const auto i : m->getInputs())
      {
        ModulePort p;
        p.id = m_elementId++;
        p.name = i;
        p.type = connoise::model::NodeType::IN_PORT;
        p.port = m->getInput(i);
        n.ports.push_back(p);
        nextPorts[p.id] = p;
      }
      for (const auto i : m->getOutputs())
      {
        ModulePort p;
        p.id = m_elementId++;
        p.name = i;
        p.type = connoise::model::NodeType::OUT_PORT;
        p.port = m->getOutput(i);
        n.ports.push_back(p);
        nextPorts[p.id] = p;
      }
      nextNodes[n.id] = n;
    }

    for (const auto &cx : m_system->getAllConnections())
    {
      ModuleLink k;
      k.id = m_elementId++;
      for (const auto &[id, p] : nextPorts)
      {
        if (p.port == cx.in)
        {
          k.in = p.port;
          k.inputId = id;
          nextLinkedPinIds.insert(id);
        }
        if (p.port == cx.out)
        {
          k.out = p.port;
          k.outputId = id;
          nextLinkedPinIds.insert(id);
        }
      }
      if (k.inputId && k.outputId)
      {
        nextLinks[k.id] = k;
      }
    }

    m_ports.swap(nextPorts);
    m_nodes.swap(nextNodes);
    m_links.swap(nextLinks);
    m_linkedPinIds.swap(nextLinkedPinIds);
  }

  void Application::configureEditor()
  {
    ed::Config cfg;
    cfg.SettingsFile = NULL;
    cfg.BeginSaveSession = [](void *userPointer)
    {
      // std::cout << "BeginSaveSession" << std::endl;
    };
    cfg.EndSaveSession = [](void *userPointer)
    {
      // std::cout << "EndSaveSession" << std::endl;
    };
    cfg.LoadSettings = LoadSystemSettings;
    cfg.SaveSettings = SaveSystemSettings;
    cfg.LoadNodeSettings = [](ed::NodeId nodeId, char *data, void *userPointer) -> size_t
    {
      // std::cout << "LoadNodeSettings for " << nodeId.Get() << std::endl;
      return 0;
    };
    cfg.SaveNodeSettings = [](ed::NodeId nodeId, const char *data, size_t size, ed::SaveReasonFlags reason, void *userPointer) -> bool
    {
      // std::cout << "SaveNodeSettings becuuse " << (int)reason << " for " << nodeId.Get() << " with data " << data << std::endl;
      return false;
    };
    m_edContext = ed::CreateEditor(&cfg);
    ed::SetCurrentEditor(m_edContext);
  }

  void Application::destroyEditor()
  {
    if (m_edContext)
    {
      ed::DestroyEditor(m_edContext);
      m_edContext = nullptr;
    }
  }

  /**
   * @brief Flags for creating a fixed position, fixed size window with no title.
   *
   */
  const ImGuiWindowFlags immovableWindow = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;

  void Application::onFrame()
  {
    bool sysChange = false;

    const auto dpiScale = m_imgui->dpiScale();

    const int menuHeight = 20 * dpiScale;
    render_MainMenu();

    const int paletteWidth = 100 * dpiScale;
    const auto ws = m_sdl->windowSize();

    const int statusHeight = 32 * dpiScale;

    const int workspaceHeight = std::max(ws[1] - menuHeight - statusHeight, 0);

    ImVec2 origin(0, menuHeight);
    ImGui::SetNextWindowPos(origin);
    ImVec2 paletteSize(paletteWidth, workspaceHeight);
    ImGui::SetNextWindowSize(paletteSize);
    sysChange |= render_Palette();

    const int inspectorWidth = m_inspector ? 400 * dpiScale : 0;

    const int editorWidth = std::max(ws[0] - paletteWidth - inspectorWidth, 0);
    ImVec2 nodeEditorPos(paletteWidth, menuHeight);
    ImGui::SetNextWindowPos(nodeEditorPos);
    ImVec2 nodeEditorSize(editorWidth, workspaceHeight);
    ImGui::SetNextWindowSize(nodeEditorSize);
    ImGui::Begin("EditorWindow", NULL, immovableWindow);
    if (m_edContext)
    {
      ed::Begin("NodeEditor");
      {
        render_Nodes();
        render_Links();
        sysChange |= handle_deleteItems();
        sysChange |= handle_createLink();
      }
      ed::End();
    }
    ImGui::End();

    ImVec2 inspectorPos(std::max(ws[0] - inspectorWidth, 0), menuHeight);
    ImGui::SetNextWindowPos(inspectorPos);
    ImVec2 inspectorSize(inspectorWidth, workspaceHeight);
    ImGui::SetNextWindowSize(inspectorSize);
    sysChange |= render_Inspector();

    ImVec2 statusPos(0, ws[1] - statusHeight);
    ImGui::SetNextWindowPos(statusPos);
    ImVec2 statusSize(ws[0], statusHeight);
    ImGui::SetNextWindowSize(statusSize);
    render_Status();

    render_Prompt();

    if (sysChange)
    {
      std::cout << "Application::onFrame sysChange" << std::endl;
      onSystemChanged();
    }
  }

  void Application::newDocument()
  {
    promptIfUnsaved([this]()
                    {
      auto next = std::make_shared<connoise::System>();
      system(next);
      m_openfilename = "";
      return true; });
  }

  void Application::openDocument()
  {
    promptIfUnsaved([this]()
                    {
      char const *filterPatterns[1] = {"*.cno"};
      const auto next = tinyfd_openFileDialog("Open", "", 1, filterPatterns, "", false);
      if (next != NULL)
      {
        loadCnoFile(next);
        m_openfilename = next;

        // system changed but the user hasn't made changes yet
        documentSaved(true);
        return true;
      }

      return false; });
  }

  bool Application::saveDocument(const bool asNew)
  {
    bool didSave = true;
    std::string next = "";
    std::string title = "Save";
    if (asNew)
    {
      next = "";
      title += " As";
    }
    else
    {
      next = m_openfilename;
    }
    // std::cout << "Application::saveDocument"
    //           << " asNew=" << asNew
    //           << " m_openfilename=" << m_openfilename
    //           << " next=" << next
    //           << " title=" << title
    //           << std::endl;
    if (!next.size())
    {
      char const *filterPatterns[1] = {"*.cno"};
      const auto val = tinyfd_saveFileDialog(title.c_str(), "", 1, filterPatterns, "");
      if (val != NULL)
      {
        next = std::string(val);
      }
      else
      {
        next = "";
      }
    }
    if (next.size())
    {
      std::cout << "Save the file at " << next << std::endl;
      std::ofstream f(next);
      if (f.is_open())
      {
        f << m_system;
        f.close();
      }
      else
      {
        std::cerr << "Could not open " << next << " for write" << std::endl;
      }
      m_openfilename = next;
      didSave = true;
    }
    else
    {
      didSave = false;
    }
    return didSave;
  }

  void Application::quit()
  {
    promptIfUnsaved([this]()
                    {
      m_imgui->quit();
      return true; });
  }

  void Application::promptIfUnsaved(PromptCallback callback)
  {
    if (!m_documentSaved)
    {
      m_prompt.text = "Current document is unsaved";
      m_prompt.choices.clear();
      m_prompt.choices["Cancel"] = []()
      {
        return true;
      };
      m_prompt.choices["Discard"] = callback;
      m_prompt.choices["Save"] = [this, callback]()
      {
        if (saveDocument(false))
        {
          return callback();
        }
        else
        {
          return false;
        }
      };
    }
    else
    {
      callback();
    }
  }

  void Application::render_MainMenu()
  {
    ImGui::BeginMainMenuBar();
    if (ImGui::BeginMenu("File"))
    {
      if (ImGui::MenuItem("New", "c-n"))
      {
        newDocument();
      }

      ImGui::Separator();

      if (ImGui::MenuItem("Open", "c-o"))
      {
        openDocument();
      }

      ImGui::Separator();

      if (ImGui::MenuItem("Save", "c-s"))
      {
        saveDocument(false);
      }
      if (ImGui::MenuItem("Save As ", "cs-s"))
      {
        saveDocument(true);
      }

      ImGui::Separator();

      if (ImGui::MenuItem("Quit", "c-q"))
      {
        m_imgui->quit();
      }
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }

  bool Application::render_Palette()
  {
    bool changed = false;
    ImGui::Begin("EditorPalette", NULL, immovableWindow);
    {
      render_PaletteView();

      ImGui::Separator();
      render_PalettePlay();

      ImGui::Separator();
      changed = render_NewNodeList(LabelType::Icon);
    }
    ImGui::End();

    return changed;
  }

  void Application::render_PaletteView()
  {
    const auto viewAll = ImGui::Button("[*]");
    if (ImGui::IsItemHovered())
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("View all");
      ImGui::EndTooltip();
    }
    if (viewAll)
    {
      ed::NavigateToContent();
    }
    ImGui::SameLine();
    const auto viewSelection = ImGui::Button("[#]");
    if (ImGui::IsItemHovered())
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("View selection");
      ImGui::EndTooltip();
    }
    if (viewSelection)
    {
      ed::NavigateToSelection(true);
    }

    ImGui::Separator();
    ImGui::Checkbox("I", &m_inspector);
    if (ImGui::IsItemHovered())
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("Show inspector");
      ImGui::EndTooltip();
    }
  }

  void Application::render_PalettePlay()
  {
    std::vector<int> bss{
        128,
        256,
        512,
        1024,
        2048,
        4096,
        8192,
        16384,
        32768,
    };
    std::vector<int> fss{
        22050,
        44100,
        48000,
        96000,
        192000,
    };

    const auto next = m_transport->nextActions();
    const auto canPlay = next.count("start") > 0;
    const auto canStop = next.count("stop") > 0;
    const auto canPause = next.count("pause") > 0;
    const auto canResume = next.count("resume") > 0;

    const auto play = ImGui::ArrowButton("play", ImGuiDir_Right);
    const auto playHover = ImGui::IsItemHovered();
    if (playHover && canPlay)
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("Play!");
      ImGui::EndTooltip();
    }
    ImGui::SameLine();
    const auto stop = ImGui::Button("[]");
    const auto stopHover = ImGui::IsItemHovered();
    if (stopHover && canStop)
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("Stop!");
      ImGui::EndTooltip();
    }
    ImGui::SameLine();
    const auto pauseResume = ImGui::Button("||");
    const auto pauseResumeHover = ImGui::IsItemHovered();
    if (pauseResumeHover && canPause)
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("Pause!");
      ImGui::EndTooltip();
    }
    if (pauseResumeHover && canResume)
    {
      ImGui::BeginTooltip();
      ImGui::TextUnformatted("Resume!");
      ImGui::EndTooltip();
    }

    ImVec2 comboSize(120, 0);

    ImGui::PushID("BS");
    ImGui::SetNextItemWidth(comboSize[0]);
    if (ImGui::BeginCombo("", std::to_string(m_bufsize).c_str()))
    {
      if (ImGui::IsItemHovered())
      {
        ImGui::BeginTooltip();
        ImGui::TextUnformatted("Rendering buffer size");
        ImGui::EndTooltip();
      }

      for (const auto &bs : bss)
      {
        if (ImGui::Selectable(std::to_string(bs).c_str(), m_bufsize == bs, 0, comboSize))
        {
          m_bufsize = bs;
        }
      }
      ImGui::EndCombo();
    }
    ImGui::PopID();

    ImGui::PushID("FS");
    ImGui::SetNextItemWidth(comboSize[0]);
    if (ImGui::BeginCombo("", std::to_string(m_Fs).c_str()))
    {
      if (ImGui::IsItemHovered())
      {
        ImGui::BeginTooltip();
        ImGui::TextUnformatted("Rendering sample rate");
        ImGui::EndTooltip();
      }

      for (const auto &fs : fss)
      {
        if (ImGui::Selectable(std::to_string(fs).c_str(), m_Fs == fs, 0, comboSize))
        {
          m_Fs = fs;
        }
      }
      ImGui::EndCombo();
    }
    ImGui::PopID();

    if (canPlay && play)
    {
      m_transport->start(m_Fs, m_bufsize, true);
    }
    if (canStop && stop)
    {
      m_transport->stop();
    }
    if (canPause && pauseResume)
    {
      m_transport->pause();
    }
    if (canResume && pauseResume)
    {
      m_transport->resume();
    }
  }

  void Application::render_Nodes()
  {
    const float portCircleSize = 5.5 + m_imgui->dpiScale();

    for (const auto &[id, n] : m_nodes)
    {
      auto m = n.m;
      ed::BeginNode(id);
      {
        ImGui::TextColored(colours::vec::orange, "%s", m->type().c_str());
        ImGui::TextColored(colours::vec::green, "%s", m->stringParam("label").c_str());

        ImGui::BeginGroup();
        for (auto &p : n.ports)
        {
          if (p.type == connoise::model::NodeType::IN_PORT)
          {
            ImGui::Text("%-8s", p.name.c_str());
            const auto tl = ImGui::GetItemRectMin();
            const auto br = ImGui::GetItemRectMax();
            ImVec2 pp(tl[0] - 8, (tl[1] + br[1]) / 2);
            ed::BeginPin(p.id, ed::PinKind::Input);
            {
              if (m_transport->status()->state == TransportState::PLAYING)
              {
                ImGui::GetWindowDrawList()->AddCircleFilled(pp, portCircleSize, colours::u32::fromLevel(p.port->rms));
              }
              else if (m_linkedPinIds.count(p.id) > 0)
              {
                ImGui::GetWindowDrawList()->AddCircleFilled(pp, portCircleSize, colours::u32::green);
              }
              else
              {
                ImGui::GetWindowDrawList()->AddCircleFilled(pp, portCircleSize, colours::u32::grey);
              }
              ed::PinPivotRect(pp, pp);
              ed::PinRect(tl, br);
            }
            ed::EndPin();
          }
        }
        ImGui::EndGroup();
        ImGui::SameLine();
        ImGui::BeginGroup();
        for (auto &p : n.ports)
        {
          if (p.type == connoise::model::NodeType::OUT_PORT)
          {
            ImGui::Text("%8s", p.name.c_str());
            const auto tl = ImGui::GetItemRectMin();
            const auto br = ImGui::GetItemRectMax();
            ImVec2 pp(br[0] + 8, (tl[1] + br[1]) / 2);
            ed::BeginPin(p.id, ed::PinKind::Output);
            {
              if (m_transport->status()->state == TransportState::PLAYING)
              {
                ImGui::GetWindowDrawList()->AddCircleFilled(pp, portCircleSize, colours::u32::fromLevel(p.port->rms));
              }
              else if (m_linkedPinIds.count(p.id) > 0)
              {
                ImGui::GetWindowDrawList()->AddCircleFilled(pp, portCircleSize, colours::u32::green);
              }
              else
              {
                ImGui::GetWindowDrawList()->AddCircleFilled(pp, portCircleSize, colours::u32::grey);
              }
              ed::PinPivotRect(pp, pp);
              ed::PinRect(tl, br);
            }
            ed::EndPin();
          }
        }
        ImGui::EndGroup();
      }
      ed::EndNode();
    }
  }

  void Application::render_Links()
  {
    for (const auto &[id, k] : m_links)
    {
      if (m_transport->status()->state == TransportState::PLAYING)
      {
        ed::Link(id, k.inputId, k.outputId, colours::vec::fromLevel(k.out->rms), 2.0);
      }
      else
      {
        ed::Link(id, k.inputId, k.outputId, colours::vec::white, 2.0);
      }
    }
  }

  bool Application::render_Inspector()
  {
    bool changed = false;
    ImGui::Begin("Inspector", &m_inspector, immovableWindow);
    {
      std::vector<ed::NodeId> selectedNodes;
      selectedNodes.resize(ed::GetSelectedObjectCount());
      int nodeCount = ed::GetSelectedNodes(selectedNodes.data(), static_cast<int>(selectedNodes.size()));
      for (const auto &selectedId : selectedNodes)
      {
        if (m_nodes.find(selectedId) != m_nodes.end())
        {
          auto &n = m_nodes.at(selectedId);
          auto m = n.m;
          ImGui::PushID(n.id.Get());
          ImGui::TextUnformatted(m->name().c_str());
          ImGui::Indent();
          if (ImGui::CollapsingHeader("Params"))
          {
            ImGui::BeginGroup();
            ImGui::Indent();
            ImGuiInputTextFlags textEditflags = ImGuiInputTextFlags_CharsNoBlank;
            // Edit the node's params
            for (auto &param : m->paramsKeys())
            {
              auto &val = m->param(param);
              const bool isConnected = std::any_of(n.ports.begin(), n.ports.end(), [this, &param](ModulePort &p)
                                                   { return p.type == connoise::model::NodeType::IN_PORT && p.name == param && m_linkedPinIds.count(p.id) > 0; });
              ImGuiInputTextFlags disabled = isConnected ? ImGuiInputTextFlags_ReadOnly : ImGuiInputTextFlags_None;
              if (std::holds_alternative<Value>(val))
              {
                auto &v = std::get<Value>(val);
                ImGui::PushStyleColor(ImGuiCol_Text, disabled ? colours::vec::grey : colours::vec::white);
                if (ImGui::InputDouble(param.c_str(), &v, 0.0, 0.0, "%.4f", disabled))
                {
                  // std::cout << "parameter " << param << " changed in module " << m->name() << std::endl;
                  changed = true;
                  m->onParameterChanged(param, m_transport->status()->t);
                }
                ImGui::PopStyleColor();
              }
              if (std::holds_alternative<std::string>(val))
              {
                auto &v = std::get<std::string>(val);
                ImGui::PushStyleColor(ImGuiCol_Text, disabled ? colours::vec::grey : colours::vec::white);
                if (ImGui::InputText(param.c_str(), &v, textEditflags | disabled))
                {
                  // std::cout << "parameter " << param << " changed in module " << m->name() << std::endl;
                }
                ImGui::PopStyleColor();
              }
            }
            ImGui::Unindent();
            ImGui::EndGroup();
          }
          ImGui::Unindent();
          ImGui::PopID();
        }
      }
    }
    ImGui::End();

    return changed;
  }

  void Application::render_Status()
  {
    ImGui::Begin("Status", NULL, immovableWindow);
    {
      ImGui::Text(
          "UI FPS: %0.2f : %s",
          m_imgui->frameRate(),
          m_transport->status()->to_string().c_str());
    }
    ImGui::End();
  }

  bool Application::render_NewNodeList(const LabelType ltype)
  {
    const auto scale = m_imgui->dpiScale();
    const auto btnDim = 32.0;
    ImVec2 btnSize(0, 0);
    if (ltype == LabelType::Icon)
    {
      btnSize = ImVec2(btnDim * scale, btnDim * scale);
    }
    bool created = false;
    int i = 0;
    for (auto &&[type, _] : connoise::modules::registry::Inputs)
    {
      ImGui::PushID(type.c_str());
      const auto lbl = ltype == LabelType::Text ? type.c_str() : "";
      ImGui::Button(lbl, btnSize);
      if (ltype == LabelType::Icon)
      {
        const auto tl = ImGui::GetItemRectMin();
        const auto br = ImGui::GetItemRectMax();
        render_NewNodeIcon(type, tl, br, btnDim, scale);
      }
      if (ImGui::IsItemHovered())
      {
        ImGui::BeginTooltip();
        ImGui::Text("Add %s", type.c_str());
        ImGui::EndTooltip();
      }
      if (ImGui::IsItemClicked(0))
      {
        // std::cout << "Application create new module type " << type << std::endl;
        const auto nextId = m_system->nextModuleId();
        connoise::InitList init{{"label", std::to_string(nextId)}};
        m_system->createModule(nextId, type, init);
        created |= true;
      }
      ImGui::PopID();
    }

    ImGui::Separator();

    for (auto &&[type, _] : connoise::modules::registry::Processors)
    {
      ImGui::PushID(type.c_str());
      const auto lbl = ltype == LabelType::Text ? type.c_str() : "";
      ImGui::Button(lbl, btnSize);
      if (ltype == LabelType::Icon)
      {
        const auto tl = ImGui::GetItemRectMin();
        const auto br = ImGui::GetItemRectMax();
        render_NewNodeIcon(type, tl, br, btnDim, scale);
        if ((i++) % 2 == 0)
        {
          ImGui::SameLine();
        }
      }
      if (ImGui::IsItemHovered())
      {
        ImGui::BeginTooltip();
        ImGui::Text("Add %s", type.c_str());
        ImGui::EndTooltip();
      }
      if (ImGui::IsItemClicked(0))
      {
        // std::cout << "Application create new module type " << type << std::endl;
        const auto nextId = m_system->nextModuleId();
        connoise::InitList init{{"label", std::to_string(nextId)}};
        m_system->createModule(nextId, type, init);
        created |= true;
      }
      ImGui::PopID();
    }

    ImGui::Separator();

    for (auto &&[type, _] : connoise::modules::registry::Outputs)
    {
      ImGui::PushID(type.c_str());
      const auto lbl = ltype == LabelType::Text ? type.c_str() : "";
      ImGui::Button(lbl, btnSize);
      if (ltype == LabelType::Icon)
      {
        const auto tl = ImGui::GetItemRectMin();
        const auto br = ImGui::GetItemRectMax();
        render_NewNodeIcon(type, tl, br, btnDim, scale);
      }
      if (ImGui::IsItemHovered())
      {
        ImGui::BeginTooltip();
        ImGui::Text("Add %s", type.c_str());
        ImGui::EndTooltip();
      }
      if (ImGui::IsItemClicked(0))
      {
        // std::cout << "Application create new module type " << type << std::endl;
        const auto nextId = m_system->nextModuleId();
        connoise::InitList init{{"label", std::to_string(nextId)}};
        m_system->createModule(nextId, type, init);
        created |= true;
      }
      ImGui::PopID();
    }

    return created;
  }

  void Application::render_NewNodeIcon(const std::string &type, const ImVec2 &tl, const ImVec2 &br, const float &size, const float &scale)
  {
    // positions
    std::map<const char *, float> p{
        {"top", tl[1]},
        {"btm", br[1]},
        {"lft", tl[0]},
        {"rgt", br[0]},
    };

    const auto scalesize = scale * size;

    // dimensions
    std::map<const char *, float> d{
        {"lt_1", scale * 1.25f},  // normal line thickness
        {"lt_2", scale * 0.625f}, // half normal line thickness
        {"0.05", scalesize * 0.05f},
        {"0.10", scalesize * 0.10f},
        {"0.23", scalesize * 0.23f},
        {"0.25", scalesize * 0.25f},
        {"0.33", scalesize * 0.33f},
        {"0.36", scalesize * 0.36f},
        {"0.40", scalesize * 0.40f},
        {"0.50", scalesize * 0.50f},
        {"0.66", scalesize * 0.66f},
        {"0.75", scalesize * 0.75f},
    };

    ImVec2 centre(p["lft"] + d["0.50"], p["top"] + d["0.50"]);

    auto *drawList = ImGui::GetWindowDrawList();

    // Inputs

    if (type == "Constant")
    {
      std::string txt("1");
      drawList->AddText({p["lft"] + d["0.40"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
    if (type == "NullInput")
    {
      drawList->AddLine({p["rgt"], centre[1]}, centre, colours::u32::white, d["lt_1"]);
      std::string txt("x");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
    }

    // Processors

    if (type == "ADSR")
    {
      std::vector<ImVec2> points{
          {p["lft"], p["btm"]},
          {p["lft"] + d["0.25"], p["top"]},
          centre,
          {p["lft"] + d["0.75"], centre[1]},
          {p["rgt"], p["btm"]},
      };
      drawList->AddPolyline(&points[0], points.size(), colours::u32::white, false, d["lt_1"]);
    }
    if (type == "Abs")
    {
      drawList->AddLine({p["lft"], centre[1]}, {p["lft"], p["top"]}, colours::u32::white, d["lt_1"]);
      drawList->AddLine(centre, {centre[0], p["top"]}, colours::u32::white, d["lt_1"]);
      drawList->AddLine(centre, {centre[0], p["btm"]}, colours::u32::grey, d["lt_1"]);
      drawList->AddLine({p["lft"], centre[1]}, {p["rgt"], centre[1]}, colours::u32::white, d["lt_1"]);
    }
    if (type == "Add")
    {
      std::string txt("+");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
    if (type == "Delay")
    {
      drawList->AddLine({p["lft"], p["btm"]}, {p["rgt"], p["btm"]}, colours::u32::white, d["lt_1"]);
      drawList->AddLine({p["lft"] + d["0.33"], p["btm"]}, {p["lft"] + d["0.33"], centre[1] - d["0.25"]}, colours::u32::white, d["lt_1"]);
    }
    if (type == "Differential")
    {
      std::string txt("d");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
    if (type == "FirFilter")
    {
      std::vector<ImVec2> points{
          {p["lft"], p["top"] + d["0.25"]},
          {p["lft"] + d["0.25"], p["top"] + d["0.25"]},
          {centre[0], p["top"]},
          {p["rgt"], p["btm"]},
      };
      drawList->AddBezierCubic(points[0], points[1], points[2], points[3], colours::u32::white, d["lt_1"]);
    }
    if (type == "Hold")
    {
      std::vector<ImVec2> points{
          {p["lft"], p["btm"]},
          {p["lft"] + d["0.25"], p["btm"]},
          {p["lft"] + d["0.25"], p["top"]},
          {centre[0], p["top"]},
          {centre[0], p["btm"]},
          {p["rgt"], p["btm"]},
      };
      drawList->AddPolyline(&points[0], points.size(), colours::u32::white, false, d["lt_1"]);
    }
    if (type == "HzMidiNote")
    {
      std::string txt("f-n");
      drawList->AddText({p["lft"] + d["0.10"], p["top"] + d["0.25"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
    }
    if (type == "MidiNoteHz")
    {
      std::string txt("n-f");
      drawList->AddText({p["lft"] + d["0.10"], p["top"] + d["0.25"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
    }
    if (type == "Mixer")
    {
      std::string txt("*");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
    if (type == "Multiply")
    {
      std::string txt("x");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
    if (type == "Random")
    {
      std::string txt("?");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
    if (type == "Saw")
    {
      std::vector<ImVec2> points{
          {p["lft"] + d["0.10"], centre[1]},
          {p["lft"] + d["0.33"], p["top"]},
          {p["lft"] + d["0.66"], p["btm"]},
          {p["rgt"] - d["0.10"], centre[1]},
      };
      drawList->AddPolyline(&points[0], points.size(), colours::u32::white, false, d["lt_1"]);
      drawList->AddLine({p["lft"], centre[1]}, {p["rgt"], centre[1]}, colours::u32::white, d["lt_1"]);
    }
    if (type == "Sequencer")
    {
      drawList->AddCircle({p["lft"] + d["0.33"], p["top"] + d["0.33"]}, d["0.10"], colours::u32::white, 0, d["lt_1"]);
      drawList->AddCircle({p["lft"] + d["0.33"], p["top"] + d["0.66"]}, d["0.10"], colours::u32::white, 0, d["lt_1"]);
      drawList->AddCircle({p["lft"] + d["0.66"], p["top"] + d["0.66"]}, d["0.10"], colours::u32::white, 0, d["lt_1"]);
      drawList->AddCircleFilled({p["lft"] + d["0.66"], p["top"] + d["0.33"]}, d["0.10"], colours::u32::white, 0);
    }
    if (type == "Sine")
    {
      std::vector<ImVec2> points{
          {p["lft"] + d["0.10"], centre[1]},
          {centre[0], p["top"] - d["0.75"]},
          {centre[0], p["btm"] + d["0.75"]},
          {p["rgt"] - d["0.10"], centre[1]},
      };
      drawList->AddBezierCubic(points[0], points[1], points[2], points[3], colours::u32::white, d["lt_1"]);
      drawList->AddLine({p["lft"], centre[1]}, {p["rgt"], centre[1]}, colours::u32::white, d["lt_1"]);
    }
    if (type == "Square")
    {
      std::vector<ImVec2> points{
          {p["lft"] + d["0.10"], centre[1]},
          {p["lft"] + d["0.10"], p["top"]},
          {centre[0], p["top"]},
          {centre[0], p["btm"]},
          {p["rgt"] - d["0.10"], p["btm"]},
          {p["rgt"] - d["0.10"], centre[1]},
      };
      drawList->AddPolyline(&points[0], points.size(), colours::u32::white, false, d["lt_1"]);
      drawList->AddLine({p["lft"], centre[1]}, {p["rgt"], centre[1]}, colours::u32::white, d["lt_1"]);
    }

    // Outputs

    if (type == "AudioDevice")
    {
      drawList->AddLine({p["lft"], centre[1]}, centre, colours::u32::white, d["lt_1"]);
      drawList->AddRectFilled({p["lft"] + d["0.33"], p["top"] + d["0.25"]}, {centre[0], p["top"] + d["0.75"]}, colours::u32::white);
      drawList->AddNgon({p["lft"] + d["0.75"], centre[1]}, -d["0.25"], colours::u32::white, 3, d["lt_1"]);
    }
    if (type == "NullOutput")
    {
      drawList->AddLine({p["lft"], centre[1]}, centre, colours::u32::white, d["lt_1"]);
      std::string txt("x");
      drawList->AddText({p["lft"] + d["0.36"], p["top"] + d["0.23"]}, colours::u32::white, &txt[0], &txt[txt.size()]);
    }
    if (type == "WavFile")
    {
      drawList->AddCircle(centre, d["0.05"], colours::u32::white, 0, d["lt_2"]);
      drawList->AddCircle(centre, d["0.40"], colours::u32::white, 0, d["lt_1"]);
    }
  }

  bool Application::handle_deleteItems()
  {
    bool changed = false;
    if (ed::BeginDelete())
    {
      // There may be many links marked for deletion, let's loop over them.
      ed::LinkId deletedLinkId;
      while (ed::QueryDeletedLink(&deletedLinkId))
      {
        if (m_links.find(deletedLinkId) == m_links.end())
        {
          std::cerr << "Application::handle_deleteItems tried to delete a link we have no ref for" << std::endl;
          ed::RejectDeletedItem();
        }
        else
        {
          const auto link = m_links.at(deletedLinkId);
          if (m_ports.find(link.inputId) == m_ports.end())
          {
            std::cerr << "Application::handle_deleteItems tried to delete a link we have no input port ref for" << std::endl;
            ed::RejectDeletedItem();
          }
          else if (m_ports.find(link.outputId) == m_ports.end())
          {
            std::cerr << "Application::handle_deleteItems tried to delete a link we have no outpot port ref for" << std::endl;
            ed::RejectDeletedItem();
          }
          else
          {
            auto op = m_ports.at(link.outputId).port;
            auto ip = m_ports.at(link.inputId).port;
            if (m_system->deleteConnection(op, ip))
            {
              changed = true;
              ed::AcceptDeletedItem();
            }
            else
            {
              std::cerr << "Application::handle_deleteItems system declined to delete link" << std::endl;
              ed::RejectDeletedItem();
            }
          }
        }
      }

      // There may be many nodes marked for deletion, let's loop over them.
      ed::NodeId deletedNodeId;
      while (ed::QueryDeletedNode(&deletedNodeId))
      {
        if (m_nodes.find(deletedNodeId) == m_nodes.end())
        {
          std::cerr << "Application::handle_deleteItems tried to delete a node we have no ref for" << std::endl;
          ed::RejectDeletedItem();
        }
        else
        {
          const auto node = m_nodes.at(deletedNodeId);
          if (m_system->deleteModule(node.m->id()))
          {
            changed = true;
            ed::AcceptDeletedItem();
          }
          else
          {
            std::cerr << "Application::handle_deleteItems system declined to delete node" << std::endl;
            ed::RejectDeletedItem();
          }
        }
      }
    }
    ed::EndDelete();

    return changed;
  }

  bool Application::handle_createLink()
  {
    bool changed = false;
    NewNodeInfo newNodeInfo{
        .open = false,
    };

    if (ed::BeginCreate())
    {
      ed::PinId inputPinId, outputPinId;
      if (ed::QueryNewLink(&inputPinId, &outputPinId))
      {
        // if both are valid, let's test the link
        if (inputPinId && outputPinId)
        {
          connoise::modules::ModulePort::shared_ptr in;
          connoise::model::NodeType inType;
          connoise::modules::ModulePort::shared_ptr out;
          connoise::model::NodeType outType;

          if (m_ports.find(inputPinId) != m_ports.end())
          {
            const auto p = m_ports.at(inputPinId);
            in = p.port;
            inType = p.type;
          }
          if (m_ports.find(outputPinId) != m_ports.end())
          {
            const auto p = m_ports.at(outputPinId);
            out = p.port;
            outType = p.type;
          }
          // ensure the types are different
          if (inType == outType)
          {
            in = nullptr;
            out = nullptr;
          }
          // get them in the acceptable order
          if (inType == connoise::model::NodeType::OUT_PORT && outType == connoise::model::NodeType::IN_PORT)
          {
            auto tmp = out;
            out = in;
            in = tmp;
          }
          // if they are not both set
          if (!(in && out))
          {
            ed::RejectNewItem(colours::vec::red);
          }
          // otherwise lets create!
          else if (ed::AcceptNewItem(colours::vec::green))
          {
            auto mo = m_system->getModule(out->moduleId());
            auto mi = m_system->getModule(in->moduleId());
            m_system->createConnection(
                mo, out->portName(),
                mi, in->portName());
            changed = true;
          }
        }
      }
      else if (ed::QueryNewNode(&inputPinId))
      {
        newNodeInfo.open = ImGui::IsMouseReleased(0);
        newNodeInfo.pinId = inputPinId;
        newNodeInfo.pinKind = ed::PinKind::Input;
      }
      else if (ed::QueryNewNode(&outputPinId))
      {
        newNodeInfo.open = ImGui::IsMouseReleased(0);
        newNodeInfo.pinId = outputPinId;
        newNodeInfo.pinKind = ed::PinKind::Output;
      }
    }
    ed::EndCreate();

    if (render_NewNodePopup(newNodeInfo, LabelType::Text))
    {
      std::cout << "Application::handle_createLink handled new node popup" << std::endl;
      changed = true;
    }

    return changed;
  }

  bool Application::render_NewNodePopup(const NewNodeInfo &info, const LabelType ltype)
  {
    bool changed = false;
    ed::Suspend();
    if (info.open)
    {
      ImGui::OpenPopup("NewNodePopup");
    }
    if (ImGui::BeginPopup("NewNodePopup"))
    {
      if (render_NewNodeList(ltype))
      {
        changed = true;
        // TODO: something about next selecting the input/output to connect to the new module
        ImGui::CloseCurrentPopup();
      }
      ImGui::EndPopup();
    }
    ed::Resume();

    return changed;
  }

  void Application::render_Prompt()
  {
    bool haveChoices = m_prompt.choices.size() > 0;
    if (haveChoices)
    {
      ImGui::OpenPopup("Question");
    }
    if (ImGui::BeginPopupModal("Question", &haveChoices))
    {
      bool didAction = false;
      ImGui::TextUnformatted(m_prompt.text.c_str());
      ImGui::Separator();
      ImGui::BeginGroup();
      {
        for (const auto &[label, action] : m_prompt.choices)
        {
          ImGui::SameLine();
          if (ImGui::Button(label.c_str()))
          {
            didAction = action();
          }
        }
      }
      ImGui::EndGroup();

      if (didAction)
      {
        m_prompt.text.clear();
        m_prompt.choices.clear();
        haveChoices = false;
      }

      ImGui::EndPopup();
    }
  }

} // namespace connoise::gui::contexts
