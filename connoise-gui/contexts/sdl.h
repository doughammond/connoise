/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <SDL2/SDL.h>
#include <imgui_impl_sdl2.h>

namespace connoise::gui::contexts
{
  /**
   * Type of action callback function
   */
  using KeyAction = std::function<void()>;

  /**
   * Structure for matching keyboard events to callback actions.
   */
  struct KeyCommand
  {
    /**
     * Key data to match against input events.
     */
    SDL_Keysym key;

    /**
     * Action to call back when the key is matched.
     */
    KeyAction action;
  };

  /**
   * Container type for KeyCommand.
   */
  using KeyCommands = std::vector<KeyCommand>;

  /**
   * @brief GUI SDL media context.
   *
   */
  class SDLContext
  {
  public:
    /**
     * @brief Shared pointer type for this context.
     *
     */
    using shared_ptr = std::shared_ptr<SDLContext>;

    /**
     * @brief Construct a new SDLContext object.
     *
     * @param title main window title
     */
    SDLContext(const std::string &title);

    /**
     * @brief Destroy the SDLContext object.
     *
     */
    ~SDLContext();

    /**
     * @brief Getter for the context initialisation state.
     *
     * @return const int
     */
    const int state() const
    {
      return m_state;
    }

    /**
     * @brief Get pointer to SDL main window.
     *
     * @return SDL_Window*
     */
    SDL_Window *window() const
    {
      return m_window;
    }

    /**
     * @brief Get the current SDL main window size.
     *
     * @return std::vector<int>
     */
    std::vector<int> windowSize()
    {
      int w, h;
      SDL_GetWindowSize(m_window, &w, &h);
      return {w, h};
    }

    /**
     * @brief Getter for OpenGL context ref.
     *
     * @return SDL_GLContext&
     */
    SDL_GLContext &glContext()
    {
      return m_glContext;
    }

    /**
     * @brief Getter for GLSL version being used.
     *
     * @return const std::string
     */
    const std::string glslVersion() const
    {
      return m_glslVersion;
    }

    /**
     * @brief Context onFrame handler; called for every rendering frame.
     *
     * @return SDLKeyEvents
     */
    void onFrame();

    /**
     * @brief Register an action callback for an SDL_keysym matching a keyboard input event.
     *
     */
    void registerKeyCommand(const SDL_Keysym &key, KeyAction action)
    {
      m_keyActions.push_back({
          .key = key,
          .action = action,
      });
    }

    /**
     * @brief Register a named action callback; e.g. "quit"
     *
     */
    void registerNamedAction(const std::string &name, KeyAction action)
    {
      m_namedActions[name] = action;
    }

    /**
     * @brief Try to execute a named action, if it has been registered.
     *
     */
    void executeNamedAction(const std::string &name)
    {
      if (m_namedActions.find(name) != m_namedActions.end())
      {
        m_namedActions.at(name)();
      }
    }

  private:
    /**
     * @brief Context initialisation state.
     *
     */
    int m_state;

    /**
     * @brief GLSL version being used.
     *
     */
    std::string m_glslVersion;

    /**
     * @brief The main SDL window.
     *
     */
    SDL_Window *m_window;

    /**
     * @brief The OpenGL rendering context.
     *
     */
    SDL_GLContext m_glContext;

    /**
     * List of registered keyboard event actions.
     */
    KeyCommands m_keyActions;

    /**
     * Map of named actions.
     */
    std::map<std::string, KeyAction> m_namedActions;
  };

} // namespace connoise::gui::contexts
