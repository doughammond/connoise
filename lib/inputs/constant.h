/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::inputs
{
  /**
   * @brief Constant input module.
   *
   * Does nothing except emit a zero signal.
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Constant : public connoise::modules::Module
  {
  public:
    /**
     * @brief Constant shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Constant>;

    /**
     * @brief Construct a new Constant Module.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Constant(const size_t id, const InitList &init);

    virtual void onParameterChanged(const std::string &param, const TimeVec &t) override
    {
      // no implementation required; the `v` param is not linked to any Input
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);
  };

} // namespace connoise::inputs
