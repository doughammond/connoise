/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <SDL2/SDL.h>

#include "midi_device.h"

namespace connoise::inputs
{
  MidiDevice::MidiDevice(const size_t id, const InitList &init)
      : Module(ModuleClass::Input, "MidiDevice", id),
        m_seq(nullptr)
  {
    CONNOISE_MODULE_CREATE_INPUT(MidiDevice, c);

    CONNOISE_MODULE_CREATE_OUTPUT(MidiDevice, n);
    CONNOISE_MODULE_CREATE_OUTPUT(MidiDevice, v);
    initialise(init, {
                         {"c", 1.0},
                     });
  }

  void MidiDevice::calculateBuffers(const TimeVec &t)
  {
    if (m_t != t[0])
    {
      auto &nbuf = getOutput("n")->buf;
      auto &vbuf = getOutput("v")->buf;
      for (Sample s = 0; s < t.size(); ++s)
      {
        if (m_notes.size() > 0)
        {
          const auto &front = m_notes.front();
          m_note = front.n;
          m_vel = front.v;
        }
        else
        {
          // DEBUG_MIDIDEVICE std::cout
          //     << name()
          //     << " empty deque, setting vel=0 "
          //     << std::endl;
          m_vel = 0;
        }

        nbuf[s] = m_note;
        vbuf[s] = m_vel;
      }
      m_t = t[0];
    }
  }

  const Buffer &MidiDevice::n(const TimeVec &t)
  {
    calculateBuffers(t);
    return getOutput("n")->buf;
  }

  const Buffer &MidiDevice::v(const TimeVec &t)
  {
    calculateBuffers(t);
    return getOutput("v")->buf;
  }

  const Buffer &MidiDevice::c(const TimeVec &t)
  {
    const auto &buf = getInput("c")->buf;
    m_channel = buf[buf.size() - 1];
    return buf;
  }

} // namespace connoise::inputs
