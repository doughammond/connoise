/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MIDIDEVICE if (false)

#include <deque>
#include <functional>
#include <memory>

#include <alsa/asoundlib.h>
#include <tbb/task_arena.h>

#include "../module.h"

namespace connoise::inputs
{
  /**
   * @brief Monophonic MIDI Device input module.
   * 
   * Inputs:
   * 
   * - `c` : MIDI channel
   *
   * Outputs:
   *
   * - `n` : MIDI note number
   * - `v` : MIDI note velocity
   *
   */
  class MidiDevice : public connoise::modules::Module
  {
  public:
    /**
     * @brief MidiDevice shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<MidiDevice>;

    /**
     * @brief Construct a new MidiDevice Module.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    MidiDevice(const size_t id, const InitList &init);

    ~MidiDevice()
    {
      DEBUG_MIDIDEVICE std::cout << name() << "::~MidiDevice start" << std::endl;
      unprepare();
      DEBUG_MIDIDEVICE std::cout << name() << "::~MidiDevice end" << std::endl;
    }

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      Module::prepare(bufferSize, Fs);

      m_t = -1;
      m_note = 0;
      m_vel = 0;
      m_channel = valueParam("c");
      m_notes.clear();

      if (m_seq == nullptr)
      {
        snd_seq_t *seq;
        snd_seq_open(&seq, "default", SND_SEQ_OPEN_INPUT, 0);
        if (seq == nullptr)
        {
          std::cerr << "MidiInput cannot open ALSA seq" << std::endl;
          return;
        }
        m_seq = seq;
      }
      snd_seq_set_client_name(m_seq, "Connoise");

      m_port = snd_seq_create_simple_port(
          m_seq, name().c_str(),
          SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE,
          SND_SEQ_PORT_TYPE_MIDI_GENERIC);

      m_running = true;
      m_arena.enqueue([this]() {
        snd_seq_event_t *ev;
        int npfd = snd_seq_poll_descriptors_count(m_seq, POLLIN);
        struct pollfd *pfd = (struct pollfd *)alloca(npfd * sizeof(struct pollfd));
        while (m_running)
        {
          if (poll(pfd, npfd, 100000) > 0)
          {
            do
            {
              snd_seq_event_input(m_seq, &ev);
              switch (ev->type)
              {
              case SND_SEQ_EVENT_NOTEON:
                DEBUG_MIDIDEVICE std::cout
                    << name()
                    << " Note On"
                    << " m_channel " << m_channel
                    << " channel " << (int)ev->data.control.channel
                    << " note " << (int)ev->data.note.note
                    << " vel " << (int)ev->data.note.velocity
                    << std::endl;
                if (m_channel == ev->data.control.channel + 1)
                {
                  m_notes.push_front({
                      .n = (Value)ev->data.note.note,
                      .v = (Value)ev->data.note.velocity / 255.0,
                  });
                  DEBUG_MIDIDEVICE std::cout
                      << name()
                      << " push note; deque size is " << m_notes.size()
                      << std::endl;
                }
                break;
              case SND_SEQ_EVENT_NOTEOFF:
                DEBUG_MIDIDEVICE std::cout
                    << name()
                    << " Note Off"
                    << " m_channel " << m_channel
                    << " channel " << (int)ev->data.control.channel
                    << " note " << (int)ev->data.note.note
                    << " vel " << (int)ev->data.note.velocity
                    << std::endl;
                if (m_channel == ev->data.control.channel + 1)
                {
                  auto rem = std::remove_if(m_notes.begin(), m_notes.end(), [&ev](Note &tn) {
                    return tn.n == (Value)ev->data.note.note;
                  });
                  m_notes.erase(rem, m_notes.end());
                  DEBUG_MIDIDEVICE std::cout
                      << name()
                      << " pop note; deque size is " << m_notes.size()
                      << std::endl;
                }
                break;
              }
              snd_seq_free_event(ev);
            } while (snd_seq_event_input_pending(m_seq, 0) > 0);
          }
        }
      });
    }

    virtual void unprepare() override
    {
      Module::unprepare();

      DEBUG_MIDIDEVICE std::cout << name() << "::unprepare start" << std::endl;
      if (m_seq)
      {
        m_running = false;
        m_arena.terminate();
        snd_seq_delete_port(m_seq, m_port);
        snd_seq_close(m_seq);
        m_seq = nullptr;
        m_port = 0;
      }
      DEBUG_MIDIDEVICE std::cout << name() << "::unprepare end" << std::endl;
    }

    void calculateBuffers(const TimeVec &t);

  private:
    // Outputs

    /**
     * @brief Output: MIDI note value.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &n(const TimeVec &t);

    /**
     * @brief Output: MIDI note velocity, normalised to range 0.0 - 1.0.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Output: MIDI channel number.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &c(const TimeVec &t);

    snd_seq_t *m_seq;
    int m_port;
    int m_channel;

    tbb::task_arena m_arena;
    bool m_running;

    struct Note
    {
      Value n; // Note
      Value v; // Velocity
    };

    std::deque<Note> m_notes;
    Value m_note;
    Value m_vel;

    Time m_t;
  };

} // namespace connoise::inputs
