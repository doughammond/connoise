/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "constant.h"

namespace connoise::inputs
{
  Constant::Constant(const size_t id, const InitList &init)
      : Module(ModuleClass::Input, "Constant", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Constant, v);
    initialise(init, {
                         {"v", 0.0},
                     });
  }

  const Buffer &Constant::v(const TimeVec &t)
  {
    const auto val = valueParam("v");
    auto &buf = getOutput("v")->buf;
    std::fill(buf.begin(), buf.end(), val);
    return buf;
  }

} // namespace connoise::inputs
