/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "null.h"

namespace connoise::inputs
{
  NullInput::NullInput(const size_t id, const InitList &init)
      : Module(ModuleClass::Input, "NullInput", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(NullInput, v);
    initialise(init, {});
  }

  const Buffer &NullInput::v(const TimeVec &t)
  {
    const auto &buf = getOutput("v")->buf;
    return buf;
  }

} // namespace connoise::inputs
