/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::inputs
{
  /**
   * @brief Null input module.
   *
   * Does nothing except emit a zero signal.
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class NullInput : public connoise::modules::Module
  {
  public:
    /**
     * @brief NullInput shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<NullInput>;

    /**
     * @brief Construct a new NullInput Module.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    NullInput(const size_t id, const InitList &init);

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);
  };

} // namespace connoise::inputs
