/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <functional>
#include <map>
#include <vector>

#include "module.h"
#include "types.h"

namespace connoise
{

  /**
   * @brief Represents a connection between Module ports.
   *
   * The running system uses lists of these objects in order to
   * propagate signals between modules.
   *
   */
  struct Connection
  {
    /**
     * @brief The Module output port of this connection.
     *
     * The Signal source.
     *
     */
    connoise::modules::ModulePort::shared_ptr out;

    /**
     * @brief The Module input port of this connection.
     *
     * The Signal sink.
     *
     */
    connoise::modules::ModulePort::shared_ptr in;
  };

  /**
   * @brief A vector of Connections.
   *
   */
  using Connections = std::vector<Connection>;

  /**
   * @brief A mapping of graph depth value to vector of Connections.
   *
   */
  using DepthSortedConnections = std::map<int, Connections>;

} // namespace connoise
