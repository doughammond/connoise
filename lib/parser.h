/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

// #define DEBUG_PARSER

#pragma once

#ifdef DEBUG_PARSER
#include <iostream>
#endif

#include <iomanip>
#include <sstream>
#include <stack>

#include <tao/pegtl.hpp>

#include "system.h"

/**
 * @brief Parts of grammar for the connoise DSL.
 *
 * @see tao::pegtl
 *
 */
namespace connoise::grammar
{
  /**
   * @brief Structure to hold data constructing a Module as it is parsed.
   *
   */
  struct ParserModule
  {
    /**
     * @brief Currently parsing Module ID.
     *
     */
    size_t id;
    /**
     * @brief Currently parsing Module type.
     *
     */
    std::string type;
    /**
     * @brief Currently parsing Module InitList.
     *
     */
    InitList init;
  };

  /**
   * @brief Structure to hold the parsing state as it executes.
   *
   */
  struct ParserData
  {
    /**
     * @brief The System being built by the parser.
     *
     */
    connoise::System::shared_ptr system;
    /**
     * @brief Current Module being constructed by the parser.
     *
     */
    ParserModule module;

    /**
     * @brief Stack of strings which have been parsed.
     *
     */
    std::stack<std::string> temp_strings;
    /**
     * @brief Stack of InitValues which have been parsed.
     *
     */
    std::stack<InitValue> temp_values;
  };

  /**
   * @brief Take the top value off a stack.
   *
   * @tparam T stack value type
   * @param s stack instance
   * @return T stack value
   */
  template <typename T>
  T stack_top_pop(std::stack<T> &s)
  {
    auto t = s.top();
    s.pop();
    return t;
  }

  namespace p = tao::pegtl;

  // Basic parts

  /**
   * @brief Grammar construct for a Module type string.
   *
   */
  struct ModuleType
      : p::plus<
            p::sor<
                p::alnum,
                p::string<'_'>>>
  {
  };

  /**
   * @brief Grammar construct for a Module ID number.
   *
   */
  struct ModuleIdValue
      : p::plus<p::digit>
  {
  };

  /**
   * @brief Grammar construct for a Module parameter name string.
   *
   */
  struct ParamName
      : p::plus<p::alnum>
  {
  };

  struct escaped
      : p::if_must<p::one<'\\'>, p::one<'\\', '"', '\'', 'a', 'f', 'n', 'r', 't', 'v'>>
  {
  };

  struct noncontrol
      : p::not_range<0, 31>
  {
  };

  struct quotedcharacter
      : p::sor<escaped, noncontrol>
  {
  };

  /**
   * @brief Grammar construct for a Quoted String.
   *
   */
  struct QuotedString
      : p::if_must<p::one<'"'>, p::until<p::one<'"'>, quotedcharacter>>
  {
  };

  /**
   * @brief Grammar construct for a Number value.
   *
   */
  struct FloatNumber
      : p::seq<
            p::opt<p::string<'-'>>,
            p::plus<p::digit>,
            p::opt<
                p::seq<p::string<'.'>,
                       p::plus<p::digit>>>>
  {
  };

  /**
   * @brief Grammar construct for a Module ID line.
   *
   */
  struct ModuleId
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("id"),
                p::plus<p::space>,
                ModuleIdValue>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a Module parameter line.
   *
   */
  struct ModuleParam
      : p::pad<
            p::seq<
                ParamName,
                p::plus<p::space>,
                p::sor<
                    FloatNumber,
                    QuotedString>>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a set of Module parameters.
   *
   */
  struct ModuleParams
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("params"),
                p::space,
                p::pad<p::string<'{'>, p::space>,
                p::star<ModuleParam>,
                p::pad<p::string<'}'>, p::space>>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a Module.
   *
   */
  struct Module
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("Module"),
                p::plus<p::space>,
                ModuleType,
                p::plus<p::space>,
                p::string<'{'>,
                p::plus<p::space>,
                ModuleId,
                p::opt<ModuleParams>,
                p::string<'}'>>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a Input.
   *
   */
  struct Input
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("Input"),
                p::plus<p::space>,
                ModuleType,
                p::plus<p::space>,
                p::string<'{'>,
                p::plus<p::space>,
                ModuleId,
                p::opt<ModuleParams>,
                p::string<'}'>>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a Output.
   *
   */
  struct Output
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("Output"),
                p::plus<p::space>,
                ModuleType,
                p::plus<p::space>,
                p::string<'{'>,
                p::plus<p::space>,
                ModuleId,
                p::opt<ModuleParams>,
                p::string<'}'>>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a Connection id - port name pair.
   *
   */
  struct ConnectionPair
      : p::pad<
            p::seq<
                ModuleIdValue,
                p::plus<p::space>,
                ParamName>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for a Connection.
   *
   */
  struct Connection
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("Connection"),
                p::space,
                p::string<'{'>,
                ConnectionPair,
                ConnectionPair,
                p::string<'}'>>,
            p::space>
  {
  };

  struct MetadataPair
      : p::pad<
            p::seq<
                ParamName,
                p::plus<p::space>,
                QuotedString>,
            p::space>
  {
  };

  /**
   * @brief Grammar construct for System Metadata.
   *
   */
  struct Metadata
      : p::pad<
            p::seq<
                TAO_PEGTL_KEYWORD("Metadata"),
                p::pad<p::string<'{'>, p::space>,
                p::star<MetadataPair>,
                p::pad<p::string<'}'>, p::space>>,
            p::space>
  {
  };

  /**
   * @brief The entire, composite `cno` grammar.
   *
   */
  struct grammar : p::plus<p::sor<Module, Input, Output, Connection, Metadata>>
  {
  };

  /**
   * @brief Parser action for unmatched input.
   *
   * Do nothing.
   *
   * @tparam Rule
   */
  template <typename Rule>
  struct action : p::nothing<Rule>
  {
  };

  /**
   * @brief Parser action for ModuleType grammar.
   *
   * Push the type string onto the strings stack.
   *
   * @tparam void
   */
  template <>
  struct action<ModuleType>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto v = in.string();
      pd.temp_strings.push(v);

#ifdef DEBUG_PARSER
      std::cout << "parser::action<ModuleType>::apply(" << v << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the ModuleIdValue grammar.
   *
   * Push the ModuleIdValue string onto the strings stack.
   *
   * @tparam void
   */
  template <>
  struct action<ModuleIdValue>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      std::stringstream ss(in.string());
      Value v;
      ss >> v;
      pd.temp_values.push(v);

#ifdef DEBUG_PARSER
      std::cout << "parser::action<ModuleIdValue>::apply(" << v << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the ParamName grammar.
   *
   * Push the param name string onto the strings stack.
   *
   * @tparam void
   */
  template <>
  struct action<ParamName>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto v = in.string();
      pd.temp_strings.push(v);

#ifdef DEBUG_PARSER
      std::cout << "parser::action<ParamName>::apply(" << v << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the FloatNumber grammar.
   *
   * Push the value onto the values stack.
   *
   * @tparam void
   */
  template <>
  struct action<FloatNumber>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      std::stringstream ss(in.string());
      Value v;
      ss >> v;
      pd.temp_values.push(v);

#ifdef DEBUG_PARSER
      std::cout << "parser::action<FloatNumber>::apply(" << v << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the QuotedString grammar.
   *
   * Un-quote the string and push it onto the strings stack.
   *
   * @tparam void
   */
  template <>
  struct action<QuotedString>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const std::string is(in.string());
      std::stringstream ss;
      ss << is;
      std::string os;
      ss >> std::quoted(os);
      pd.temp_values.push(os);

#ifdef DEBUG_PARSER
      std::cout << "parser::action<QuotedString>::apply(" << os << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the ModuleId grammar.
   *
   * Pop the ID string from the strings stack and apply it to the
   * currently parsing Module's ID value.
   *
   * @tparam void
   */
  template <>
  struct action<ModuleId>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto vv = stack_top_pop(pd.temp_values);
      if (std::holds_alternative<Value>(vv))
      {
        const auto v = std::get<Value>(vv);
        pd.module.id = v;
      }
      else
      {
        std::cerr << "parser::action<ModuleId>::apply holds incorrect ID type"
                  << " vv=" << vv
                  << std::endl;
      }

#ifdef DEBUG_PARSER
      std::cout << "parser::action<ModuleId>::apply(" << vv << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the ModuleParam grammar.
   *
   * Pop the name and value from the string and value stacks, push the InitValue
   * into the currently parsing Module's InitList.
   *
   * @tparam void
   */
  template <>
  struct action<ModuleParam>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto k = stack_top_pop(pd.temp_strings);
      const auto v = stack_top_pop(pd.temp_values);
      pd.module.init.push_back({k, v});

#ifdef DEBUG_PARSER
      std::cout << "parser::action<ModuleParam>::apply(" << k << ", " << v << ")" << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the MetadataPair grammar.
   *
   * Pop the name and value from the string and value stacks,
   * if the value type is string, then add it to the system metadata
   *
   * @tparam void
   */
  template <>
  struct action<MetadataPair>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto k = stack_top_pop(pd.temp_strings);
      const auto vv = stack_top_pop(pd.temp_values);
      if (std::holds_alternative<std::string>(vv))
      {
        const auto v = std::get<std::string>(vv);
        // std::cout << "parser metadata pair sets system " << k << "=`" << v << "`" << std::endl;
        pd.system->metadata(k, v);
      }
      else
      {
        std::cerr << "parser::action<MetadataPair>::apply key " << k << " holds incorrect data type " << std::endl;
      }

#ifdef DEBUG_PARSER
      std::cout << "parser::action<MetadataPair>::apply(" << k << ", " << vv << ")" << std::endl;
#endif
    }
  };

#ifdef DEBUG_PARSER
  /**
   * @brief Parser action for the ModuleParams grammar.
   *
   * No-op here; just used for debugging.
   *
   * @tparam void
   */
  template <>
  struct action<ModuleParams>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto p = pd.module.init;
      std::cout << "parser::action<ModuleParams>::apply(size=" << p.size() << ")" << std::endl;
    }
  };
#endif

  /**
   * @brief Parser action for the Module grammar.
   *
   * Pop the Module type string from the strings stack, and create
   * a module in the System using the currently parsing Module's
   * ID, type and InitList.
   *
   * Then reset the currently parsing Module.
   *
   * @tparam void
   */
  template <>
  struct action<Module>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      auto m = pd.module;
      m.type = stack_top_pop(pd.temp_strings);
      pd.system->createModule(m.id, m.type, m.init);
      pd.module = ParserModule();

#ifdef DEBUG_PARSER
      std::cout << "parser::action<Module>::apply(" << m.type << ", " << m.id << ", init=" << m.init.size() << ")" << std::endl;
      std::cout << "parser::action<Module>::apply residual strings=" << pd.temp_strings.size() << " values=" << pd.temp_values.size() << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the Input grammar.
   *
   * Pop the Input type string from the strings stack, and create
   * an input in the System using the currently parsing Module's
   * ID, type and InitList.
   *
   * Then reset the currently parsing Module.
   *
   * @tparam void
   */
  template <>
  struct action<Input>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      auto m = pd.module;
      m.type = stack_top_pop(pd.temp_strings);
      pd.system->createModule(m.id, m.type, m.init);
      pd.module = ParserModule();

#ifdef DEBUG_PARSER
      std::cout << "parser::action<Input>::apply(" << m.type << ", " << m.id << ", init=" << m.init.size() << ")" << std::endl;
      std::cout << "parser::action<Input>::apply residual strings=" << pd.temp_strings.size() << " values=" << pd.temp_values.size() << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the Output grammar.
   *
   * Pop the Output type string from the strings stack, and create
   * an output in the System using the currently parsing Module's
   * ID, type and InitList.
   *
   * Then reset the currently parsing Module.
   *
   * @tparam void
   */
  template <>
  struct action<Output>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      auto m = pd.module;
      m.type = stack_top_pop(pd.temp_strings);
      pd.system->createModule(m.id, m.type, m.init);
      pd.module = ParserModule();

#ifdef DEBUG_PARSER
      std::cout << "parser::action<Output>::apply(" << m.type << ", " << m.id << ", init=" << m.init.size() << ")" << std::endl;
      std::cout << "parser::action<Output>::apply residual strings=" << pd.temp_strings.size() << " values=" << pd.temp_values.size() << std::endl;
#endif
    }
  };

  /**
   * @brief Parser action for the Connection grammar.
   *
   * Pop the source and destination IDs and port names from
   * the strings stack, and create a connection in the System.
   *
   * @tparam void
   */
  template <>
  struct action<Connection>
  {
    /**
     * @brief Application implementation for this parser action.
     *
     * @tparam ActionInput
     * @param in
     * @param pd
     */
    template <typename ActionInput>
    static void apply(const ActionInput &in, ParserData &pd)
    {
      const auto ip = stack_top_pop(pd.temp_strings);
      const auto ii = stack_top_pop(pd.temp_values);
      const auto op = stack_top_pop(pd.temp_strings);
      const auto oi = stack_top_pop(pd.temp_values);

      if (std::holds_alternative<Value>(oi) && std::holds_alternative<Value>(ii))
      {
        const auto oiv = std::get<Value>(oi);
        const auto iiv = std::get<Value>(ii);
        auto om = pd.system->getModule(oiv);
        auto im = pd.system->getModule(iiv);

        pd.system->createConnection(om, op, im, ip);
      }
      else
      {
        std::cerr << "parser::action<Connection>::apply have incorrect ID types:"
                  << " oi=" << oi
                  << " ii=" << ii
                  << std::endl;
      }

#ifdef DEBUG_PARSER
      std::cout << "parser::action<Connection>::apply residual strings=" << pd.temp_strings.size() << " values=" << pd.temp_values.size() << std::endl;
#endif
    }
  };

} // namespace connoise::grammar
