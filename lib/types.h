/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <list>
#include <string>
#include <variant>
#include <vector>

namespace connoise
{
  /**
   * @brief Audio samle value type.
   *
   */
  using Value = double;

  /**
   * @brief Audio sample time type.
   *
   */
  using Time = double;

  /**
   * @brief Audio sample number type.
   *
   */
  using Sample = long int;

  /**
   * @brief Audio buffer type.
   *
   */
  using Buffer = std::vector<Value>;

  /**
   * @brief Time vector type.
   *
   */
  using TimeVec = std::vector<Time>;

  /**
   * @brief Module parameter initialisation value.
   *
   */
  using InitValue = std::variant<std::string, Value>;
  std::ostream &operator<<(std::ostream &o, const connoise::InitValue &v); // see util.cpp

  /**
   * @brief Module parameter initialisation list.
   *
   */
  using InitList = std::list<std::pair<const std::string, const InitValue>>;

} // namespace connoise
