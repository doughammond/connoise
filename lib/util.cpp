/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <iostream>

#include "util.h"

namespace connoise
{
  /**
   * @brief Stream output operator for InitValue.
   *
   * @param o
   * @param v
   * @return std::ostream&
   */
  std::ostream &operator<<(std::ostream &o, const connoise::InitValue &v)
  {
    if (std::holds_alternative<std::string>(v))
    {
      o << std::get<std::string>(v);
    }
    if (std::holds_alternative<connoise::Value>(v))
    {
      o << std::get<connoise::Value>(v);
    }
    return o;
  }

  namespace util
  {
    Value lerp(
        const Value dstMin, const Value dstMax,
        const Value srcMin, const Value srcMax,
        const Value in)
    {
      const auto f = (in - srcMin) / (srcMax - srcMin);
      return ((dstMax - dstMin) * f) + dstMin;
    }
  } // namespace util
} // namespace connoise
