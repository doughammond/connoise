/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <functional>
#include <iomanip>
#include <iostream>
#include <map>

#include "module.h"

namespace connoise::modules
{
  /**
   * @brief Module serialisation.
   *
   * @param o
   * @param m
   * @return std::ostream&
   */
  std::ostream &operator<<(std::ostream &o, const Module::shared_ptr m);
} // namespace connoise::modules

namespace connoise::modules::registry
{
  /**
   * @brief Module factory function type.
   *
   */
  using ModuleFactory = std::function<Module::shared_ptr(const size_t id, const InitList &init)>;

  using RegistryMap = std::map<std::string, ModuleFactory>;

  /**
   * @brief Registry of known Processor modules.
   *
   * Maps the module type name to a factory function, so that modules
   * can be instantiated by name from the grammar.
   *
   */
  extern const RegistryMap Processors;

  /**
   * @brief Registry of known Input modules.
   *
   * Maps the module type name to a factory function, so that modules
   * can be instantiated by name from the grammar.
   *
   */
  extern const RegistryMap Inputs;

  /**
   * @brief Registry of known Output modules.
   *
   * Maps the module type name to a factory function, so that modules
   * can be instantiated by name from the grammar.
   *
   */
  extern const RegistryMap Outputs;

} // namespace connoise::modules::registry
