/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <algorithm>

#include "systemmodel.h"

namespace connoise::model
{
  /**
   * @brief Node search result type.
   *
   */
  using NodeSearch = std::tuple<bool, Node, GraphVertDescr>;

  /**
   * @brief Search for a Node by ID in the graph.
   *
   * @param G
   * @param id
   * @return NodeSearch
   */
  NodeSearch nodeSearch(const Graph &G, const std::string id)
  {
    auto ri = boost::make_iterator_range(boost::vertices(G));
    auto fi = std::find_if(ri.begin(), ri.end(), [&G, &id](auto v) -> bool {
      return G[v].id() == id;
    });
    if (fi != ri.end())
    {
      const Node &n = G[*fi];
      return {true, n, *fi};
    }
    return {false, {}, NULL};
  }

  /**
   * @brief Edge search result type.
   */
  using EdgeSearch = bool;

  /**
   * @brief Search for an Edge by source name and target name in the graph.
   *
   * @param G
   * @param sourceId
   * @param targetId
   * @return EdgeSearch
   */
  EdgeSearch edgeSearch(const Graph &G, const std::string sourceId, const std::string targetId)
  {
    auto ri = boost::make_iterator_range(boost::edges(G));
    auto ei = std::find_if(ri.begin(), ri.end(), [&G, &sourceId, &targetId](auto e) -> bool {
      const auto &sn = G[e.m_source];
      const auto &tn = G[e.m_target];
      return sn.id() == sourceId && tn.id() == targetId;
    });
    return ei != ri.end();
  }

  void SystemModel::ensureModule(connoise::modules::Module::shared_ptr m)
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    const std::string id = m->mid();
    bool exists = false;
    std::tie(exists, std::ignore, std::ignore) = nodeSearch(m_graph, id);
    if (!exists)
    {
      GraphVertDescr v = boost::add_vertex(m_graph);
      auto &n = m_graph[v];
      n.moduleClass = m->getClass();
      n.type = NodeType::MODULE;
      n.m = m;
      DEBUG_SYSTEMMODEL std::cout << "SystemModel::ensureModule created node for " << id << std::endl;
    }
    else
    {
      DEBUG_SYSTEMMODEL std::cerr << "SystemModel::ensureModule node for " << id << " already exists!" << std::endl;
    }
  }

  bool SystemModel::deleteModule(connoise::modules::Module::shared_ptr m)
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    bool completed = true;

    const std::string id = m->mid();

    bool exists = false;
    GraphVertDescr v;
    std::tie(exists, std::ignore, v) = nodeSearch(m_graph, id);
    if (exists)
    {
      // Must use clear first; otherwise the graph will retain invalid edges
      boost::clear_vertex(v, m_graph);
      boost::remove_vertex(v, m_graph);
      DEBUG_SYSTEMMODEL std::cout << "SystemModel::deleteModule deleted node for " << id << std::endl;
    }
    else
    {
      std::cerr << "SystemModel::deleteModule node for " << id << " does not exist" << std::endl;
      return false;
    }

    for (auto portName : m->getOutputs())
    {
      auto port = m->getOutput(portName);
      completed &= deletePort(port);
    }
    for (auto portName : m->getInputs())
    {
      auto port = m->getInput(portName);
      completed &= deletePort(port);
    }

    return completed;
  }

  void SystemModel::ensurePort(const NodeType type, connoise::modules::ModulePort::shared_ptr port)
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    const std::string id = port->pid();
    bool exists = false;
    std::tie(exists, std::ignore, std::ignore) = nodeSearch(m_graph, id);
    if (!exists)
    {
      GraphVertDescr v = boost::add_vertex(m_graph);
      auto &n = m_graph[v];
      n.type = type;
      n.port = port;
      DEBUG_SYSTEMMODEL std::cout << "SystemModel::ensureOutPort created node for " << id << std::endl;
    }
    else
    {
      DEBUG_SYSTEMMODEL std::cerr << "SystemModel::ensureOutPort node for " << id << " already exists!" << std::endl;
    }
  }

  bool SystemModel::deletePort(connoise::modules::ModulePort::shared_ptr port)
  {
    // No lock here, internal API only called from deleteModule

    const std::string id = port->pid();
    bool exists = false;
    GraphVertDescr v;
    std::tie(exists, std::ignore, v) = nodeSearch(m_graph, id);
    if (exists)
    {
      // Must use clear first; otherwise the graph will retain invalid edges
      boost::clear_vertex(v, m_graph);
      boost::remove_vertex(v, m_graph);
      DEBUG_SYSTEMMODEL std::cout << "SystemModel::deletePort deleted node for " << id << std::endl;
      return true;
    }
    else
    {
      std::cerr << "SystemModel::deletePort node for " << id << " does not exist" << std::endl;
      return false;
    }
  }

  void SystemModel::ensureConnection(const std::string &source, const std::string &target)
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    bool sourceExists = false;
    GraphVertDescr sourceVert;
    std::tie(sourceExists, std::ignore, sourceVert) = nodeSearch(m_graph, source);
    if (!sourceExists)
    {
      std::cerr << "SystemModel::ensureConnection cannot find source name " << source << std::endl;
      return;
    }

    bool targetExists = false;
    GraphVertDescr targetVert;
    std::tie(targetExists, std::ignore, targetVert) = nodeSearch(m_graph, target);
    if (!targetExists)
    {
      std::cerr << "SystemModel::ensureConnection cannot find target name " << target << std::endl;
      return;
    }

    // Connections point UP the graph!
    // So that we can BFS with distance from the System outputs to leaf nodes
    if (!edgeSearch(m_graph, target, source))
    {
      boost::add_edge(targetVert, sourceVert, m_graph);
      DEBUG_SYSTEMMODEL std::cout << "SystemModel::ensureConnection created " << source << " <- " << target << std::endl;
    }
    else
      DEBUG_SYSTEMMODEL
      {
        std::cerr << "SystemModel::ensureConnection " << source << " <- " << target << " exists!" << std::endl;
      }
  }

  bool SystemModel::deleteConnection(const std::string &source, const std::string &target)
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    bool sourceExists = false;
    GraphVertDescr sourceVert;
    std::tie(sourceExists, std::ignore, sourceVert) = nodeSearch(m_graph, source);
    if (!sourceExists)
    {
      std::cerr << "SystemModel::deleteConnection cannot find source name " << source << std::endl;
      return false;
    }

    bool targetExists = false;
    GraphVertDescr targetVert;
    std::tie(targetExists, std::ignore, targetVert) = nodeSearch(m_graph, target);
    if (!targetExists)
    {
      std::cerr << "SystemModel::deleteConnection cannot find target name " << target << std::endl;
      return false;
    }

    // Connections point UP the graph!
    // So that we can BFS with distance from the System outputs to leaf nodes
    if (edgeSearch(m_graph, target, source))
    {
      boost::remove_edge(targetVert, sourceVert, m_graph);
      DEBUG_SYSTEMMODEL std::cout << "SystemModel::deleteConnection removed " << source << " <- " << target << std::endl;
      return true;
    }
    else
    {
      std::cerr << "SystemModel::deleteConnection " << source << " <- " << target << " does not exist" << std::endl;
    }
    return false;
  }

  const DepthSortedConnections SystemModel::calculateConnectionOrder()
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    DepthSortedConnections dc;

    std::vector<GraphVertDescr> roots;
    auto vertRangeIt = boost::make_iterator_range(boost::vertices(m_graph));
    std::copy_if(vertRangeIt.begin(), vertRangeIt.end(), std::back_inserter(roots), [this](auto v) -> bool {
      return m_graph[v].moduleClass == ModuleClass::Output;
    });

    if (roots.size() == 0)
    {
      std::cerr << "SystemModel::calculateConnectionOrder cannot complete without any Outputs" << std::endl;
      return dc;
    }

    using Size = boost::graph_traits<Graph>::vertices_size_type;
    using Sizes = std::vector<Size>;

    const auto N = boost::num_vertices(m_graph);
    Sizes distances(N);
    auto distRecorder = boost::record_distances(
        boost::make_iterator_property_map(distances.begin(), boost::get(boost::vertex_index, m_graph)),
        boost::on_tree_edge());

    // Search from every root
    for (auto &&root : roots)
    {
      boost::breadth_first_search(m_graph, root, boost::visitor(boost::make_bfs_visitor(distRecorder)));
    }

    const auto existsInLevel = [&dc](const std::string &outname, const size_t level) -> const bool {
      if (dc.find(level) == dc.end())
      {
        return false;
      }
      for (auto &cx : dc[level])
      {
        if (cx.out->name() == outname)
        {
          return true;
        }
      }
      return false;
    };

    for (const auto v : vertRangeIt)
    {
      const auto &sn = m_graph[v];
      if (sn.type == NodeType::OUT_PORT)
      {
        for (const auto e : boost::make_iterator_range(boost::in_edges(v, m_graph)))
        {
          const auto &tn = m_graph[e.m_source];
          if (tn.type == NodeType::IN_PORT)
          {
            // To prevent concurrent write processing of output ports;
            // If sn.port()
            // exists in dc[rd] && exists in dc[rd+1]
            // -> add to rd
            // exists in !dc[rd] && exists in dc[rd+1]
            // --> add to rd
            // exists in dc[rd] && !exists in dc[rd+1]
            // --> add to rd+1
            // exists in !dc[rd] && !exists in dc[rd+1]
            // --> add to rd

            const auto outportname = sn.name();
            auto insertlevel = distances[v];
            if (existsInLevel(outportname, insertlevel) && !existsInLevel(outportname, insertlevel + 1))
            {
              insertlevel++;
            }

            DEBUG_SYSTEMMODEL std::cout << " Cx " << insertlevel << " : " << outportname << " <- " << tn.name() << std::endl;
            dc[insertlevel].push_back({
                .out = sn.port,
                .in = tn.port,
            });
          }
        }
      }
    }

    return dc;
  }

  const Connections SystemModel::getAllConnections()
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    Connections cx;

    std::vector<GraphEdgeDescr> ev;
    auto ri = boost::make_iterator_range(boost::edges(m_graph));
    std::copy_if(ri.begin(), ri.end(), std::back_inserter(ev), [this](auto e) -> bool {
      const auto &source = m_graph[e.m_source];
      const auto &target = m_graph[e.m_target];
      return source.type == NodeType::IN_PORT && target.type == NodeType::OUT_PORT;
    });
    std::transform(ev.begin(), ev.end(), std::back_inserter(cx), [this](auto e) -> Connection {
      const auto &source = m_graph[e.m_source];
      const auto &target = m_graph[e.m_target];
      if (!target.port)
      {
        std::cerr << "SystemModel::getAllConnections encountered edge target with no port " << e.m_target << std::endl;
      }
      if (!source.port)
      {
        std::cerr << "SystemModel::getAllConnections encountered edge source with no port " << e.m_source << std::endl;
      }
      return {
          .out = target.port,
          .in = source.port,
      };
    });

    return cx;
  }

  void SystemModel::printDot(const std::string &name)
  {
    tbb::spin_mutex::scoped_lock lock(m_modelMutex);

    using ReverseGraph = boost::reverse_graph<Graph, const Graph &>;
    using RGEdge = ReverseGraph::edge_descriptor;
    using RGVert = ReverseGraph::vertex_descriptor;

    ReverseGraph rg = boost::make_reverse_graph(m_graph);

    struct VertexPropWriter
    {
      ReverseGraph g;
      void operator()(std::ostream &out, const RGVert &v)
      {
        const auto n = g.m_g[v];
        out << " [label=\"" << n.name() << "\"";
        switch (n.type)
        {
        case NodeType::MODULE:
        {
          out << " shape=\"box\"";
          break;
        }
        }
        out << "]";
      }
    } vpw{rg};
    struct EdgePropWriter
    {
      ReverseGraph g;
      void operator()(std::ostream &out, const RGEdge &e)
      {
        const auto sn = g.m_g[e.underlying_descx.m_source];
        std::string prop = " ";
        switch (sn.type)
        {
        case NodeType::OUT_PORT:
        {
          prop += "[color=\"yellow\"]";
          break;
        }
        case NodeType::IN_PORT:
        {
          prop += "[color=\"blue\"]";
          break;
        }
        case NodeType::MODULE:
        {
          if (sn.moduleClass == ModuleClass::Output)
          {
            prop += "[color=\"green\"]";
          }
          else
          {
            prop += "[color=\"yellow\"]";
          }
          break;
        }
        }
        out << prop;
      }
    } epw{rg};
    boost::write_graphviz(std::cout, rg, vpw, epw);
  }

} // namespace connoise::model
