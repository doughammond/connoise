/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "modules.h"

#include "modules/adsr.h"
#include "modules/delay.h"
#include "modules/filter_fir.h"
#include "modules/hold.h"
#include "modules/hz_midi.h"
#include "modules/math_abs.h"
#include "modules/math_add.h"
#include "modules/math_differential.h"
#include "modules/math_multiply.h"
#include "modules/midi_hz.h"
#include "modules/mixer.h"
#include "modules/osc_saw.h"
#include "modules/osc_sin.h"
#include "modules/osc_squ.h"
#include "modules/random.h"
#include "modules/sequencer.h"

#include "inputs/constant.h"
#include "inputs/midi_device.h"
#include "inputs/null.h"

#include "outputs/audio.h"
#include "outputs/null.h"
#include "outputs/wavfile.h"

#define CONNOISE_MODULE_FACTORY(c) \
  [](const size_t id, const InitList &init) { return std::static_pointer_cast<Module>(std::make_shared<c>(id, init)); }

/**
 * @brief Macro to create a Module registry entry given only the Module's name.
 *
 */
#define CONNOISE_MODULE_REGISTER(c) \
  {                                 \
#c, CONNOISE_MODULE_FACTORY(c)  \
  }

namespace connoise::modules
{
  /**
   * @brief Return string name of a ModuleClass.
   *
   * @param cls
   * @return const std::string
   */
  const std::string moduleClassName(const ModuleClass cls)
  {
    switch (cls)
    {
    case ModuleClass::Processor:
      return "Module";
    case ModuleClass::Output:
      return "Output";
    case ModuleClass::Input:
      return "Input";
    default:
      return "ERROR";
    }
  }

  std::ostream &operator<<(std::ostream &o, const Module::shared_ptr m)
  {
    const auto cls = moduleClassName(m->getClass());
    const auto type = m->type();
    o << cls << " " << type << " {" << std::endl
      << "  id " << m->id() << std::endl
      << "  params {" << std::endl;

    for (auto &param : m->paramsKeys())
    {
      const auto &val = m->param(param);
      if (std::holds_alternative<Value>(val))
      {
        o << "    " << param << " " << std::get<Value>(val) << std::endl;
      }
      else if (std::holds_alternative<std::string>(val))
      {
        o << "    " << param << " " << std::quoted(std::get<std::string>(val)) << std::endl;
      }
    }
    o << "  }" << std::endl
      << "}" << std::endl;
    return o;
  }
} // namespace connoise::modules

namespace connoise::modules::registry
{
  using namespace connoise::modules;
  using namespace connoise::inputs;
  using namespace connoise::outputs;

  const RegistryMap Processors = {
      CONNOISE_MODULE_REGISTER(ADSR),
      CONNOISE_MODULE_REGISTER(Delay),
      CONNOISE_MODULE_REGISTER(FirFilter),
      CONNOISE_MODULE_REGISTER(Hold),
      CONNOISE_MODULE_REGISTER(HzMidiNote),
      CONNOISE_MODULE_REGISTER(Abs),
      CONNOISE_MODULE_REGISTER(Add),
      CONNOISE_MODULE_REGISTER(Differential),
      CONNOISE_MODULE_REGISTER(Multiply),
      CONNOISE_MODULE_REGISTER(MidiNoteHz),
      CONNOISE_MODULE_REGISTER(Mixer),
      {"Saw", CONNOISE_MODULE_FACTORY(Oscillator_Saw)},
      {"Sine", CONNOISE_MODULE_FACTORY(Oscillator_Sine)},
      {"Square", CONNOISE_MODULE_FACTORY(Oscillator_Square)},
      CONNOISE_MODULE_REGISTER(Random),
      CONNOISE_MODULE_REGISTER(Sequencer),
  };

  const RegistryMap Inputs = {
      CONNOISE_MODULE_REGISTER(Constant),
      CONNOISE_MODULE_REGISTER(NullInput),
      CONNOISE_MODULE_REGISTER(MidiDevice),
  };

  const RegistryMap Outputs = {
      CONNOISE_MODULE_REGISTER(AudioDevice),
      CONNOISE_MODULE_REGISTER(NullOutput),
      CONNOISE_MODULE_REGISTER(WavFile),
  };

} // namespace connoise::modules::registry
