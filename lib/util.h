/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "types.h"

/**
 * @brief Utilities...
 *
 */
namespace connoise::util
{
  /**
   * @brief Linear interpolate.
   *
   * @param dstMin destination min value
   * @param dstMax destination max value
   * @param srcMin source min value
   * @param srcMax source max value
   * @param in value within source range
   * @return Value
   */
  Value lerp(
      const Value dstMin, const Value dstMax,
      const Value srcMin, const Value srcMax,
      const Value in);
} // namespace connoise::util
