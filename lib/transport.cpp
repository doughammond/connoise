/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#define DEBUG_TRANSPORT_THREADLOOP if (false)
#define DEBUG_TRANSPORT_CONNECTION if (false)

#include <execution>
#include <functional>
#include <iostream>

#include <SDL2/SDL.h>

#include "transport.h"

namespace connoise
{
  const auto now = std::chrono::steady_clock::now;

  static const TransportStateTransitions transitions = {
      {
          TransportState::STOPPED,
          {
              {"start", TransportState::PLAYING},
          },
      },
      {
          TransportState::PLAYING,
          {
              {"stop", TransportState::STOPPED},
              {"pause", TransportState::PAUSED},
          },
      },
      {
          TransportState::PAUSED,
          {
              {"stop", TransportState::STOPPED},
              {"resume", TransportState::PLAYING},
              {"start", TransportState::PLAYING},
          },
      },
  };

  Transport::Transport(System::shared_ptr system)
      : m_runThread(true),
        m_system(system),
        m_status(std::make_shared<TransportStatus>()),
        m_nextActions({"start"})
  {
    m_status->state = TransportState::STOPPED;
    m_status->realtime = true;

    m_arena.enqueue(std::bind(&Transport::threadLoop, this));
  }

  Transport::~Transport()
  {
    stopThread();
  }

  void Transport::start(const int Fs, const int bufsize, const bool realtime)
  {
    if (m_nextActions.count("start") > 0)
    {
      prepare(Fs, bufsize, realtime);
      m_status->t0 = 0.0;
      m_startTime = now();
    }
    nextState("start");
  }

  void Transport::stop()
  {
    DEBUG_TRANSPORT std::cout << "Transport::stop (1)" << std::endl;
    if (nextState("stop"))
    {
      DEBUG_TRANSPORT std::cout << "Transport::stop (2)" << std::endl;
      unprepare();
      DEBUG_TRANSPORT std::cout << "Transport::stop (3)" << std::endl;
    }
    DEBUG_TRANSPORT std::cout << "Transport::stop (4)" << std::endl;
  }

  void Transport::pause()
  {
    nextState("pause");
  }

  void Transport::resume()
  {
    nextState("resume");
  }

  bool Transport::nextState(const std::string &action)
  {
    const auto current = m_status->state;
    if (transitions.find(current) == transitions.end())
    {
      std::cerr << "Transport::nextState - no valid transitions from current state" << current << std::endl;
      return false;
    }
    const auto t = transitions.at(current);
    if (t.find(action) == t.end())
    {
      std::cerr << "Transport::nextState does not handle action " << action << " from state " << current << std::endl;
      return false;
    }

    const auto next = t.at(action);

    m_nextActions.clear();
    std::transform(
        transitions.at(next).cbegin(), transitions.at(next).cend(),
        std::inserter(m_nextActions, m_nextActions.begin()),
        [](auto &n) -> std::string { return n.first; });

    DEBUG_TRANSPORT std::cout << "Tranport::nextState " << current << " + " << action << " -> " << next << std::endl;
    DEBUG_TRANSPORT for (auto &n : m_nextActions)
    {
      std::cout << " ?-> " << n << std::endl;
    }
    m_status->state = next;
    return true;
  }

  void Transport::prepare(const int Fs, const int bufsize, const bool realtime)
  {
    if (m_status->state == TransportState::STOPPED)
    {
      m_status->realtime = realtime;
      m_status->Fs = Fs;
      m_status->dt = 1.0 / Fs;
      m_status->bufsize = bufsize;
      m_status->t.resize(bufsize);

      m_status->pd = {
          // how much time do we have to render a buffer in milliseconds?
          .target = 1000.0 * bufsize / (double)Fs,
      };

      onSystemChanged(true);
    }
    else
    {
      std::cerr << "Transport::prepare cannot be called unless STOPPED" << std::endl;
    }
  }

  void Transport::step()
  {
    tbb::spin_mutex::scoped_lock lock(m_systemMutex);

    // Initialise performance data
    m_status->pd.start = now();

    // Update TimeVec
    for (connoise::Sample s = 0; s < m_status->bufsize; ++s)
    {
      m_status->t[s] = m_status->t0 + (s * m_status->dt);
    }

    // Run the system
    propagateConnections(m_status);

    // Update timing data
    m_status->t0 = m_status->t[m_status->bufsize - 1];

    // Update performance data
    const auto end = now();
    const auto renderms = std::chrono::duration<double, std::milli>(end - m_status->pd.start).count();
    m_status->pd.load = 100.0 * renderms / m_status->pd.target;
    DEBUG_TRANSPORT_THREADLOOP std::cout << "Transport::threadLoop frame render done;"
                                         << " t0=" << m_status->t0
                                         << " render time=" << renderms << "ms"
                                         << " budget used=" << m_status->pd.load << "%"
                                         << std::endl;

    // delaying here forces real-time rendering, which we may not want in case of rendering only to file
    if (m_status->realtime)
    {
      const auto runtime = std::chrono::duration<double, std::milli>(end - m_startTime).count();
      const auto drift = (m_status->t0 * 1000.0) - runtime;
      DEBUG_TRANSPORT_THREADLOOP std::cout << "Transport::threadLoop frame time drift " << drift << "ms" << std::endl;

      // eek, this feels tragically fragile :/
      const auto delaytime = std::max(m_status->pd.target - renderms + drift, 0.0);
      DEBUG_TRANSPORT_THREADLOOP std::cout << "Transport::threadLoop delay for " << delaytime << "ms" << std::endl;
      SDL_Delay(delaytime);
    }
  }

  void Transport::unprepare()
  {
    DEBUG_TRANSPORT std::cout << "Transport::unprepare (1)" << std::endl;
    if (m_status->state == TransportState::STOPPED)
    {
      DEBUG_TRANSPORT std::cout << "Transport::unprepare (2)" << std::endl;
      m_status->connections.clear();
      DEBUG_TRANSPORT std::cout << "Transport::unprepare (3)" << std::endl;
      for (auto &[_, m] : m_system->getModules())
      {
        m->unprepare();
      }
      DEBUG_TRANSPORT std::cout << "Transport::unprepare (4)" << std::endl;
    }
    else
    {
      std::cerr << "Transport::unprepare cannot be called unless STOPPED" << std::endl;
    }
    DEBUG_TRANSPORT std::cout << "Transport::unprepare (5)" << std::endl;
  }

  void Transport::threadLoop()
  {
    DEBUG_TRANSPORT_THREADLOOP std::cout << "Transport::threadLoop start" << std::endl;
    while (m_runThread)
    {
      // TODO: this is a good idea for realtime, to prevent 100% core use
      // TODO: however, why does non-realtime crash on stop() without this?
      SDL_Delay(1);
      
      if (m_status->state == TransportState::PLAYING)
      {
        step();
      }
    }
    DEBUG_TRANSPORT_THREADLOOP std::cout << "Transport::threadLoop end" << std::endl;
  }

  // TODO: can/should we implement this with the tbb flow graph library?
  void Transport::propagateConnections(const TransportStatus::shared_ptr rd)
  {
    auto cx = rd->connections;

    std::unordered_map<size_t, uint> connUse;

    DEBUG_TRANSPORT_CONNECTION std::cout << "propagateConnections: size=" << cx.size() << std::endl;
    for (auto &[level, dcx] : cx)
    {
      DEBUG_TRANSPORT_CONNECTION std::cout << "propagateConnections: reset flags at level " << level << std::endl;
      const auto resetStale = [&connUse](auto &c) {
        c.out->stale(true);
        c.in->stale(true);
        connUse[c.in->moduleId()]++;
      };
      std::for_each(std::execution::par_unseq, dcx.begin(), dcx.end(), resetStale);
    }

    const auto t = rd->t;

    for (DepthSortedConnections::reverse_iterator ri = cx.rbegin(); ri != cx.rend(); ++ri)
    {
      auto &[d, dcx] = *ri;
      DEBUG_TRANSPORT_CONNECTION std::cout << "propagateConnections: Processing connection depth " << d << std::endl;

      const auto runConnection = [this, &connUse, &t](auto &&c) {
        DEBUG_TRANSPORT_CONNECTION std::cout << " Connection: " << c.out->name() << " -> " << c.in->name() << std::endl;
        if (c.in->stale())
        {
          if (c.out->stale())
          {
            DEBUG_TRANSPORT_CONNECTION std::cout << "  fill buf "
                                                 << c.out->name() << "[" << c.out->buf.size() << "]"
                                                 << std::endl;
            c.out->fn(t);
            c.out->meter();
            c.out->stale(false);
          }
          else
          {
            DEBUG_TRANSPORT_CONNECTION std::cout << "  cached " << c.out->name() << std::endl;
          }
          
          DEBUG_TRANSPORT_CONNECTION std::cout << "  copy buf "
                                               << c.out->name() << "[" << c.out->buf.size() << "]"
                                               << " -> "
                                               << c.in->name() << "[" << c.in->buf.size() << "]" << std::endl;

          std::copy(c.out->buf.begin(), c.out->buf.end(), c.in->buf.begin());
          c.in->fn(t);
          c.in->meter();
          c.in->stale(false);
        }
        else
        {
          DEBUG_TRANSPORT_CONNECTION std::cout << "  OK " << c.in->name() << std::endl;
        }

        const auto mid = c.in->moduleId();
        auto m = m_system->getModule(mid);
        connUse[mid]--;
        if (connUse[mid] == 0)
        {
          DEBUG_TRANSPORT_CONNECTION std::cout << "propagateConnections: flush"
            << " " << m->name()
            << " caused by " << c.in->pid()
            << std::endl;
          m->flush();
        }
        else DEBUG_TRANSPORT_CONNECTION
        {
          std::cout << "propagateConnections: not ready to flush"
            << " " << m->name()
            << " caused by " << c.in->pid()
            << " (" << connUse[mid] << ")"
            << std::endl;
        }
      };

      // TODO: fix concurrency
      // std::for_each(std::execution::par_unseq, dcx.begin(), dcx.end(), runConnection);
      std::for_each(std::execution::seq, dcx.begin(), dcx.end(), runConnection);
    }
  }

} // namespace connoise
