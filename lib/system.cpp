/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#define DEBUG_SYSTEM if (false)

#include <iomanip>
#include <iostream>

#include "connection.h"
#include "system.h"

namespace connoise
{
  System::System()
  {
    DEBUG_SYSTEM std::cout << "System::System -------- new --------" << std::endl;
  }

  connoise::modules::Module::shared_ptr System::createModule(const size_t id, const std::string &type, const InitList &init)
  {
    using namespace connoise::modules::registry;

    if (m_modules.find(id) != m_modules.end())
    {
      std::cerr << "System::createModule module with ID " << id << " already created" << std::endl;
      return m_modules.at(id);
    }

    RegistryMap M;
    for (const auto m : Processors)
    {
      M.insert(m);
    }
    for (const auto m : Inputs)
    {
      M.insert(m);
    }
    for (const auto m : Outputs)
    {
      M.insert(m);
    }

    if (M.find(type) == M.end())
    {
      std::cerr << "System::createModule requested unknown type " << type << std::endl;
      return NULL;
    }

    auto m = M.at(type)(id, init);
    DEBUG_SYSTEM std::cout << "System::createModule(type=" << type << ", id=" << id << ", init=" << init.size() << ")" << std::endl;

    const auto mid = m->id();
    m_modules[mid] = m;

    m_model.ensureModule(m);

    return m;
  }

  bool System::deleteModule(const size_t id)
  {
    if (m_modules.find(id) == m_modules.end())
    {
      std::cerr << "System::deleteModule module with ID " << id << " does not exist" << std::endl;
      return false;
    }

    auto m = m_modules.at(id);
    m_model.deleteModule(m);
    m_modules.erase(id);
    return true;
  }

  void System::createConnection(connoise::modules::Module::shared_ptr mo, const std::string &output, connoise::modules::Module::shared_ptr mi, const std::string &input)
  {
    if (!mo)
    {
      std::cerr << "System::createConnection requested invalid output module " << std::endl;
      return;
    }
    if (!mo->hasOutput(output))
    {
      std::cerr << "System::createConnection requested invalid output " << mo->type() << "::" << output << std::endl;
      return;
    }
    if (!mi)
    {
      std::cerr << "System::createConnection requested invalid input module " << std::endl;
      return;
    }
    if (!mi->hasInput(input))
    {
      std::cerr << "System::createConnection requested invalid input " << mi->type() << "::" << input << std::endl;
      return;
    }

    auto moo = mo->getOutput(output);
    auto mii = mi->getInput(input);

    DEBUG_SYSTEM std::cout << "System::createConnection("
                           << moo->name()
                           << " -> "
                           << mii->name()
                           << ")" << std::endl;

    m_model.ensureModule(mo);
    m_model.ensurePort(connoise::model::NodeType::OUT_PORT, moo);
    m_model.ensurePort(connoise::model::NodeType::IN_PORT, mii);
    m_model.ensureModule(mi);

    m_model.ensureConnection(mo->mid(), moo->pid());
    m_model.ensureConnection(moo->pid(), mii->pid());
    m_model.ensureConnection(mii->pid(), mi->mid());
  }

  bool System::deleteConnection(connoise::modules::ModulePort::shared_ptr moo, connoise::modules::ModulePort::shared_ptr mii)
  {
    DEBUG_SYSTEM std::cout << "System::deleteConnection("
                           << moo->name()
                           << " -> "
                           << mii->name()
                           << ")" << std::endl;

    return m_model.deleteConnection(moo->pid(), mii->pid());
  }

  connoise::modules::Module::shared_ptr System::getModule(const size_t &id) const
  {
    if (m_modules.find(id) == m_modules.end())
    {
      std::cerr << "System::getModule requested unknown module ID " << id << std::endl;
      return NULL;
    }
    return m_modules.at(id);
  }

  std::ostream &operator<<(std::ostream &o, System::shared_ptr system)
  {
    for (const auto &[_, m] : system->getModules())
    {
      o << m << std::endl;
    }
    for (const auto &[_, cx] : system->getConnections())
    {
      for (const auto &c : cx)
      {
        o << "Connection {" << std::endl
          << "  " << c.out->moduleId() << " " << c.out->portName() << std::endl
          << "  " << c.in->moduleId() << " " << c.in->portName() << std::endl
          << "}" << std::endl
          << std::endl;
      }
    }
    o << "Metadata {" << std::endl;
    for (const auto &[k, v] : system->metadata())
    {
      o << "  " << k << " " << std::quoted(v) << std::endl;
    }
    o << "}" << std::endl
      << std::endl;
    return o;
  }

} // namespace connoise
