/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_TRANSPORT if (false)

#include <chrono>
#include <memory>
#include <unordered_set>

#include <tbb/spin_mutex.h>
#include <tbb/task_arena.h>

#include "system.h"
#include "types.h"

/*

Playback to SDL works in 1 of two ways; and we have challenges to implement either.


1. Push.

We would need the Transport to implement a timer, firing at the exact
frequency to push buffers into the audio device without missing samples.

We can calculate this frequency from the Fs and bufsize (=  bufsize / Fs).

The advantage though is that we need not worry about the System structure,
or what Modules it contains, we just push buffers through to wherever.
Also, we have a single clock source driving the entire system.

Disadvantage is that we are blindly relying on supplying enough data at
the correct rate, and have no feedback from the device as to whether we
meet our buffer delivery deadlines.


2. Pull.

No timers required here, the audio device calls us when it wants data;
in other words, we make the timer frequency problem of the Push method
someone else's problem.

However, we have a different problem here, that we need to get the callback
signal pushed through several layers from the AudioDevice module to the
Transport where we need it. Also, what happens if there's more than one
AudioDevice output in the System, should we expect multiple callbacks?!

-------------------------------------

[Transport] -- [System  [Modules{
    Processor, Processor, Processor,
    Output, Output
  }]]

-------------------------------------

Potentially we can resolve some of the above issues if the SDL audio
device is managed _here_ and we simply grab the AudioOutput's buffers
to give to the hardware? (In which case then the AudioOutput module
actually doesn't do anything...).

-------------------------------------

For now, we're implementing #1 Push - so that we don't break Module encapsulation.

*/

namespace connoise
{
  /**
   * @brief time_point alias used for run time performance measurement.
   *
   */
  using tp = std::chrono::steady_clock::time_point;

  /**
   * @brief Structure to hold runtime performance data.
   *
   */
  struct PerfData
  {
    /**
     * @brief Render iteration performance target duration.
     *
     */
    double target;

    /**
     * @brief time_point of the start of a render iteration.
     *
     */
    tp start;

    /**
     * @brief Percentage of rendering time budget used.
     * 
     */
    double load;
  };

  enum TransportState
  {
    STOPPED,
    PLAYING,
    PAUSED
  };

  using TransportStateTransitions = std::map<TransportState, std::map<std::string, TransportState>>;

  /**
   * @brief Structure to hold runtime data.
   *
   */
  struct TransportStatus
  {
    /**
     * @brief Shared pointer type for TransportStatus.
     * 
     */
    using shared_ptr = std::shared_ptr<TransportStatus>;

    /**
     * @brief Run time performance data.
     *
     */
    PerfData pd;

    /**
     * @brief The connections to be processed during rendering.
     *
     */
    DepthSortedConnections connections;

    /**
     * @brief Size of the buffer to be rendered in each iteration.
     *
     * This represents the number of samples.
     *
     */
    Sample bufsize;

    /**
     * @brief Absolute time value at the start of the rendering iteration.
     *
     */
    Time t0;

    /**
     * @brief Vector of time values to be rendered in the current iteration.
     *
     */
    connoise::TimeVec t;

    /**
     * @brief Are we going to render in real time?
     * 
     */
    bool realtime;

    /**
     * @brief The rendering sample rate.
     *
     */
    int Fs;

    /**
     * @brief The rendering delta-time (== 1/Fs).
     *
     */
    Value dt;

    /**
     * @brief Current state
     * 
     */
    TransportState state;

  public:
    /**
     * @brief Return a string representation of this status.
     * 
     * @return const std::string 
     */
    const std::string to_string() const
    {
      switch (state)
      {
      case TransportState::STOPPED:
        return "IDLE";
      case TransportState::PLAYING:
        return "RENDERING @ " + std::to_string(Fs) + " Hz; " + std::to_string(t0).substr(0, 5) + "s " + std::to_string(pd.load).substr(0, 5) + "%";
      case TransportState::PAUSED:
        return "PAUSED";
      default:
        return "";
      }
    }
  };

  /**
   * @brief System playback controller.
   * 
   */
  class Transport
  {
  public:
    /**
     * @brief Transport shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Transport>;

    /**
     * @brief Construct a new Transport object
     * 
     * @param system 
     */
    Transport(System::shared_ptr system);

    /**
     * @brief Destroy the Transport object
     * 
     */
    ~Transport();

    /**
     * @brief Return the current Transport status.
     * 
     * @return TransportStatus::shared_ptr 
     */
    TransportStatus::shared_ptr status() const
    {
      return m_status;
    }

    /**
     * @brief Start the playback from the beginning with the given rendering parameters.
     * 
     * @param Fs 
     * @param bufsize 
     * @param realtime 
     */
    void start(const int Fs, const int bufsize, const bool realtime);

    /**
     * @brief Stop and reset the playback state.
     * 
     */
    void stop();

    /**
     * @brief Pause the playback, without resetting state.
     * 
     */
    void pause();

    /**
     * @brief Resume the playback.
     * 
     */
    void resume();

    /**
     * @brief Compute next Transport state for a given action.
     * 
     * @param action 
     * @return true if the state changed
     * @return false if the state did not change
     */
    bool nextState(const std::string &action);

    /**
     * @brief Return a set of valid action which can be performed in the current state.
     * 
     * @return const std::unordered_set<std::string> 
     */
    const std::unordered_set<std::string> nextActions() const
    {
      return m_nextActions;
    }

    /**
     * @brief Prepare the Transport for rendering.
     * 
     * @param Fs 
     * @param bufsize 
     * @param realtime 
     */
    void prepare(const int Fs, const int bufsize, const bool realtime);

    /**
     * @brief Perform one time-step of rendering.
     * 
     */
    void step();

    /**
     * @brief To be called whenever something in the system has changed.
     * 
     */
    void onSystemChanged(const bool forceUpdate = false)
    {
      tbb::spin_mutex::scoped_lock lock(m_systemMutex);

      if (forceUpdate || m_status->state != TransportState::STOPPED)
      {
        DEBUG_TRANSPORT std::cout << "Transport::onSystemChanged() will prep modules " << std::endl;
        for (auto &[_, m] : m_system->getModules())
        {
          DEBUG_TRANSPORT std::cout << "Transport::onSystemChanged()  module " << m->name() << " is prepped? " << m->prepared() << std::endl;

          // Ensure new modules added whilst rendering are initialised!
          if (!m->prepared())
          {
            m->prepare(m_status->bufsize, m_status->Fs);
            DEBUG_TRANSPORT std::cout << "Transport::onSystemChanged() prepared module " << m->name() << std::endl;
          }
        }
      }

      m_status->connections = m_system->getConnections();
    }

    void stopThread()
    {
      DEBUG_TRANSPORT std::cout << "Transport::stopThread (1)" << std::endl;
      m_runThread = false;
      DEBUG_TRANSPORT std::cout << "Transport::stopThread (2)" << std::endl;
      stop();
      DEBUG_TRANSPORT std::cout << "Transport::stopThread (3)" << std::endl;
      m_arena.terminate();
      DEBUG_TRANSPORT std::cout << "Transport::stopThread (4)" << std::endl;
    }

  private:
    /**
     * @brief Clean up after rendering;
     * 
     */
    void unprepare();

    /**
     * @brief Transport-System processing loop thread
     * 
     */
    void threadLoop();

    /**
     * @brief Perform a synthesis iteration for the given state.
     *
     * @param rd
     */
    void propagateConnections(const TransportStatus::shared_ptr rd);

    /**
     * @brief Should the internal thread loop be running?
     * 
     */
    bool m_runThread;

    /**
     * @brief The system instance we are controlling
     * 
     */
    System::shared_ptr m_system;

    /**
     * @brief Rendering sample rate
     * 
     */
    int m_Fs;

    /**
     * @brief Rendering buffer size
     * 
     */
    size_t m_bufsize;

    /**
     * @brief Current time value
     * 
     */
    Value m_t;

    /**
     * @brief Current transport status
     * 
     */
    TransportStatus::shared_ptr m_status;

    /**
     * @brief Set of the next valid actions which can be called on Transport.
     * 
     */
    std::unordered_set<std::string> m_nextActions;

    /**
     * @brief Transport controlled System change mutex.
     * 
     */
    tbb::spin_mutex m_systemMutex;

    tbb::task_arena m_arena;
    tp m_startTime;
  };

} // namespace conniose
