/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE if (false)

#include <functional>
#include <iostream>
#include <list>
#include <math.h>
#include <memory>
#include <unordered_map>
#include <vector>

#include "meter.h"
#include "types.h"

namespace connoise
{
  enum ModuleClass
  {
    Processor,
    Output,
    Input
  };
} // namespace connoise

/**
 * @brief Macro to create std::bind of a Module function, by name.
 *
 */
#define CONNOISE_MODULE_BOUND_PORT_FUNCTION(c, m) std::bind(&c::m, this, std::placeholders::_1)

/**
 * @brief Macro to construct a call to Module::createOutput given only the Module type and function names.
 *
 */
#define CONNOISE_MODULE_CREATE_OUTPUT(c, m) createOutput(#m, CONNOISE_MODULE_BOUND_PORT_FUNCTION(c, m))

/**
 * @brief Macro to construct a call to Module::createInput given only the Module type and function names.
 *
 */
#define CONNOISE_MODULE_CREATE_INPUT(c, m) createInput(#m, CONNOISE_MODULE_BOUND_PORT_FUNCTION(c, m))

/**
 * @brief Namespace for all Output Modules.
 *
 */
namespace connoise::outputs
{
}

/**
 * @brief Namespace for all Modules.
 *
 */
namespace connoise::modules
{
  /**
   * @brief Function type for a Module I/O port.
   *
   */
  using ModulePortFunction = std::function<const Buffer &(const TimeVec &t)>;

  /**
   * @brief Values which uniquely identify a Module.
   *
   */
  struct ModuleIdentifier
  {
    /**
     * @brief Shared pointer type for ModuleIdentifier.
     * 
     */
    using shared_ptr = std::shared_ptr<ModuleIdentifier>;

    /**
     * @brief Construct a new Module Identifier object.
     * 
     * @param id 
     * @param type 
     */
    ModuleIdentifier(const size_t id, const std::string &type)
        : id(id), type(type)
    {
    }

    /**
     * @brief Type of the module.
     *
     */
    const std::string type;

    /**
     * @brief Id value for the Module.
     *
     */
    const size_t id;
  };

  /**
   * @brief Represents an IO port on a Module.
   *
   */
  class ModulePort
  {
  public:
    /**
     * @brief ModulePort shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<ModulePort>;

    /**
     * @brief Construct a new Module Port object.
     *
     * @param mid
     * @param portName
     * @param stale
     * @param fn
     */
    ModulePort(
        ModuleIdentifier::shared_ptr mid,
        const std::string &portName,
        bool stale,
        const ModulePortFunction &fn)
        : m_mid(mid),
          m_portName(portName),
          m_stale(stale),
          fn(fn)
    {
      buf.resize(0);
    }
    

    void init(const size_t bufferSize, const Value iv)
    {
      buf.resize(bufferSize);
      std::fill(buf.begin(), buf.end(), iv);
      m_meter.reset(bufferSize);
    }

    /**
     * @brief Getter for this port's Module's type.
     *
     * @return const std::string
     */
    const std::string moduleType() const
    {
      return m_mid->type;
    }

    /**
     * @brief Getter for this port's Module's ID.
     *
     * @return const size_t
     */
    const size_t moduleId() const
    {
      return m_mid->id;
    }

    /**
     * @brief Getter for this port's name.
     *
     * @return const std::string
     */
    const std::string portName() const
    {
      return m_portName;
    }

    /**
     * @brief Return the port human-readable name.
     *
     * @return const std::string
     */
    const std::string name() const
    {
      return m_mid->type + "[" + std::to_string(m_mid->id) + "]::" + m_portName;
    }

    /**
     * @brief Get this port's identifier string.
     *
     * @return const std::string
     */
    const std::string pid() const
    {
      return m_mid->type + "::" + std::to_string(m_mid->id) + "::" + m_portName;
    }

    /**
     * @brief Setter for port m_stale member.
     *
     * @param s
     */
    void stale(const bool s)
    {
      m_stale = s;
    }

    /**
     * @brief Getter for m_stale member.
     *
     * @return true
     * @return false
     */
    bool stale() const
    {
      return m_stale;
    }

    /**
     * @brief Calculate RMS of this port's audio buffer.
     * 
     */
    void meter()
    {
      m_meter.update(buf.data(), buf.size());
      rms = m_meter.peak;
    }

  public:
    /**
     * @brief This port's audio buffer.
     *
     */
    Buffer buf;

    /**
     * @brief RMS value of this port's audio buffer.
     *
     */
    Value rms;

    /**
     * @brief This port's function.
     *
     */
    ModulePortFunction fn;

  private:
    /**
     * @brief The identifier for this port's owning Module.
     *
     */
    ModuleIdentifier::shared_ptr m_mid;

    /**
     * @brief The actual name of this port.
     *
     */
    std::string m_portName;

    /**
     * @brief This port's staleness status.
     *
     * Where staleness means whether this port has been processed in the current frame.
     *
     */
    bool m_stale;

    connoise::Meter<Value> m_meter;
  };

  /**
   * @brief Base class for Module in the system.
   *
   */
  class Module
  {
  public:
    /**
     * @brief Module shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Module>;

    /**
     * @brief Construct a new Module object.
     *
     * @param cls
     * @param type
     * @param id
     */
    explicit Module(const ModuleClass &cls, const std::string &type, const size_t id)
        : m_class(cls),
          m_mid(std::make_shared<ModuleIdentifier>(id, type)),
          m_prepared(false)
    {
    }

    /**
     * @brief Get the Class of this Module.
     *
     * @return ModuleClass
     */
    ModuleClass getClass() const
    {
      return m_class;
    }

    /**
     * @brief Get the Type string.
     *
     * @return const std::string
     */
    const std::string type() const
    {
      return m_mid->type;
    }

    /**
     * @brief Get the ID numbertring.
     *
     * @return const size_t
     */
    const size_t id() const
    {
      return m_mid->id;
    }

    /**
     * @brief Getter for this Module's param keys.
     *
     * Get the pairs in params order; see Module::getInputs()/getOutputs()
     *
     * @return std::vector<std::string>
     */
    std::vector<std::string> paramsKeys()
    {
      std::vector<std::string> keys;
      std::transform(m_params.begin(), m_params.end(), std::back_inserter(keys), [](auto &p) { return p.first; });
      std::reverse(keys.begin(), keys.end());
      return keys;
    }

    /**
     * @brief Return a parameter InitValue ref by key.
     *
     * @param key
     * @return InitValue&
     */
    InitValue &param(const std::string &key)
    {
      return m_params.at(key);
    }

    /**
     * @brief Get ref to a string parameter.
     *
     * @return std::string
     */
    std::string stringParam(const std::string &key) const
    {
      std::string empty;
      if (m_params.find(key) != m_params.end())
      {
        const auto vv = m_params.at(key);
        if (std::holds_alternative<std::string>(vv))
        {
          return std::get<std::string>(vv);
        }
        else
        {
          return empty;
        }
      }
      return empty;
    }

    /**
     * @brief Get a Value parameter.
     *
     * @return Value
     */
    Value valueParam(const std::string &key) const
    {
      Value empty = 0.0;
      if (m_params.find(key) != m_params.end())
      {
        const auto vv = m_params.at(key);
        if (std::holds_alternative<Value>(vv))
        {
          return std::get<Value>(vv);
        }
        else
        {
          return empty;
        }
      }
      return empty;
    }

    /**
     * @brief Get this Module's human-readable name.
     *
     * @return const std::string
     */
    const std::string name() const
    {
      return m_mid->type + "[" + std::to_string(m_mid->id) + "/" + stringParam("label") + "]";
    }

    /**
     * @brief Get this Module's identifier string.
     *
     * @return const std::string
     */
    const std::string mid() const
    {
      return m_mid->type + "::" + std::to_string(m_mid->id);
    }

    /**
     * @brief Get all of this Module's Outputs.
     *
     * @return std::vector<std::string>
     */
    std::vector<std::string> getOutputs() const
    {
      std::vector<std::string> i;
      std::transform(m_outputs.begin(), m_outputs.end(), std::back_inserter(i), [](auto &op) { return op.first; });
      // Note that iteration over unordered_map has *undefined* order,
      // however in practice, for the small number of items used in Modules,
      // and with gcc at least, they appear to come out in reverse-insertion
      // order, so we'll reverse this list to match how a Module defines its ports.
      std::reverse(i.begin(), i.end());
      return i;
    }

    /**
     * @brief Return whether this Module has an output port with the given name.
     *
     * @param name
     * @return true
     * @return false
     */
    bool hasOutput(const std::string &name) const
    {
      return (m_outputs.find(name) != m_outputs.end());
    }

    /**
     * @brief Get the Output port object shared ptr by name.
     *
     * @param name
     * @return ModulePort::shared_ptr
     */
    ModulePort::shared_ptr getOutput(const std::string &name)
    {
      return m_outputs[name];
    };

    /**
     * @brief Get all of this Module's Inputs.
     *
     * @return std::vector<std::string>
     */
    std::vector<std::string> getInputs() const
    {
      std::vector<std::string> i;
      std::transform(m_inputs.begin(), m_inputs.end(), std::back_inserter(i), [](auto &op) { return op.first; });
      // Note that iteration over unordered_map has *undefined* order,
      // however in practice, for the small number of items used in Modules,
      // and with gcc at least, they appear to come out in reverse-insertion
      // order, so we'll reverse this list to match how a Module defines its ports.
      std::reverse(i.begin(), i.end());
      return i;
    }

    /**
     * @brief Return whether this Module has an input port with the given name.
     *
     * @param name
     * @return true
     * @return false
     */
    bool hasInput(const std::string &name) const
    {
      return (m_inputs.find(name) != m_inputs.end());
    }

    /**
     * @brief Get the Input port object shared ptr by name.
     *
     * @param name
     * @return ModulePort::shared_ptr
     */
    ModulePort::shared_ptr getInput(const std::string &name)
    {
      return m_inputs[name];
    };

    /**
     * @brief To be called when a parameter has changed, to allow the Module to update internal state.
     * 
     * @param param 
     * @param t 
     */
    virtual void onParameterChanged(const std::string &param, const TimeVec &t)
    {
      if (m_inputs.find(param) == m_inputs.end())
      {
        std::cerr << name() << "::onParameterChanged cannot process unknown input " << param << std::endl;
        return;
      }
      auto port = m_inputs.at(param);
      const auto init = valueParam(param);
      DEBUG_MODULE std::cout << name() << "::onParameterChanged " << port->name() << " get init value " << init << std::endl;
      port->init(port->buf.size(), init);
      DEBUG_MODULE std::cout << name() << "::onParameterChanged " << port->name() << " filled buffer " << std::endl;
      port->fn(t);
      DEBUG_MODULE std::cout << name() << "::onParameterChanged " << port->name() << " called fn " << std::endl;
    }

    /**
     * @brief Prepare this Module for processing.
     *
     * Should be called just once before a rendering.
     *
     * @param bufferSize
     * @param Fs
     */
    virtual void prepare(const size_t bufferSize, const int Fs)
    {
      for (auto &[_, port] : m_outputs)
      {
        DEBUG_MODULE std::cout << name() << "::prepare output " << port->name() << " set buffers to " << bufferSize << std::endl;
        port->init(bufferSize, 0);
      }

      TimeVec t;
      t.resize(bufferSize);
      std::fill(t.begin(), t.end(), 0);

      for (auto &[_, port] : m_inputs)
      {
        const auto portName = port->name();
        port->init(bufferSize, 0);
        DEBUG_MODULE std::cout << name() << "::prepare input " << portName << " set buffers to " << bufferSize << std::endl;
        const auto param = port->portName();
        if (m_params.find(param) != m_params.end())
        {
          onParameterChanged(param, t);
        }
        else
        {
          DEBUG_MODULE std::cout << name() << "::prepare no init value found for param " << portName << std::endl;
        }
      }

      m_prepared = true;
    }

    /**
     * @brief Clean up after processing.
     * 
     * Clears/resets buffers and values etc.
     * 
     */
    virtual void unprepare()
    {
      for (auto &[_, port] : m_outputs)
      {
        port->init(0, 0);
      }

      for (auto &[_, port] : m_inputs)
      {
        port->init(0, 0);
      }

      m_prepared = false;
    }

    /**
     * @brief Return whether this Module has been prepared for rendering.
     * 
     * @return true 
     * @return false 
     */
    const bool prepared() const
    {
      return m_prepared;
    }

    virtual void flush()
    {
      // No-op by default;
    }

  protected:
    /**
     * @brief Initialise this Module's inputs and parameters.
     *
     * \todo the implementation of this could be better/smarter
     *
     * @param init
     * @param defaults
     */
    void initialise(const InitList &init, const InitList &defaults)
    {
      // Every module should have a label; defaults to the ID as string
      m_params["label"] = std::to_string(m_mid->id);

      for (auto &[param, vv] : defaults)
      {
        m_params[param] = vv;
      }

      // TODO: should not parse invalid params; Modules should define valid params.

      for (auto &[param, val] : init)
      {
        m_params[param] = val;
      }
    }

    /**
     * @brief Create a named Output port object in this Module.
     *
     * @param name
     * @param fn
     */
    void createOutput(const std::string &name, const ModulePortFunction &fn)
    {
      DEBUG_MODULE std::cout << this->name() << "::createOutput " << m_mid->type << "[" << m_mid->id << "] -> " << name << std::endl;
      auto port = std::make_shared<ModulePort>(m_mid, name, true, fn);
      m_outputs[name] = port;
    };

    /**
     * @brief Create a named Input port object in this Module.
     *
     * @param name
     * @param fn
     */
    void createInput(const std::string &name, const ModulePortFunction &fn)
    {
      DEBUG_MODULE std::cout << this->name() << "::createInput " << m_mid->type << "[" << m_mid->id << "] <- " << name << std::endl;
      auto port = std::make_shared<ModulePort>(m_mid, name, true, fn);
      m_inputs[name] = port;
    }

    /**
     * @brief This Module's class.
     *
     * e.g. Processor, Output ...
     *
     */
    ModuleClass m_class;

    /**
     * @brief Identification values for this module.
     *
     */
    ModuleIdentifier::shared_ptr m_mid;

    /**
     * @brief This Module's Output ports, mapped by the port name.
     *
     */
    std::unordered_map<std::string, ModulePort::shared_ptr> m_outputs;
    /**
     * @brief This Module's Input ports, mapped by the port name.
     *
     */
    std::unordered_map<std::string, ModulePort::shared_ptr> m_inputs;

    /**
     * @brief This module's params; mutable.
     *
     */
    std::unordered_map<std::string, InitValue> m_params;

    /**
     * @brief Whether this Module has been prepare()d
     * 
     */
    bool m_prepared;
  };

} // namespace connoise::modules
