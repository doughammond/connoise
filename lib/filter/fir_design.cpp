/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "fir_design.h"

namespace connoise::filter::fir
{
  namespace KaiserBessel
  {
    /**
     * @brief This function calculates the zeroth order Bessel function.
     *
     * @return Value
     */
    Value Ino(const Value x)
    {
      Value d = 0;
      Value ds = 1;
      Value s = 1;
      Value xx = x * x;
      do
      {
        d += 2;
        ds *= xx / (d * d);
        s += ds;
      } while (ds > s * 1e-6);
      return s;
    }

    Buffer calculate(const Value Fs, const Value Fa, const Value Fb, const size_t M, const Value Att)
    {
      const auto Np = (M - 1) / 2;
      size_t j = 0;
      Buffer A(M, 0);
      Value Alpha;
      Buffer H(M, 0);

      const auto fa2pi_fs = 2.0 * M_PI * Fa / Fs;
      const auto fb2pi_fs = 2.0 * M_PI * Fb / Fs;

      // Calculate the impulse response of the ideal filter
      A[0] = 2.0 * (Fb - Fa) / Fs;
      for (j = 1; j <= Np; ++j)
      {
        const auto jpi = j * M_PI;
        A[j] = (std::sin(j * fb2pi_fs) - std::sin(j * fa2pi_fs)) / jpi;
      }

      // Calculate the desired shape factor for the Kaiser-Bessel window
      if (Att < 21.0)
      {
        Alpha = 0;
      }
      else if (Att > 50.0)
      {
        Alpha = 0.1102 * (Att - 8.7);
      }
      else
      {
        Alpha = 0.5842 * std::pow((Att - 21.0), 0.4) + 0.07886 * (Att - 21.0);
      }

      // Window the ideal response with the Kaiser-Bessel window
      const auto Inoalpha = Ino(Alpha);
      const auto NpNp = Np * Np;
      for (j = 0; j <= Np; j++)
      {
        H[Np - j] = H[Np + j] = A[j] * Ino(Alpha * std::sqrt(1.0 - (j * j / NpNp))) / Inoalpha;
      }

      return H;
    }
  } // namespace KaiserBessel
} // namespace connoise::filter::fir
