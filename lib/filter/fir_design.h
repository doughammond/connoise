/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "../types.h"

namespace connoise::filter::fir
{
  namespace KaiserBessel
  {
    /*
     * This function calculates Kaiser windowed
     * FIR filter coefficients for a single passband
     * based on
     * "DIGITAL SIGNAL PROCESSING, II" IEEE Press pp 123-126.
     *
     * Translated to C++ from implementation on http://www.arc.id.au/FilterDesign.html
     * By Doug Hammond
     *
     * Fs=Sampling frequency
     * Fa=Low freq ideal cut off (0=low pass)
     * Fb=High freq ideal cut off (Fs/2=high pass)
     * Att=Minimum stop band attenuation (>21dB)
     * M=Number of points in filter (ODD number)
     * H[] holds the output coefficients (they are symetric only half generated)
     */
    Buffer calculate(const Value Fs, const Value Fa, const Value Fb, const size_t M, const Value Att);

  } // namespace KaiserBessel

} // namespace connoise::filter::fir
