/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <vector>

namespace connoise {

#define DB_MIN (-30)

template <class Numeric>
class Meter
{
public:
	// These are the readable stats members, public for easy access
	Numeric peak;		// in range 0.0 - 1.0;
	Numeric peakdB;		// in range DB_MIN - 0.f
	Numeric RMS;		// in range 0.0 - 1.0;
	Numeric RMSdB;		// in range DB_MIN - 0.f

	Meter();
	~Meter();
	
	void update(const Numeric* buffer, const int len);
	void reset(const int bufferSize);

private:
	Numeric dbClamp(const Numeric db);

	Numeric sum_sqr;
	std::vector<Numeric> sample_sqr_history;
	int RMS_len;
	int RMS_ctr;
};

template <class Numeric>
Meter<Numeric>::Meter()
	: peak(0), peakdB(DB_MIN),
	  RMS(0), RMSdB(DB_MIN),
	  sum_sqr(0.f),
	  RMS_ctr(0)
{
	reset(0);
}

template <class Numeric>
Meter<Numeric>::~Meter()
{
}

template <class Numeric>
void Meter<Numeric>::reset(const int bufferSize)
{
    RMS_len = bufferSize;
	sample_sqr_history.clear();
	sample_sqr_history.resize(bufferSize);
    std::fill(sample_sqr_history.begin(), sample_sqr_history.end(), 0);
	sum_sqr = (Numeric)0.f;
	RMS_ctr = 0;
}

template <class Numeric>
void Meter<Numeric>::update(const Numeric* buffer, const int len)
{
	Numeric absval = peak = 0.;

	for(int i=0; i<len; ++i)
	{
		absval = (Numeric)abs( buffer[i] );

		if (absval > peak)
			peak = absval;

		// subtract oldest sqr from sum
		sum_sqr -= sample_sqr_history[RMS_ctr];

		// calculate and accumulate new value
		sum_sqr += sample_sqr_history[RMS_ctr] = absval*absval;
		
		RMS_ctr = (RMS_ctr+1)%RMS_len;
	}
	
	peakdB = dbClamp(10.f*log10(peak));

	RMS = sqrt(sum_sqr/RMS_len);
	RMSdB = dbClamp(10.f*log10(RMS));
}

template <class Numeric>
Numeric Meter<Numeric>::dbClamp(const Numeric db)
{
	if (db<DB_MIN) return DB_MIN;
	if (db>0.f) return 0;
	return db;
}

} // namespace connoise