/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_OUTPUT_WAVFILE if (false)

#include <string>

#include <iostream>
#include <memory>
#include <sndfile.h>

#include "../module.h"

namespace connoise::outputs
{

  /**
   * @brief Wav file output module.
   *
   * Writes the input Signal to the specified file.
   *
   * This module will write a single channel 24bit wav file using the system sample rate.
   *
   * Params:
   *
   * - `path` : QuotedString, Full path to the wav file to write
   *
   * Inputs:
   *
   * - `u` : Signal
   *
   */
  class WavFile : public connoise::modules::Module
  {
  public:
    /**
     * @brief WavFile shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<WavFile>;

    /**
     * @brief Construct a new WavFile Module.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    WavFile(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override;

    /**
     * @brief Close the wav file.
     *
     */
    void closeFile();

    ~WavFile();

  private:
    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &u(const TimeVec &t);

    // Internal

    /**
     * @brief Pointer to the libsndfile structure for writing the file.
     *
     */
    SNDFILE *m_sf = nullptr;
  };

} // namespace connoise::outputs
