/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_OUTPUT_AUDIODEVICE if (false)

#include <memory>

#include <SDL2/SDL.h>

#include "../module.h"

namespace connoise::outputs
{

  /**
   * @brief Audio device output module.
   *
   * Writes the input Signal to the default audio device, using SDL2.
   *
   * Inputs:
   *
   * - `L` : Signal
   * - `R` : Signal
   *
   */
  class AudioDevice : public connoise::modules::Module
  {
  public:
    /**
     * @brief AudioDevice shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<AudioDevice>;

    /**
     * @brief Construct a new AudioDevice Module.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    AudioDevice(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override;
    virtual void unprepare() override;

    ~AudioDevice();

    virtual void flush();

  private:
    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &L(const TimeVec &t);

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &R(const TimeVec &t);

    // Internal

    /**
     * @brief Cache of the actual output sample rate.
     *
     */
    int m_Fs;

    /**
     * @brief Reference to the output audio device.
     *
     */
    SDL_AudioDeviceID m_dev;
  };

} // namespace connoise::outputs
