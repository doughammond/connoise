/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "audio.h"

namespace connoise::outputs
{
  AudioDevice::AudioDevice(const size_t id, const InitList &init)
      : Module(ModuleClass::Output, "AudioDevice", id)
  {
    m_dev = 0;
    CONNOISE_MODULE_CREATE_INPUT(AudioDevice, L);
    CONNOISE_MODULE_CREATE_INPUT(AudioDevice, R);

    initialise(init, {});
  }

  void AudioDevice::prepare(const size_t bufferSize, const int Fs)
  {
    if (SDL_Init(SDL_INIT_AUDIO) != 0)
    {
      std::cerr << "Unable to initialize SDL: " << SDL_GetError() << std::endl;
    }

    SDL_AudioSpec want, have;
    SDL_zero(want);
    want.freq = Fs;
    want.format = AUDIO_F32;
    want.channels = 2;
    want.samples = bufferSize;

    if (m_dev == 0)
    {
      m_dev = SDL_OpenAudioDevice(NULL, false, &want, &have, 0);
      std::cerr << "Opened audio device:"
                << " dev=" << m_dev
                << " Fs=" << have.freq
                << " Format=" << have.format
                << " Channels=" << (int)have.channels
                << " Samples=" << have.samples << std::endl;
      m_Fs = have.freq;
    }

    Module::prepare(bufferSize, m_Fs);
  }

  void AudioDevice::unprepare()
  {
    Module::unprepare();

    if (m_dev != 0)
    {
      SDL_PauseAudioDevice(m_dev, 1);
      DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::unprepare clearing queue..." << std::endl;
      SDL_ClearQueuedAudio(m_dev);

      const auto qsize = SDL_GetQueuedAudioSize(m_dev);

      DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::unprepare q size=" << qsize << " closing device..." << std::endl;

      SDL_CloseAudioDevice(m_dev);
      m_dev = 0;
      DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::unprepare ...done" << std::endl;
    }
  }

  AudioDevice::~AudioDevice()
  {
    unprepare();
  }

  const Buffer &AudioDevice::L(const TimeVec &t)
  {
    const auto &buf = getInput("L")->buf;
    // Nothing here, the device is updated on flush
    return buf;
  }

  const Buffer &AudioDevice::R(const TimeVec &t)
  {
    const auto &buf = getInput("R")->buf;
    // Nothing here, the device is updated on flush
    return buf;
  }

  void AudioDevice::flush()
  {
    DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::flush" << std::endl;

    const auto &bufL = getInput("L")->buf;
    const auto &bufR = getInput("R")->buf;

    // Copy output to SDL using "push"
    // TODO: this is essentially a pass-through now; can we avoid this?
    auto istream = SDL_NewAudioStream(AUDIO_F32, 2, m_Fs, AUDIO_F32, 2, m_Fs);
    bool isClipped = false;
    std::vector<float> buf;
    for (connoise::Sample s = 0; s < bufL.size(); ++s)
    {
      const float bdL = bufL[s];
      isClipped |= bdL > 1.0 || bdL < -1.0;

      const float bdR = bufR[s];
      isClipped |= bdR > 1.0 || bdR < -1.0;

      buf.push_back(bdL);
      buf.push_back(bdR);
    }

    const auto put = SDL_AudioStreamPut(istream, &buf[0], buf.size() * sizeof(float));
    DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::flush loop put=" << put << std::endl;

    const auto fret = SDL_AudioStreamFlush(istream);
    const auto len = SDL_AudioStreamAvailable(istream);

    std::vector<Uint8> data;
    data.resize(len);
    SDL_AudioStreamGet(istream, &data[0], len);

    DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::flush got fret " << fret << " stream " << len << " bytes" << std::endl;

    if (isClipped)
    {
      std::cerr << name() << "::flush detected audio CLIPPING" << std::endl;
    }

    const auto deviceStatus = SDL_GetAudioDeviceStatus(m_dev);
    if (deviceStatus == SDL_AUDIO_PAUSED)
    {
      SDL_PauseAudioDevice(m_dev, 0);
    }

    const auto queued = SDL_QueueAudio(m_dev, &data[0], len);
    DEBUG_OUTPUT_AUDIODEVICE std::cout << name() << "::flush queued " << queued << " bytes" << std::endl;

    SDL_FreeAudioStream(istream);
  }

} // namespace connoise::outputs
