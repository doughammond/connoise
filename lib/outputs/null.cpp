/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "null.h"

namespace connoise::outputs
{
  NullOutput::NullOutput(const size_t id, const InitList &init)
      : Module(ModuleClass::Output, "NullOutput", id)
  {
    CONNOISE_MODULE_CREATE_INPUT(NullOutput, u);
    initialise(init, {});
  }

  const Buffer &NullOutput::u(const TimeVec &t)
  {
    const auto &buf = getInput("u")->buf;
    return buf;
  }

} // namespace connoise::outputs
