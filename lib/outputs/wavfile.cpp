/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "wavfile.h"

namespace connoise::outputs
{
  WavFile::WavFile(const size_t id, const InitList &init)
      : Module(ModuleClass::Output, "WavFile", id)
  {
    CONNOISE_MODULE_CREATE_INPUT(WavFile, u);

    initialise(init, {
                         {"path", ""},
                     });
  }

  void WavFile::prepare(const size_t bufferSize, const int Fs)
  {
    closeFile();

    SF_INFO sfinfo{
        .samplerate = Fs,
        .channels = 1,
        .format = SF_FORMAT_WAV | SF_FORMAT_PCM_24,
    };
    auto path = stringParam("path");
    m_sf = sf_open(path.c_str(), SFM_WRITE, &sfinfo);

    if (m_sf == nullptr)
    {
      std::cerr << name() << "::prepare error opening " << path << " : " << sf_strerror(NULL) << std::endl;
    }

    DEBUG_OUTPUT_WAVFILE std::cout << name() << "::prepare opened file at " << path << std::endl;

    Module::prepare(bufferSize, Fs);
  }

  void WavFile::closeFile()
  {
    if (m_sf != nullptr)
    {
      const auto r = sf_close(m_sf);
      if (r != 0)
      {
        std::cerr << name() << "::~WavFile error closing file: error" << r << std::endl;
      }

      m_sf = nullptr;

      DEBUG_OUTPUT_WAVFILE std::cout << name() << "::~WavFile closed file" << std::endl;
    }
  }

  WavFile::~WavFile()
  {
    closeFile();
  }

  const Buffer &WavFile::u(const TimeVec &t)
  {
    const auto &buf = getInput("u")->buf;
    if (m_sf != nullptr)
    {
      const auto bs = buf.size();
      const auto c = sf_write_double(
          m_sf,
          &buf[0],
          bs);

      if (c != bs)
      {
        std::cerr << name() << "::u did not write entire buffer:" << c << " / " << bs << std::endl;
      }

      DEBUG_OUTPUT_WAVFILE std::cout << name() << "::u wrote buffer length " << bs << std::endl;
    }
    return buf;
  }

} // namespace connoise::outputs
