/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::outputs
{
  /**
   * @brief Null output module.
   *
   * Does nothing except sink signals.
   *
   * Inputs:
   *
   * - `u` : Signal
   *
   */
  class NullOutput : public connoise::modules::Module
  {
  public:
    /**
     * @brief NullOutput shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<NullOutput>;

    /**
     * @brief Construct a new NullOutput Module.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    NullOutput(const size_t id, const InitList &init);

  private:
    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &u(const TimeVec &t);
  };

} // namespace connoise::outputs
