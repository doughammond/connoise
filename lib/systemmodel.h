/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_SYSTEMMODEL if (false)

#include <functional>
#include <set>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <tbb/spin_mutex.h>

#include "connection.h"
#include "module.h"

/**
 * @brief Namespace for abstract modelling of the Synthesizer system.
 *
 */
namespace connoise::model
{
  /**
   * @brief Type of a Node in the SystemModel.
   *
   */
  enum NodeType
  {
    /**
     * @brief UNKNOWN, the default NodeType, indicates an error with Node construction.
     *
     */
    UNDEFINED,
    /**
     * @brief The Node represents a Module.
     *
     */
    MODULE,
    /**
     * @brief The Node represents a ModuleOutput.
     *
     */
    OUT_PORT,
    /**
     * @brief The Node represents a ModuleInput.
     *
     */
    IN_PORT
  };

  /**
   * @brief Structure to hold the model's Node data.
   *
   */
  struct Node
  {
    /**
     * @brief The class of the Module in this Node.
     *
     */
    ModuleClass moduleClass;
    /**
     * @brief The type of this Node.
     *
     */
    NodeType type;
    /**
     * @brief If type == NodeType::MODULE, a reference to the Module object.
     *
     */
    connoise::modules::Module::shared_ptr m;
    /**
     * @brief If type in (NodeType::OUT_PORT, NodeType::IN_PORT), a reference to the ModulePort object.
     *
     */
    connoise::modules::ModulePort::shared_ptr port;

    /**
     * @brief Test equality with another Node.
     *
     * @param other
     * @return true
     * @return false
     */
    bool operator==(const Node &other)
    {
      return type == other.type && m == other.m && port == other.port;
    }

    /**
     * @brief This Node's ID as a string.
     *
     * @return const size_t
     */
    const std::string id() const
    {
      switch (type)
      {
      case NodeType::MODULE:
        return m ? m->mid() : "";
      case NodeType::OUT_PORT:
      case NodeType::IN_PORT:
        return port ? port->pid() : "";
      default:
        return "";
      }
    }

    /**
     * @brief This Node's name.
     *
     * @return const std::string
     */
    const std::string name() const
    {
      switch (type)
      {
      case NodeType::MODULE:
        return m ? m->name() : "";
      case NodeType::OUT_PORT:
      case NodeType::IN_PORT:
        return port ? port->name() : "";
      default:
        return "";
      }
    }
  };

  /**
   * @brief The Graph Adjacency List type.
   *
   */
  using Graph = boost::adjacency_list<
      boost::vecS,
      boost::vecS,
      boost::bidirectionalS,
      Node>;

  /**
   * @brief The Graph Edge descriptor type.
   *
   */
  using GraphEdgeDescr = boost::graph_traits<Graph>::edge_descriptor;

  /**
   * @brief The Graph Vertex descriptor type.
   *
   */
  using GraphVertDescr = boost::graph_traits<Graph>::vertex_descriptor;

  /**
   * @brief The Graph Vertex in-Edge iterator type.
   *
   */
  using GraphInEdgeIt = boost::graph_traits<Graph>::in_edge_iterator;

  /**
   * @brief Abstract representation of a Synthesizer system.
   *
   * Models a directed graph, where the Nodes (\see NodeType) are
   * decomposed parts of Modules and the Edges are the connections
   * between them.
   *
   * Implements the graph algorithms needed for the correct calculation
   * of the synthesized audio.
   *
   */
  class SystemModel
  {
  public:
    SystemModel()
    {
    }

    /**
     * @brief Return string representation of NodeType.
     *
     * @param type
     * @return const std::string
     */
    const std::string nodeTypeName(const NodeType &type) const
    {
      switch (type)
      {
      case NodeType::MODULE:
        return "MODULE";
      case NodeType::OUT_PORT:
        return "OUT_PORT";
      case NodeType::IN_PORT:
        return "IN_PORT";
      default:
        return "UNKNOWN";
      }
    }

    /**
     * @brief Factory function for creating a Module or Output in the model.
     *
     * Ensures the Module exists once and only once.
     *
     */
    void ensureModule(connoise::modules::Module::shared_ptr m);

    /**
     * @brief Delete a Module from the model; including:
     * - all it's edges
     * - its input and output port nodes
     * - its module node
     *
     * @param m
     * @return true
     * @return false
     */
    bool deleteModule(connoise::modules::Module::shared_ptr m);

    /**
     * @brief Factory function for creating a ModulePort in the model.
     *
     * Ensures the ModulePort exists once and only once.
     *
     */
    void ensurePort(const NodeType type, connoise::modules::ModulePort::shared_ptr port);

    /**
     * @brief Factory function for creating a Connection in the model.
     *
     * Ensures the Connection exists once and only once.
     *
     */
    void ensureConnection(const std::string &source, const std::string &target);

    /**
     * @brief Delete a connection from the model.
     *
     * Note that we can only remove inter-module connections. All intra-module connections are internally managed.
     *
     * @param source ID of the source Port
     * @param target ID of the target Port
     * @return true
     * @return false
     */
    bool deleteConnection(const std::string &source, const std::string &target);

    /**
     * @brief Calculate the order in which System Connections should be processed.
     *
     * Runs an algorithm on the Graph model, to determine the Connection processing order.
     * Starts with the Connetions furthest from the System output, and groups them by
     * depth.
     * Also ensures that Connections in each depth level can be processed in parallel
     * without concurrency or race conditions, by promoting Connections to a higher
     * level as required.
     * Note that this will only return connections which lead to a system Output.
     *
     * @return const DepthSortedConnections
     */
    const DepthSortedConnections calculateConnectionOrder();

    /**
     * @brief Get all of the connections in the System in no particular order.
     *
     * @return const Connections
     */
    const Connections getAllConnections();

    /**
     * @brief Print a graphviz dot representation of the model to stdout.
     *
     * @param name
     */
    void printDot(const std::string &name);

  private:
    /**
     * @brief Delete a ModulePort from the model; including its edges.
     *
     * @param port
     * @return true
     * @return false
     */
    bool deletePort(connoise::modules::ModulePort::shared_ptr port);

    /**
     * @brief Instance of the Graph for modelling the System.
     *
     */
    Graph m_graph;

    /**
     * @brief Mutex for model update locking.
     * 
     */
    tbb::spin_mutex m_modelMutex;
  };

} // namespace connoise::model
