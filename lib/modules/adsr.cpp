/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "adsr.h"

namespace connoise::modules
{
  ADSR::ADSR(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "ADSR", id)
  {
    DEBUG_MODULE_ADSR std::cout << "ADSR ctor: id=" << id << std::endl;

    CONNOISE_MODULE_CREATE_OUTPUT(ADSR, v);

    CONNOISE_MODULE_CREATE_INPUT(ADSR, A);
    CONNOISE_MODULE_CREATE_INPUT(ADSR, D);
    CONNOISE_MODULE_CREATE_INPUT(ADSR, S);
    CONNOISE_MODULE_CREATE_INPUT(ADSR, R);
    CONNOISE_MODULE_CREATE_INPUT(ADSR, trigger);

    initialise(init, {
                         {"A", 0.01},
                         {"D", 0.1},
                         {"S", 0.5},
                         {"R", 0.2},
                     });
  }

  const Buffer &ADSR::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &A = getInput("A")->buf;
    const auto &D = getInput("D")->buf;
    const auto &S = getInput("S")->buf;
    const auto &R = getInput("R")->buf;

    for (Sample s = 0; s < t.size(); ++s)
    {
      DEBUG_MODULE_ADSR std::cout << "ADSR::v"
                                  << " t=" << t[s]
                                  << " t0=" << m_t0[s]
                                  << " A=" << A[s]
                                  << " D=" << D[s]
                                  << " S=" << S[s]
                                  << " R=" << R[s]
                                  << " last=" << m_last
                                  << " ";
      if (t[s] < m_t0[s])
      {
        DEBUG_MODULE_ADSR std::cout << "before t0 " << std::endl;
        v[s] = last(s, 0);
        continue;
      }

      auto td = t[s] - m_t0[s];
      DEBUG_MODULE_ADSR std::cout << "td=" << td;
      if (m_on[s])
      {
        DEBUG_MODULE_ADSR std::cout << " on ";
        if (td <= A[s])
        {
          if (td == 0)
          {
            DEBUG_MODULE_ADSR std::cout << "A zero" << std::endl;
            v[s] = last(s, 0);
            continue;
          }
          auto av = td / A[s];
          DEBUG_MODULE_ADSR std::cout << "A=" << av << std::endl;
          v[s] = last(s, av);
          continue;
        }
        else if (td < (A[s] + D[s]))
        {
          auto tdd = td - A[s];
          auto dv = 1.0 - (1.0 - S[s]) * tdd / D[s];
          DEBUG_MODULE_ADSR std::cout << "D"
                                      << " tdd=" << tdd
                                      << " D=" << dv
                                      << std::endl;
          v[s] = last(s, dv);
          continue;
        }
        else
        {
          DEBUG_MODULE_ADSR std::cout << "S=" << S[s] << std::endl;
          v[s] = last(s, S[s]);
          continue;
        }
      }
      else
      {
        DEBUG_MODULE_ADSR std::cout << " off ";
        if (t[s] > (m_t0[s] + R[s]))
        {
          DEBUG_MODULE_ADSR std::cout << "after t0+R " << std::endl;
          v[s] = last(s, 0);
          continue;
        }
        else
        {
          auto rv = m_last * (1 - td / R[s]);
          DEBUG_MODULE_ADSR std::cout << "R=" << rv << std::endl;
          v[s] = rv;
          continue;
        }
      }
    }
    return v;
  }

  const Buffer &ADSR::A(const TimeVec &t)
  {
    return getInput("A")->buf;
  }

  const Buffer &ADSR::D(const TimeVec &t)
  {
    return getInput("D")->buf;
  }

  const Buffer &ADSR::S(const TimeVec &t)
  {
    return getInput("S")->buf;
  }

  const Buffer &ADSR::R(const TimeVec &t)
  {
    return getInput("R")->buf;
  }

  const Buffer &ADSR::trigger(const TimeVec &t)
  {
    const auto &trigger = getInput("trigger")->buf;
    const auto sz = t.size();

    // Start with last values of previous period
    for (Sample s = 0; s < sz; ++s)
    {
      m_on[s] = m_on[sz - 1];
      m_t0[s] = m_t0[sz - 1];
    }
    // now calculate the current period
    for (Sample s = 0; s < sz; ++s)
    {
      auto next = trigger[s] > 0.5;
      if (m_on[s] != next)
      {
        // Use this value for the rest of this period
        for (Sample u = s; u < sz; ++u)
        {
          m_on[u] = next;
          m_t0[u] = t[s];
        }
      }
      DEBUG_MODULE_ADSR std::cout << "ADSR::trigger"
                                  << " s=" << s
                                  << " t=" << t[s]
                                  << " on=" << (m_on[s] ? 1 : 0)
                                  << " t0=" << m_t0[s]
                                  << std::endl;
    }
    return trigger;
  }

  Value ADSR::last(const Sample s, Value v)
  {
    m_last = v;
    return v;
  }

} // namespace connoise::modules
