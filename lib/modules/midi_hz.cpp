/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "midi_hz.h"

namespace connoise::modules
{
  MidiNoteHz::MidiNoteHz(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "MidiNoteHz", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(MidiNoteHz, f);

    CONNOISE_MODULE_CREATE_INPUT(MidiNoteHz, n);

    initialise(init, {
                         {"n", 69.0},
                     });
  }

  const Buffer &MidiNoteHz::f(const TimeVec &t)
  {
    auto &f = getOutput("f")->buf;
    const auto &n = getInput("n")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      // fm  =  2**(m−69)/12 (440 Hz).
      f[s] = pow(2.0, (n[s] - 69.0) / 12.0) * 440.0;
    }
    return f;
  }

  const Buffer &MidiNoteHz::n(const TimeVec &t)
  {
    return getInput("n")->buf;
  }

} // namespace connoise::modules
