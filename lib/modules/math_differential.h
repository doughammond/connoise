/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_DIFFERENTIAL if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Calculates the difference between the current sample and the last, using an initial value of zero.
   *
   * Inputs:
   *
   * - `u` : Signal
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Differential : public Module
  {
  public:
    /**
     * @brief Differential shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Differential>;

    /**
     * @brief Construct a new Differential object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Differential(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      m_last = 0;
      Module::prepare(bufferSize, Fs);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &u(const TimeVec &t);

    // Internal

    /**
     * @brief Last rendered value.
     *
     */
    Value m_last;
  };

} // namespace connoise::modules
