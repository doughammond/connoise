/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Convert frequency in Hz to MIDI note number; with 440Hz tuning.
   *
   * Inputs:
   *
   * - `f` : Frequency, Hz
   *
   * Outputs:
   *
   * - `n` : MIDI note number
   *
   */
  class HzMidiNote : public Module
  {
  public:
    /**
     * @brief HzMidiNote shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<HzMidiNote>;

    /**
     * @brief Construct a new Hz Midi Note object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    HzMidiNote(const size_t id, const InitList &init);

  private:
    // Outputs

    /**
     * @brief Output: MIDI note value.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &n(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Frequency in Hz.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &f(const TimeVec &t);
  };

} // namespace connoise::modules
