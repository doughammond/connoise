/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "mixer.h"

namespace connoise::modules
{
  Mixer::Mixer(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Mixer", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Mixer, v);

    InitList superinit;
    bool setInputs = false;
    size_t inCount = 2;
    for (auto &&[k, val] : init)
    {
      if (k == "inputCount" && std::holds_alternative<Value>(val))
      {
        const auto v = std::get<Value>(val);
        inCount = v;
        setInputs = true;
      }
      else
      {
        superinit.push_back({k, val});
      }
    }
    inputCount(inCount);
    InitList defaults{
        {"inputCount", (Value)inCount},
    };
    for (size_t i = 0; i < inCount; ++i)
    {
      defaults.push_back({"gain" + std::to_string(i + 1), 1.0 / inCount});
    }

    initialise(superinit, defaults);
  }

  const Buffer &Mixer::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto sz = m_signals.size();
    for (Sample s = 0; s < t.size(); ++s)
    {
      auto val = 0.0;
      for (size_t n = 0; n < sz; ++n)
      {
        val += m_gains[n][s] * m_signals[n][s];
      }
      v[s] = val;
    }
    return v;
  }

  void Mixer::inputCount(const size_t num)
  {
    if (m_gains.size() != num)
    {
      m_gains.clear();
      m_signals.clear();

      m_gains.resize(num);
      m_signals.resize(num);

      for (size_t n = 0; n < num; ++n)
      {
        auto ns = std::to_string(n + 1);
        std::string gname = "gain" + ns;
        createInput(gname, std::bind(&Mixer::gain, this, n, std::placeholders::_1));

        std::string signame = "signal" + ns;
        createInput(signame, std::bind(&Mixer::signal, this, n, std::placeholders::_1));
      }

      DEBUG_MODULE_MIXER
      {
        std::cout << "Mixer now has inputs:" << std::endl;
        for (auto oi = m_inputs.begin(); oi != m_inputs.end(); ++oi)
        {
          auto iv = *oi;
          std::cout << " <- " << iv.first << std::endl;
        }
      }
    }
  }

  const Buffer &Mixer::signal(const size_t index, const TimeVec &t)
  {
    const auto &s = getInput("signal" + std::to_string(index + 1));
    DEBUG_MODULE_MIXER std::cout << "Mixer::signal" << (index + 1)
                                 << " =" << s
                                 << " buf.size=" << s->buf.size()
                                 << " m_signals.size=" << m_signals.size()
                                 << " m_signals[index].size=" << m_signals[index].size()
                                 << std::endl;
    std::copy(s->buf.begin(), s->buf.end(), m_signals[index].begin());
    return m_signals[index];
  }

  const Buffer &Mixer::gain(const size_t index, const TimeVec &t)
  {
    const auto &g = getInput("gain" + std::to_string(index + 1));
    DEBUG_MODULE_MIXER std::cout << "Mixer::gain" << (index + 1)
                                 << " =" << g
                                 << " buf.size=" << g->buf.size()
                                 << " m_gains.size=" << m_gains.size()
                                 << " m_gains[index].size=" << m_gains[index].size()
                                 << std::endl;
    std::copy(g->buf.begin(), g->buf.end(), m_gains[index].begin());
    return m_gains[index];
  }

} // namespace connoise::modules
