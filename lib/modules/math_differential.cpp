/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "math_differential.h"

namespace connoise::modules
{
  Differential::Differential(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Differential", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Differential, v);

    CONNOISE_MODULE_CREATE_INPUT(Differential, u);

    initialise(init, {});
  }

  const Buffer &Differential::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &u = getInput("u")->buf;
    const auto sz = t.size();
    for (Sample s = 0; s < sz; ++s)
    {
      v[s] = u[s] - m_last;

      DEBUG_MODULE_DIFFERENTIAL
      {
        if (v[s] != 0.0)
          std::cout << name() << "::v"
                    << " t=" << t[s]
                    << " u=" << u[s]
                    << " last=" << m_last
                    << " v=" << v[s]
                    << std::endl;
      }
      m_last = u[s];
    }
    return v;
  }

  const Buffer &Differential::u(const TimeVec &t)
  {
    return getInput("u")->buf;
  }

} // namespace connoise::modules
