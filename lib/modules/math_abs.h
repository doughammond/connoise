/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Absolute value of a signal.
   *
   * Inputs:
   *
   * - `u` : Any signal
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Abs : public Module
  {
  public:
    /**
     * @brief Abs shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Abs>;

    /**
     * @brief Construct a new Abs object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Abs(const size_t id, const InitList &init);

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &u(const TimeVec &t);
  };

} // namespace connoise::modules
