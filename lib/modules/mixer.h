/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_MIXER if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief A signal mixer. Variable number of channels. Each channel has a gain and signal input. Input numbering starts at 1.
   *
   * Inputs:
   *
   * - `inputCount` : Number of inputs the mixer has. Cannot be driven by a input connection, only via params
   * - `gainX`: Gain for the X'th input
   * - `signalX` : Signal for the X'th input
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Mixer : public Module
  {
  public:
    /**
     * @brief Mixer shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Mixer>;

    /**
     * @brief Construct a new Mixer object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Mixer(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      DEBUG_MODULE_MIXER std::cout << "Mixer::prepare" << std::endl;

      for (auto &g : m_gains)
      {
        g.resize(bufferSize);
      }
      for (auto &s : m_signals)
      {
        s.resize(bufferSize);
      }

      Module::prepare(bufferSize, Fs);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Parameterised inputs

    /**
     * @brief Input: index'th channel Signal.
     *
     * @param index
     * @param t
     * @return Buffer
     */
    const Buffer &signal(const size_t index, const TimeVec &t);

    /**
     * @brief Input: index'th channel Gain.
     *
     * @param index
     * @param t
     * @return Buffer
     */
    const Buffer &gain(const size_t index, const TimeVec &t);

    // Internal

    /**
     * @brief Reconfigure the mixer with the given number of inputs.
     *
     * @param num
     */
    void inputCount(const size_t num);

    /**
     * @brief Vector of channel Gains.
     *
     */
    std::vector<Buffer> m_gains;

    /**
     * @brief Vector of channel Signals.
     *
     */
    std::vector<Buffer> m_signals;
  };

} // namespace connoise::modules
