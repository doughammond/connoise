/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Multiplies two inputs.
   *
   * Inputs:
   *
   * - `a` : Any signal
   * - `b` : Any signal
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Multiply : public Module
  {
  public:
    /**
     * @brief Multiply shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Multiply>;

    /**
     * @brief Construct a new Multiply object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Multiply(const size_t id, const InitList &init);

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: first Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &a(const TimeVec &t);

    /**
     * @brief Input: second Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &b(const TimeVec &t);
  };

} // namespace connoise::modules
