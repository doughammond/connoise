/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "math_abs.h"

namespace connoise::modules
{
  Abs::Abs(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Abs", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Abs, v);

    CONNOISE_MODULE_CREATE_INPUT(Abs, u);

    initialise(init, {});
  }

  const Buffer &Abs::v(const TimeVec &t)
  {
    const auto &u = getInput("u")->buf;
    auto &v = getOutput("v")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      v[s] = u[s] < 0 ? -u[s] : u[s];
    }
    return v;
  }

  const Buffer &Abs::u(const TimeVec &t)
  {
    return getInput("u")->buf;
  }

} // namespace connoise::modules
