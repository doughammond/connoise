/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_OSCSIN if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Sinusoidal oscillator.
   *
   * Inputs:
   *
   * - `frequency` : The oscillator frequency in Hz
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Oscillator_Sine : public Module
  {
  public:
    /**
     * @brief Oscillator_Sine shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Oscillator_Sine>;

    /**
     * @brief Construct a new Oscillator_Sine object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Oscillator_Sine(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      DEBUG_MODULE_OSCSIN std::cout << "Oscillator_Sine::prepare" << std::endl;
      m_omega.resize(bufferSize);
      Module::prepare(bufferSize, Fs);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Frequency in Hz.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &frequency(const TimeVec &t);

    // Internal

    /**
     * @brief Internal representation of frequency, in rad/s.
     *
     */
    Buffer m_omega;
  };

} // namespace connoise::modules
