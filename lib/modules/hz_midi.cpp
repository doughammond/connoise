/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "hz_midi.h"

namespace connoise::modules
{
  HzMidiNote::HzMidiNote(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "HzMidiNote", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(HzMidiNote, n);

    CONNOISE_MODULE_CREATE_INPUT(HzMidiNote, f);

    initialise(init, {
                         {"f", 440.0},
                     });
  }

  const Buffer &HzMidiNote::n(const TimeVec &t)
  {
    auto &n = getOutput("n")->buf;
    const auto &f = getInput("f")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      // m  =  12*log2(fm/440 Hz) + 69
      n[s] = round(12.0 * log2(f[s] / 440.0) + 69.0);
    }
    return n;
  }

  const Buffer &HzMidiNote::f(const TimeVec &t)
  {
    return getInput("f")->buf;
  }

} // namespace connoise::modules
