/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_OSCSAW if (false)

#include "../module.h"

namespace connoise::modules
{
  /**
   * @brief Saw/triangle wave oscillator. Variable skew and phase.
   *
   * Inputs:
   *
   * - `skew` : a.k.a. shape, range 0.0 - 1.0
   * - `frequency` : The oscillator frequency in Hz
   * - `phase` : Phase offset in seconds
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Oscillator_Saw : public Module
  {
  public:
    /**
     * @brief Oscillator_Saw shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Oscillator_Saw>;

    /**
     * @brief Construct a new Oscillator_Saw object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Oscillator_Saw(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      DEBUG_MODULE_OSCSAW std::cout << "Oscillator_Saw::prepare bufferSize=" << bufferSize << std::endl;
      m_period.resize(bufferSize);
      Module::prepare(bufferSize, Fs);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Triange/saw shape skew.
     *
     * Range 0.0 - 1.0.
     *
     * 0.0 == left-handed saw |\|\|\
     * 0.5 == triangle /\/\/\
     * 1.0 == right-handed saw /|/|/|
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &skew(const TimeVec &t);

    /**
     * @brief Input: Frequency in Hz.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &frequency(const TimeVec &t);

    /**
     * @brief Input: Phase offset, time in seconds.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &phase(const TimeVec &t);

    // Internal

    /**
     * @brief Internal representation of frequency, period in seconds.
     *
     */
    Buffer m_period;
  };

} // namespace connoise::modules
