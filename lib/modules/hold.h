/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_HOLD if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief A sort of delayed-closing gate.
   *
   * Output value will be 1 whilst the trigger is above 0.5 and will remain to be 1 for `duration` seconds after the trigger falls below 0.5, after which it becomes 0.
   *
   * Inputs:
   *
   * - `duration` : Gate hold duration, seconds
   * - `trigger` : Trigger input
   *
   * Outputs:
   *
   * - `v` : Gate level (1 or 0)
   *
   */
  class Hold : public Module
  {
  public:
    /**
     * @brief Hold shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Hold>;

    /**
     * @brief Construct a new Hold object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Hold(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      m_t0.resize(bufferSize);

      Module::prepare(bufferSize, Fs);

      const auto d = getInput("duration")->buf;
      const auto init0 = d[0];
      DEBUG_MODULE_HOLD std::cout << name() << "::prepare init t0=" << init0 << std::endl;
      std::fill(m_t0.begin(), m_t0.end(), -init0);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Hold duration in seconds.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &duration(const TimeVec &t);

    /**
     * @brief Input: trigger.
     *
     * Any value > 0.5 will cause a trigger.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &trigger(const TimeVec &t);

    // Internal

    /**
     * @brief Vector of trigger times.
     *
     */
    TimeVec m_t0;
  };

} // namespace connoise::modules
