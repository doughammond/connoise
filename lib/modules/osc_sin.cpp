/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "osc_sin.h"

namespace connoise::modules
{
  Oscillator_Sine::Oscillator_Sine(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Sine", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Oscillator_Sine, v);

    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Sine, frequency);

    initialise(init, {
                         {"frequency", 440.0},
                     });
  }

  const Buffer &Oscillator_Sine::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      v[s] = sin(m_omega[s] * t[s]);
    }
    return v;
  }

  const Buffer &Oscillator_Sine::frequency(const TimeVec &t)
  {
    const auto &f = getInput("frequency")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      m_omega[s] = 2 * M_PI * f[s];
    }
    return f;
  }

} // namespace connoise::modules
