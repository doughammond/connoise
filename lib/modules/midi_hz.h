/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Convert MIDI note number to frequency in Hz; with 440Hz tuning.
   *
   * Inputs:
   *
   * - `n` : MIDI note number
   *
   * Outputs:
   *
   * - `f` : Frequency, Hz
   *
   */
  class MidiNoteHz : public Module
  {
  public:
    /**
     * @brief MidiNoteHz shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<MidiNoteHz>;

    /**
     * @brief Construct a new Midi Note Hz object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    MidiNoteHz(const size_t id, const InitList &init);

  private:
    // Outputs

    /**
     * @brief Output: Frequency in Hz.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &f(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: MIDI note number.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &n(const TimeVec &t);
  };

} // namespace connoise::modules
