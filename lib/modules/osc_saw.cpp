/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "osc_saw.h"
#include "util.h"

namespace connoise::modules
{
  Oscillator_Saw::Oscillator_Saw(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Saw", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Oscillator_Saw, v);

    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Saw, skew);
    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Saw, frequency);
    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Saw, phase);

    initialise(init, {
                         {"skew", 0.5},
                         {"frequency", 440.0},
                         {"phase", 0.0},
                     });
  }

  const Buffer &Oscillator_Saw::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &skew = getInput("skew")->buf;
    const auto &phase = getInput("phase")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      // phase adjusted time
      const auto tp = t[s] - phase[s];
      // how far in time through the period are we?
      const auto f = tp - floor(tp / m_period[s]) * m_period[s];
      // and fractionally?
      const auto ff = f / m_period[s];

      // So, the period for Saw has 4 key points:
      // | /B\       |
      // |/   \      |
      // A-----C-----.
      // |      \   /|
      // |       \D/ |

      // if skew == 0.0, then AB == DA == 0,   BC == CD == p/2
      // if skew == 0.5, then AB == DA == p/4, BC == CD == p/4
      // if skew == 1.0, then AB == DA == p/2, BC == CD == 0
      const auto ab = skew[s] * m_period[s] / 2;       // == da
      const auto bc = (1 - skew[s]) * m_period[s] / 2; // == cd

      const auto q1 = ab;                // A to B
      const auto q2 = ab + bc;           // B to C
      const auto q3 = ab + bc + bc;      // C to D
      const auto q4 = ab + bc + bc + ab; // D to A

      if (f < q1)
      {
        v[s] = util::lerp(0.0, 1.0, 0.0, q1, f);
      }
      else if (f < q2)
      {
        v[s] = util::lerp(1.0, 0.0, q1, q2, f);
      }
      else if (f < q3)
      {
        v[s] = util::lerp(0.0, -1.0, q2, q3, f);
      }
      else if (f < q4)
      {
        v[s] = util::lerp(-1.0, 0.0, q3, q4, f);
      }
    }
    return v;
  }

  const Buffer &Oscillator_Saw::skew(const TimeVec &t)
  {
    return getInput("skew")->buf;
  }

  const Buffer &Oscillator_Saw::frequency(const TimeVec &t)
  {
    const auto &fb = getInput("frequency")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      if (fb[s] == 0)
      {
        m_period[s] = 0;
      }
      else
      {
        m_period[s] = 1.0 / fb[s];
      }
    };
    return fb;
  }

  const Buffer &Oscillator_Saw::phase(const TimeVec &t)
  {
    return getInput("phase")->buf;
  }

} // namespace connoise::modules
