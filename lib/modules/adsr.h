/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_ADSR if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Typical ADSR envelope generator.
   *
   * Inputs:
   *
   * - `A` : Attack duration, seconds
   * - `D` : Decay duration, seconds
   * - `S` : Sustain level
   * - `R` : Release duration, seconds
   *
   * Outputs:
   *
   * - `v` : Envelope level
   *
   */
  class ADSR : public Module
  {
  public:
    /**
     * @brief ADSR shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<ADSR>;

    /**
     * @brief Construct a new ADSR object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    ADSR(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      DEBUG_MODULE_ADSR std::cout << "ADSR::prepare internal size " << bufferSize << std::endl;
      m_t0.resize(bufferSize);
      std::fill(m_t0.begin(), m_t0.end(), 0);
      m_on.resize(bufferSize);
      std::fill(m_on.begin(), m_on.end(), false);
      m_last = 0;
      Module::prepare(bufferSize, Fs);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Attack time.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &A(const TimeVec &t);

    /**
     * @brief Input: Decay time.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &D(const TimeVec &t);

    /**
     * @brief Input: Sustain level.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &S(const TimeVec &t);

    /**
     * @brief Input: Release time.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &R(const TimeVec &t);

    /**
     * @brief Input: trigger.
     *
     * Any value > 0.5 will cause a trigger.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &trigger(const TimeVec &t);

    // Internal

    /**
     * @brief Setter/Getter for last value rendered by this Module.
     *
     * @param s
     * @param v
     * @return Value
     */
    Value last(const Sample s, Value v);

    /**
     * @brief Vector of trigger times.
     *
     */
    std::vector<Time> m_t0;

    /**
     * @brief Vector of 'on' values.
     *
     */
    std::vector<bool> m_on;

    /**
     * @brief The last value rendered by this Module.
     *
     */
    Value m_last;
  };

} // namespace connoise::modules
