/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "osc_squ.h"

namespace connoise::modules
{
  Oscillator_Square::Oscillator_Square(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Square", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Oscillator_Square, v);

    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Square, width);
    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Square, frequency);
    CONNOISE_MODULE_CREATE_INPUT(Oscillator_Square, phase);

    initialise(init, {
                         {"width", 0.5},
                         {"frequency", 440.0},
                         {"phase", 0.0},
                     });
  }

  const Buffer &Oscillator_Square::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &phase = getInput("phase")->buf;
    const auto &width = getInput("width")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      // phase adjusted time
      const auto tp = t[s] - phase[s];
      // how far in time through the period are we?
      const auto f = tp - floor(tp / m_period[s]) * m_period[s];
      // if the fraction is less than width, output high, else low
      const auto vs = f < (m_period[s] * width[s]) ? 1 : -1;
      DEBUG_MODULE_OSCSQU std::cout << "Oscillator_Square::v:"
                                    << " t=" << t[s]
                                    << " tp=" << tp
                                    << " f=" << f
                                    << " v=" << vs
                                    << std::endl;

      v[s] = vs;
    }
    return v;
  }

  const Buffer &Oscillator_Square::width(const TimeVec &t)
  {
    return getInput("width")->buf;
  }

  const Buffer &Oscillator_Square::frequency(const TimeVec &t)
  {
    const auto &fb = getInput("frequency")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      if (fb[s] == 0)
      {
        m_period[s] = 0;
      }
      else
      {
        m_period[s] = 1.0 / fb[s];
      }
    };
    return fb;
  }

  const Buffer &Oscillator_Square::phase(const TimeVec &t)
  {
    return getInput("phase")->buf;
  }

} // namespace connoise::modules
