/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "filter_fir.h"

namespace connoise::modules
{
  FirFilter::FirFilter(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "FirFilter", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(FirFilter, v);

    CONNOISE_MODULE_CREATE_INPUT(FirFilter, f);
    CONNOISE_MODULE_CREATE_INPUT(FirFilter, q);
    CONNOISE_MODULE_CREATE_INPUT(FirFilter, u);

    InitList superinit;
    for (auto &&[k, val] : init)
    {
      if (k == "type" && std::holds_alternative<std::string>(val))
      {
        const auto type = std::get<std::string>(val);
        if (type == "lowpass")
        {
          m_type = FilterType::LOWPASS;
          continue;
        }
        else if (type == "highpass")
        {
          m_type = FilterType::HIGHPASS;
          continue;
        }
        else if (type == "bandpass")
        {
          m_type = FilterType::BANDPASS;
          continue;
        }
      }
      superinit.push_back({k, val});
    }

    initialise(superinit, {
                              {"f", 440.0},
                              {"q", 0.1},
                          });
  }

  void FirFilter::prepare(const size_t bufferSize, const int Fs)
  {
    m_Fs = Fs;

    m_ir.clear();
    m_ir.push_back(0.0);
    m_ir_len = 1;

    m_buf.clear();
    m_buf.push_back(0.0);
    m_buf_ptr = 0;

    Module::prepare(bufferSize, Fs);
  }

  void FirFilter::calculate(const Value f, const Value q)
  {
    Value f1 = 0;
    Value f2 = 0;
    switch (m_type)
    {
    case FilterType::LOWPASS:
      f2 = f;
      break;
    case FilterType::HIGHPASS:
      f1 = f;
      f2 = m_Fs / 2.0;
      break;
    case FilterType::BANDPASS:
      f1 = std::max(f - q * (f / 2.0), 0.0);
      f2 = std::min(f + q * (f / 2.0), m_Fs / 2.0);
      break;
    }

    auto ir = filter::fir::KaiserBessel::calculate(m_Fs, f1, f2, 31, 80);

    if (ir.size() > 0)
    {
      m_ir.swap(ir);
    }
    // otherwise use the previous IR

    m_ir_len = m_ir.size();

    // keep the *last* m_ir_len items
    // In practice, the IR length shouldn't be changing
    const auto bs = m_buf.size();
    if (bs != m_ir_len)
    {
      Buffer nextBuf(m_ir_len);
      const auto lim = std::min(m_ir_len, bs);
      for (Sample s = 0; s < lim; ++s)
      {
        nextBuf[m_ir_len - s - 1] = m_buf[bs - s - 1];
      }
      m_buf_ptr = m_ir_len - 1;
      m_buf.swap(nextBuf);
    }

    m_f = f;
  }

  const Buffer &FirFilter::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &f = getInput("f")->buf;
    const auto &q = getInput("q")->buf;
    const auto &u = getInput("u")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      if ((std::abs(m_f - f[s]) > 2.0) || (std::abs(m_q - q[s]) > 0.01))
      {
        calculate(f[s], q[s]);
      }

      // update the buffer current position
      // Not sure why we have to reduce the signal,
      // but it produces a full amplitude and stable output
      m_buf[m_buf_ptr] = u[s] * 0.707; // value is somewhat arbitrary

      v[s] = m_ir[0] * m_buf[m_buf_ptr];
      for (Sample r = 1; r < m_ir_len; ++r)
      {
        // read the IR and history forwards
        v[s] += m_ir[r] * m_buf[(m_buf_ptr + r) % m_ir_len];
      }

      // rewind the pointer (write backwards)
      m_buf_ptr = (m_buf_ptr == 0 ? m_ir_len : m_buf_ptr) - 1;
    }
    return v;
  }

  const Buffer &FirFilter::f(const TimeVec &t)
  {
    const auto &f = getInput("f")->buf;
    const auto &q = getInput("q")->buf;
    if (t[0] == 0 && f.size() > 0 && q.size() > 0)
    {
      calculate(f[0], q[0]);
    }
    return f;
  }

  const Buffer &FirFilter::q(const TimeVec &t)
  {
    const auto &f = getInput("f")->buf;
    const auto &q = getInput("q")->buf;
    if (t[0] == 0 && f.size() > 0 && q.size() > 0)
    {
      calculate(f[0], q[0]);
    }
    return q;
  }

  const Buffer &FirFilter::u(const TimeVec &t)
  {
    return getInput("u")->buf;
  }

} // namespace connoise::modules
