/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "math_add.h"

namespace connoise::modules
{
  Add::Add(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Add", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Add, v);

    CONNOISE_MODULE_CREATE_INPUT(Add, a);
    CONNOISE_MODULE_CREATE_INPUT(Add, b);

    initialise(init, {
                         {"a", 0.0},
                         {"b", 0.0},
                     });
  }

  const Buffer &Add::v(const TimeVec &t)
  {
    const auto &a = getInput("a")->buf;
    const auto &b = getInput("b")->buf;
    auto &v = getOutput("v")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      v[s] = a[s] + b[s];
    }
    return v;
  }

  const Buffer &Add::a(const TimeVec &t)
  {
    return getInput("a")->buf;
  }

  const Buffer &Add::b(const TimeVec &t)
  {
    return getInput("b")->buf;
  }

} // namespace connoise::modules
