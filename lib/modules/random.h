/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <random>

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Generate random numbers.
   *
   * Outputs:
   *
   * - `v` : Random numbers in range [-1, 1]
   *
   */
  class Random : public Module
  {
  public:
    /**
     * @brief Random shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Random>;

    /**
     * @brief Construct a new Random object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Random(const size_t id, const InitList &init);

  private:
    // Outputs

    /**
     * @brief Output: random numbers in range [-1, 1].
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Internal

    /**
     * @brief Random number generator.
     *
     */
    std::default_random_engine m_eng;

    /**
     * @brief Random number distribution.
     *
     */
    std::uniform_real_distribution<> m_dist = std::uniform_real_distribution(-1.0, 1.0);
  };

} // namespace connoise::modules
