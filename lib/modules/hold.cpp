/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "hold.h"

namespace connoise::modules
{
  Hold::Hold(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Hold", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Hold, v);

    CONNOISE_MODULE_CREATE_INPUT(Hold, duration);
    CONNOISE_MODULE_CREATE_INPUT(Hold, trigger);

    initialise(init, {
                         {"duration", 1.0},
                     });
  }

  const Buffer &Hold::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &d = getInput("duration")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      DEBUG_MODULE_HOLD std::cout << name() << "::v"
                                  << " t=" << t[s]
                                  << " t0=" << m_t0[s]
                                  << " d=" << d[s]
                                  << " t1=" << (m_t0[s] + d[s])
                                  << std::endl;
      v[s] = t[s] >= m_t0[s] && t[s] < (m_t0[s] + d[s]) ? 1 : 0;
    }
    return v;
  }

  const Buffer &Hold::duration(const TimeVec &t)
  {
    return getInput("duration")->buf;
  }

  const Buffer &Hold::trigger(const TimeVec &t)
  {
    const auto &trigger = getInput("trigger")->buf;
    const auto sz = t.size();

    // Start with last value of previous period
    for (Sample s = 0; s < sz; ++s)
    {
      m_t0[s] = m_t0[sz - 1];
    }
    // now calculate the current period
    for (Sample s = 0; s < sz; ++s)
    {
      if (trigger[s] > 0.5)
      {
        // Use this value for the rest of this period
        for (Sample u = s; u < sz; ++u)
        {
          m_t0[u] = t[s];
        }

        DEBUG_MODULE_HOLD
        {
          const auto &d = getInput("duration")->buf;
          std::cout << name() << "::trigger "
                    << " m_t0=" << m_t0[s]
                    << " t1=" << (m_t0[s] + d[s])
                    << std::endl;
        }
      }
    }
    return trigger;
  }

} // namespace connoise::modules
