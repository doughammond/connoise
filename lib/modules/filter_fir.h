/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include "../filter/fir_design.h"
#include "../module.h"

namespace connoise::modules
{
  /**
   * @brief FIR Filter implementation.
   *
   */
  class FirFilter : public Module
  {
  public:
    /**
     * @brief FirFilter shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<FirFilter>;

    /**
     * @brief Construct a new FIR Filter object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    FirFilter(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override;

    /**
     * @brief Calculate the filter IR from frequency and bandwidth.
     *
     * @param f frequency, Hz
     * @param q bandwidth (octaves)
     */
    void calculate(const Value f, const Value q);

  private:
    /**
     * @brief Enumeration of valid filter types.
     *
     */
    enum FilterType
    {
      LOWPASS,
      HIGHPASS,
      BANDPASS
    };

    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &u(const TimeVec &t);

    /**
     * @brief Input: Cutoff frequency.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &f(const TimeVec &t);

    /**
     * @brief Input: Band width in octaves.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &q(const TimeVec &t);

    // Internal

    /**
     * @brief The type of this filter.
     *
     */
    FilterType m_type;

    /**
     * @brief FIR filter impulse response.
     *
     */
    Buffer m_ir;

    /**
     * @brief Cached length of the IR.
     *
     */
    size_t m_ir_len;

    /**
     * @brief Input history ring buffer.
     *
     * Note that this buffer is written backwards,
     * and read forwards!
     *
     */
    Buffer m_buf;

    /**
     * @brief Pointer into m_buf.
     *
     */
    size_t m_buf_ptr;

    /**
     * @brief Cached sampling rate.
     *
     */
    int m_Fs;

    /**
     * @brief Cached last cutoff value.
     *
     */
    Value m_f;

    /**
     * @brief Cached last bandwidth value.
     *
     */
    Value m_q;
  };

} // namespace connoise::modules
