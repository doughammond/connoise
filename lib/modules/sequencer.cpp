/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "sequencer.h"

namespace connoise::modules
{
  Sequencer::Sequencer(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Sequencer", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Sequencer, v);
    CONNOISE_MODULE_CREATE_OUTPUT(Sequencer, changed);

    CONNOISE_MODULE_CREATE_INPUT(Sequencer, trigger);

    InitList superinit;
    m_sequence.clear();
    bool setInputs = false;

    // TODO: values need to be provided as params so that they can be edited!
    for (auto &&[k, val] : init)
    {
      if (k == "value" && std::holds_alternative<Value>(val))
      {
        m_sequence.push_back(std::get<Value>(val));
      }
      else
      {
        superinit.push_back({k, val});
      }
    }

    initialise(superinit, {}); // TODO: defaults?!
  }

  const Buffer &Sequencer::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    auto changeport = getOutput("changed");
    auto &changed = changeport->buf;
    const auto sz = t.size();
    for (Sample s = 0; s < sz; ++s)
    {
      auto next = m_sequence[m_ptr[s]];
      changed[s] = m_last == next ? 0 : 1;
      if (changed[s])
      {
        DEBUG_MODULE_SEQUENCER std::cout << name() << "::v"
                                         << " t=" << t[s]
                                         << " last=" << m_last
                                         << " next=" << next
                                         << " changed=" << changed[s]
                                         << std::endl;
      }
      v[s] = m_last = next;
    }
    changeport->stale(false);
    return v;
  }

  const Buffer &Sequencer::changed(const TimeVec &t)
  {
    return getOutput("changed")->buf;
  }

  const Buffer &Sequencer::trigger(const TimeVec &t)
  {
    const auto &trigger = getInput("trigger")->buf;
    const auto sz = t.size();
    size_t ptr = m_ptr[sz - 1];
    for (Sample s = 0; s < sz; ++s)
    {
      if (trigger[s] > 0.5)
      {
        ptr = (ptr + 1) % m_sequence.size();
        DEBUG_MODULE_SEQUENCER std::cout << name() << "::trigger"
                                         << " t=" << t[s]
                                         << " ptr=" << ptr
                                         << std::endl;
      }
      m_ptr[s] = ptr;
    }
    return trigger;
  }

} // namespace connoise::modules
