/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "delay.h"

namespace connoise::modules
{
  Delay::Delay(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Delay", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Delay, v);

    CONNOISE_MODULE_CREATE_INPUT(Delay, u);
    CONNOISE_MODULE_CREATE_INPUT(Delay, duration);

    initialise(init, {
                         {"duration", 1.0},
                     });
  }

  const Buffer &Delay::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    const auto &u = getInput("u")->buf;
    const auto &dur = getInput("duration")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      v[s] = m_buf[m_readptr];
      m_buf[m_writeptr] = u[s];

      const size_t durSamples = (dur[s] * m_Fs) + 0.5;
      const auto bs = m_buf.size();
      // Case 1 : delay time decrease, durSamples < bs
      //  rptr in range && wptr in range
      //  [ . . w r . . . . ] => [ . . w r . . ] . . => [ . . w r . . ]
      // Case 2 : delay time decrease, durSamples < bs
      //  rptr not in range && wptr in range
      //  [ . . . . . w r . ] => [ . . . . . w ] r . => [ r . . . . w ]
      // Case 3 : delay time decrease, durSamples < bs
      //  rptr in range && wptr not in range
      //  [ r . . . . . . w ] => [ r . . . . . ] . w => [ r . . . . w ]
      // Case 4 : delay time decrease, durSamples < bs
      //  rptr not in range && wptr not in range
      //  [ . . . . . . w r ] => [ . . . . . . ] w r => [ r . . . . w ]
      if (bs != durSamples)
      {
        m_buf.resize(durSamples);
        const auto imax = bs - 1;
        if (m_readptr > imax || m_writeptr > imax)
        {
          m_readptr = 0;
          m_writeptr = imax;
        }
        else
        {
          m_writeptr = m_readptr + durSamples - 1;
        }
        DEBUG_MODULE_DELAY std::cout << "Delay::v resets pointers;"
                                     << " t=" << t[s] << ","
                                     << " r=" << m_readptr << ","
                                     << " w=" << m_writeptr << ","
                                     << " sz=" << durSamples
                                     << std::endl;
      }
      else
      {
        m_readptr = (m_readptr + 1) % bs;
        m_writeptr = (m_writeptr + 1) % bs;
      }
      DEBUG_MODULE_DELAY std::cout << "Delay::v t=" << t[s]
                                   << " r=" << m_readptr
                                   << ", w=" << m_writeptr
                                   << std::endl;
    }
    return v;
  }

  const Buffer &Delay::u(const TimeVec &t)
  {
    return getInput("u")->buf;
  }

  const Buffer &Delay::duration(const TimeVec &t)
  {
    return getInput("duration")->buf;
  }

} // namespace connoise::modules