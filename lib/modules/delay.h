/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_DELAY if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Simple delay line.
   *
   * Inputs:
   *
   * - `duration` : Delay time, seconds
   * - `u` : Signal
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Delay : public Module
  {
  public:
    /**
     * @brief Delay shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Delay>;

    /**
     * @brief Construct a new Delay object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Delay(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      DEBUG_MODULE_DELAY std::cout << "Delay::prepare" << std::endl;
      Module::prepare(bufferSize, Fs);
      m_Fs = Fs;
      const auto &dur = getInput("duration")->buf;
      const auto initDuration = dur[0];
      const size_t delayBufferSize = (initDuration * Fs) + 0.5;
      m_buf.resize(delayBufferSize);
      m_writeptr = 0;
      m_readptr = 1;
      DEBUG_MODULE_DELAY std::cout << "Delay::prepare"
                                   << " delayBufferSize=" << delayBufferSize
                                   << " m_readptr=" << m_readptr
                                   << " m_writeptr=" << m_writeptr
                                   << std::endl;
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &u(const TimeVec &t);

    /**
     * @brief Input: Delay duration in seconds.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &duration(const TimeVec &t);

    // Internal

    /**
     * @brief Cache of the rendering sample rate.
     *
     */
    int m_Fs;

    /**
     * @brief Delay line ring buffer.
     *
     */
    Buffer m_buf;

    /**
     * @brief Current read position in the ring buffer.
     *
     */
    size_t m_readptr;

    /**
     * @brief Current write position in the ring buffer.
     *
     */
    size_t m_writeptr;
  };

} // namespace connoise::modules
