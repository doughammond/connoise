/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_OSCSQU if (false)

#include "../module.h"

namespace connoise::modules
{
  /**
   * @brief Square wave oscillator. Variable width and phase.
   *
   * Inputs:
   *
   * - `width` : a.k.a. Duty cycle, range 0.0 - 1.0
   * - `frequency` : The oscillator frequency in Hz
   * - `phase` : Phase offset in seconds
   *
   * Outputs:
   *
   * - `v` : Signal
   *
   */
  class Oscillator_Square : public Module
  {
  public:
    /**
     * @brief Oscillator_Square shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Oscillator_Square>;

    /**
     * @brief Construct a new Oscillator_Square object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Oscillator_Square(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      DEBUG_MODULE_OSCSQU std::cout << "Oscillator_Square::prepare bufferSize=" << bufferSize << std::endl;
      m_period.resize(bufferSize);
      Module::prepare(bufferSize, Fs);
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: Normalised square wave pulse width / duty cycle.
     *
     * Range 0.0 - 1.0.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &width(const TimeVec &t);

    /**
     * @brief Input: Frequency in Hz.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &frequency(const TimeVec &t);

    /**
     * @brief Input: Phase offset, time in seconds.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &phase(const TimeVec &t);

    // Internal

    /**
     * @brief Internal representation of frequency, period in seconds.
     *
     */
    Buffer m_period;
  };

} // namespace connoise::modules
