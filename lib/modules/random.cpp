/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <math.h>

#include "random.h"

namespace connoise::modules
{
  Random::Random(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Random", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Random, v);

    initialise(init, {});
  }

  const Buffer &Random::v(const TimeVec &t)
  {
    auto &v = getOutput("v")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      v[s] = m_dist(m_eng);
    }
    return v;
  }

} // namespace connoise::modules
