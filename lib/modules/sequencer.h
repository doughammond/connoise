/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#define DEBUG_MODULE_SEQUENCER if (false)

#include "../module.h"

namespace connoise::modules
{

  /**
   * @brief Simple single-value step sequencer.
   *
   * Values are currently fixed, and supplied via Module params:
   *
   * ```
   * Module Sequencer {
   *   id seq
   *   params {
   *     value 1
   *     value 2
   *     value 3
   *     value 4
   *   }
   * }
   * ```
   *
   * Inputs:
   *
   * - `trigger` : Trigger input, value > 0.5 will cause the sequence to advance.
   *
   * Outputs:
   *
   * - `v` : Value
   * - `changed` : 0/1 if the Value changed at the current Sample
   *
   */
  class Sequencer : public Module
  {
  public:
    /**
     * @brief Sequencer shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<Sequencer>;

    /**
     * @brief Construct a new Sequencer object.
     *
     * \see connoise::modules::registry::ModuleFactory
     *
     * @param id
     * @param init
     */
    Sequencer(const size_t id, const InitList &init);

    virtual void prepare(const size_t bufferSize, const int Fs) override
    {
      m_ptr.resize(bufferSize);
      // end of the sequence
      std::fill(m_ptr.begin(), m_ptr.end(), m_sequence.size() - 1);
      m_last = m_sequence[m_sequence.size() - 1];

      Module::prepare(bufferSize, Fs);
      DEBUG_MODULE_SEQUENCER std::cout << name() << "::prepare done" << std::endl;
    }

  private:
    // Outputs

    /**
     * @brief Output: Signal.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &v(const TimeVec &t);

    /**
     * @brief Output: Signal changed trigger.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &changed(const TimeVec &t);

    // Inputs

    /**
     * @brief Input: trigger.
     *
     * Any value > 0.5 will cause a trigger.
     *
     * @param t
     * @return const Buffer&
     */
    const Buffer &trigger(const TimeVec &t);

    // Internal

    /**
     * @brief The sequence values.
     *
     */
    Buffer m_sequence;

    /**
     * @brief Pointer to current sequence value.
     *
     */
    Buffer m_ptr;

    /**
     * @brief The last value rendered.
     *
     */
    Value m_last;
  };

} // namespace connoise::modules
