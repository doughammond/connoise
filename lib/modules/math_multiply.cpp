/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include "math_multiply.h"

namespace connoise::modules
{
  Multiply::Multiply(const size_t id, const InitList &init)
      : Module(ModuleClass::Processor, "Multiply", id)
  {
    CONNOISE_MODULE_CREATE_OUTPUT(Multiply, v);

    CONNOISE_MODULE_CREATE_INPUT(Multiply, a);
    CONNOISE_MODULE_CREATE_INPUT(Multiply, b);

    initialise(init, {
                         {"a", 1.0},
                         {"b", 1.0},
                     });
  }

  const Buffer &Multiply::v(const TimeVec &t)
  {
    const auto &a = getInput("a")->buf;
    const auto &b = getInput("b")->buf;
    auto &v = getOutput("v")->buf;
    for (Sample s = 0; s < t.size(); ++s)
    {
      v[s] = a[s] * b[s];
    }
    return v;
  }

  const Buffer &Multiply::a(const TimeVec &t)
  {
    return getInput("a")->buf;
  }

  const Buffer &Multiply::b(const TimeVec &t)
  {
    return getInput("b")->buf;
  }

} // namespace connoise::modules
