/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#pragma once

#include <iostream>
#include <map>
#include <memory>

#include "modules.h"
#include "systemmodel.h"

namespace connoise
{
  /**
   * @brief Representation of a Synthesizer system.
   *
   * Owns Module and Connection instances.
   * Owns runtime data.
   *
   */
  class System
  {
  public:
    /**
     * @brief System shared pointer type.
     *
     */
    using shared_ptr = std::shared_ptr<System>;

    /**
     * @brief Construct a new System object
     * 
     */
    System();

    /**
     * @brief Getter for the next available Module ID.
     *
     * @return const size_t
     */
    const size_t nextModuleId() const
    {
      const auto m = std::max_element(
          std::begin(m_modules), std::end(m_modules),
          [](const auto &p1, const auto &p2) {
            return p1.first < p2.first;
          });
      return m->first + 1;
    }

    /**
     * @brief Create a Module object.
     *
     * @param id
     * @param type
     * @param init
     * @return connoise::modules::Module::shared_ptr
     */
    connoise::modules::Module::shared_ptr createModule(const size_t id, const std::string &type, const InitList &init = {});

    /**
     * @brief Delete a Module from the system.
     *
     * @param id
     * @return true
     * @return false
     */
    bool deleteModule(const size_t id);

    /**
     * @brief Create a Connection object.
     *
     * @param mo
     * @param output
     * @param mi
     * @param input
     */
    void createConnection(connoise::modules::Module::shared_ptr mo, const std::string &output, connoise::modules::Module::shared_ptr mi, const std::string &input);

    /**
     * @brief Delete a connection between Module out port - in port pair.
     *
     * @param moo ModulePort representing a module output
     * @param mii ModulePort representing a module input
     * @return true
     * @return false
     */
    bool deleteConnection(connoise::modules::ModulePort::shared_ptr moo, connoise::modules::ModulePort::shared_ptr mii);

    /**
     * @brief Get the Module object.
     *
     * @param id
     * @return connoise::modules::Module::shared_ptr
     */
    connoise::modules::Module::shared_ptr getModule(const size_t &id) const;

    /**
     * @brief Get the map of Modules by name.
     *
     * @return std::map<size_t, connoise::modules::Module::shared_ptr>
     */
    std::map<size_t, connoise::modules::Module::shared_ptr> getModules() const
    {
      return m_modules;
    }

    /**
     * @brief Get the System's Connections arranged by graph depth.
     * Note that only connnections which lead to an Output will be returned.
     *
     * @return const DepthSortedConnections
     */
    const DepthSortedConnections getConnections()
    {
      return m_model.calculateConnectionOrder();
    }

    /**
     * @brief Get all of the System's connections in no particular order.
     *
     * @return const Connections
     */
    const Connections getAllConnections()
    {
      return m_model.getAllConnections();
    }

    /**
     * @brief Print a graphviz dot representation of the System to stdout.
     *
     * @param name
     */
    void printDot(const std::string &name)
    {
      m_model.printDot(name);
    }

    /**
     * @brief Setter for a piece of metadata.
     *
     * @param key
     * @param value
     */
    void metadata(const std::string &key, const std::string &value)
    {
      m_metadata[key] = value;
    }

    /**
     * @brief Get a piece of metadata by key.
     *
     * @param key
     * @return const std::string
     */
    const std::string metadata(const std::string &key) const
    {
      if (m_metadata.find(key) != m_metadata.end())
      {
        return m_metadata.at(key);
      }
      return "";
    }

    /**
     * @brief Getter for this System's metadata map.
     *
     * @return const std::map<std::string, std::string>
     */
    const std::map<std::string, std::string> metadata() const
    {
      return m_metadata;
    }

  private:
    /**
     * @brief The abstract model of the system.
     *
     */
    connoise::model::SystemModel m_model;

    /**
     * @brief Map of Modules in the System by ID.
     *
     */
    std::map<size_t, connoise::modules::Module::shared_ptr> m_modules;

    /**
     * @brief System metadata, e.g. for GUI layout information.
     *
     */
    std::map<std::string, std::string> m_metadata;
  };

  /**
   * @brief System serialisation.
   *
   * @param o
   * @param system
   * @return std::ostream&
   */
  std::ostream &operator<<(std::ostream &o, System::shared_ptr system);

} // namespace connoise
