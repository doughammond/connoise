/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module Hold - Static duration", "[Module][Hold]")
{
  auto m = Processors.at("Hold")(0, {{"duration", 0.5}});

  REQUIRE(m);

  TimeVec t{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
  m->prepare(t.size(), 1);
  auto u = m->getInput("trigger");
  u->buf = {0, 1, 0, 0, 0, 0, 0, 0, 0, 1};
  u->fn(t);

  auto v = m->getOutput("v")->fn(t);
  // got first trigger, and first sample of 2nd trigger
  REQUIRE(v == Buffer{0, 1, 1, 1, 1, 1, 0, 0, 0, 1});

  // second period
  t = {1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9};
  u->buf = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  u->fn(t);

  v = m->getOutput("v")->fn(t);
  // got the remainder of the 2nd trigger
  REQUIRE(v == Buffer{1, 1, 1, 1, 0, 0, 0, 0, 0, 0});
}