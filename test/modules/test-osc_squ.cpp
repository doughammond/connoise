/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module Square ", "[Module][Square]")
{
    auto m = Processors.at("Square")(0, {
                                            {"frequency", 1.0},
                                            {"width", 0.5},
                                        });

    REQUIRE(m);

    TimeVec tv{
        0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
        1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9,
        2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9};
    m->prepare(tv.size(), 10);

    auto v = m->getOutput("v")->fn(tv);
    REQUIRE(v == (Buffer{
                     1, 1, 1, 1, 1, -1, -1, -1, -1, -1,
                     1, 1, 1, 1, 1, -1, -1, -1, -1, -1,
                     1, 1, 1, 1, 1, -1, -1, -1, -1, -1}));
}
