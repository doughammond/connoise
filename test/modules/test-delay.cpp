/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module Delay - Static duration", "[Module][Delay]")
{
  auto m = Processors.at("Delay")(0, {{"duration", 1.0}});

  REQUIRE(m);

  // First period
  TimeVec t{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  m->prepare(t.size(), 1);
  auto u = m->getInput("u");
  u->buf = {0.0, 0.0, 0.2, 0.2, 0.0, 0.0, 0.0, -1, 1, 2};
  u->fn(t);

  auto v = m->getOutput("v")->fn(t);
  // all delayed by one sample,
  // initially zero
  REQUIRE(v == Buffer{0.0, 0.0, 0.0, 0.2, 0.2, 0.0, 0, 0, -1.0, 1.0});

  // second period
  t = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  u->buf = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  u->fn(t);

  v = m->getOutput("v")->fn(t);
  // [0] == last value of previous period
  REQUIRE(v == Buffer{2, 0, 0, 0, 0, 0, 0, 0, 0, 0});
}

TEST_CASE("Module Delay - Dynamic duration", "[Module][Delay]")
{
  auto m = Processors.at("Delay")(0, {{"duration", 3.0}});

  REQUIRE(m);

  // First period
  TimeVec t{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  m->prepare(t.size(), 1);
  auto u = m->getInput("u");
  auto duration = m->getInput("duration");
  duration->buf = {3, 3, 3, 3, 3, 1, 1, 1, 1, 1};
  duration->fn(t);
  u->buf = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  u->fn(t);

  auto v = m->getOutput("v")->fn(t);
  // Gets a bit jumbled around the transition?
  //                                             OK..............  ???....  OK..
  REQUIRE(v == Buffer{0, 0, 0, 1, 2, 3, 3, 6, 7, 8});

  // second period
  t = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  duration->buf = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  duration->fn(t);
  u->buf = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  u->fn(t);

  v = m->getOutput("v")->fn(t);
  // by now we're correctly 1 behind
  REQUIRE(v == Buffer{9, 10, 11, 12, 13, 14, 15, 16, 17, 18});
}
