/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module FirFilter - Static duration", "[Module][FirFilter]")
{
  auto m = Processors.at("FirFilter")(0, {
                                             {"f", 20.0},
                                         });

  REQUIRE(m);

  TimeVec t{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
  m->prepare(t.size(), 1);
  auto u = m->getInput("u");
  u->buf = {0, 1, 0, -1, 0, 0, 2, 2, 0, 1};
  u->fn(t);

  auto v = m->getOutput("v")->fn(t);
  // Don't really care what the output contains right now.
}
