/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module Differential", "[Module][Differential]")
{
  auto m = Processors.at("Differential")(0, {});

  REQUIRE(m);

  // First period
  TimeVec t{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  m->prepare(t.size(), 1);
  auto u = m->getInput("u");
  u->buf = {0, 0, 0.2, 0.2, 0.0, 0.0, 0.0, -1, 1, 0};
  u->fn(t);

  auto v = m->getOutput("v")->fn(t);
  //                                        v[s] 0.0, 0.0, 0.2, 0.2,  0.0, 0.0, 0.0, -1.0, 1.0,  0.0
  //                                             -    -    -    -     -    -    -     -    -     -
  //                                      v[s-1] 0.0, 0.0, 0.0, 0.2,  0.2, 0.0, 0.0,  0.0,-1.0,  1.0
  //                                             =    =    =    =     =    =    =     =    =     =
  REQUIRE(v == Buffer{0.0, 0.0, 0.2, 0.0, -0.2, 0.0, 0.0, -1.0, 2.0, -1.0});

  // second period
  t = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  u->buf = {1, 0, -1, -1, -1, 1, 1, 1, 0, 0};
  u->fn(t);

  v = m->getOutput("v")->fn(t);

  //                                        v[s] 1,  0, -1,-1,-1, 1, 1, 1,  0, 0
  //                                             -   -   -  -  -  -  -  -   -  -
  //                                      v[s-1] 0,  1,  0,-1,-1,-1, 1, 1,  1, 0
  //                                             =   =   =  =  =  =  =  =   =  =
  REQUIRE(v == Buffer{1, -1, -1, 0, 0, 2, 0, 0, -1, 0});
}