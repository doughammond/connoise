/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module Sequencer ", "[Module][Sequencer]")
{
  auto m = Processors.at("Sequencer")(0, {
                                             {"value", 1.0},
                                             {"value", 2.0},
                                             {"value", 3.0},
                                         });

  REQUIRE(m);

  TimeVec t{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  m->prepare(t.size(), 1);
  auto trigger = m->getInput("trigger");
  trigger->buf = {1, 1, 1, 1, 1, 0, 0, 0, 1, 1};
  trigger->fn(t);

  auto v = m->getOutput("v")->fn(t);
  //                                    trigger  1  1  1  1  1  0  0  0  1  1
  REQUIRE(v == Buffer{1, 2, 3, 1, 2, 2, 2, 2, 3, 1});

  auto c = m->getOutput("changed")->fn(t);
  //                                    trigger  1  1  1  1  1  0  0  0  1  1
  REQUIRE(c == Buffer{1, 1, 1, 1, 1, 0, 0, 0, 1, 1});
}
