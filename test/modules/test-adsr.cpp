/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>

using namespace connoise::modules;
using namespace connoise::modules::registry;
using TimeVec = connoise::TimeVec;
using Buffer = connoise::Buffer;

TEST_CASE("Module ADSR", "[Module][ADSR]")
{
  auto m = Processors.at("ADSR")(0, {
                                        {"A", 2.0},
                                        {"D", 3.0},
                                        {"S", 0.4},
                                        {"R", 2.0},
                                    });

  REQUIRE(m);

  // First period
  TimeVec t{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  m->prepare(t.size(), 1);
  auto trigger = m->getInput("trigger");
  trigger->buf = {1, 1, 1, 1, 1, 1, 1, 0, 0, 0};
  trigger->fn(t);

  auto v = m->getOutput("v")->fn(t);
  //                                             A    A    A    D    D    S    S    R    R    R
  REQUIRE_THAT(v, Catch::Matchers::Approx(Buffer{0.0, 0.5, 1.0, 0.8, 0.6, 0.4, 0.4, 0.4, 0.2, 0.0}));

  // second period
  t = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
  trigger->buf = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  trigger->fn(t);

  v = m->getOutput("v")->fn(t);
  // All off now
  REQUIRE(v == Buffer{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

  // third period
  t = {20, 21, 22, 23, 24, 25, 26, 27, 28, 29};
  trigger->buf = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  trigger->fn(t);

  v = m->getOutput("v")->fn(t);
  // All off now
  REQUIRE(v == Buffer{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
}