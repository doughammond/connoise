/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <parser.h>
#include <system.h>

#include <outputs/wavfile.h>

const std::string testDataPath = "../test/data/inputs/";

using LevelList = std::vector<int>;
using NameList = std::vector<std::string>;

/**
 * @brief Struct to hold information to inspect System connections.
 *
 */
struct DSInfo
{
  /**
   * @brief list of known graph levels.
   *
   */
  LevelList levels;

  /**
   * @brief Map of graph level to list of node names.
   *
   */
  std::map<int, NameList> names;
};

DSInfo inspectConnections(const connoise::DepthSortedConnections &cx)
{
  DSInfo info;
  for (auto &&[level, dcx] : cx)
  {
    info.levels.push_back(level);
    for (auto &&c : dcx)
    {
      info.names[level].push_back(c.out->name());
    }
  }
  return info;
}

TEST_CASE("Parser input test 01-single-module", "[Parser][01]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "01-single-module.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module = system->getModule(1);
  REQUIRE(module);

  REQUIRE(module->id() == 1);
  REQUIRE(module->stringParam("label") == "1");
  REQUIRE(module->type() == "ADSR");

  // No connections
  auto cx = system->getConnections();
  REQUIRE(cx.size() == 0);
}

TEST_CASE("Parser input test 02-multi-module", "[Parser][02]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "02-multi-module.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module1 = system->getModule(1);
  REQUIRE(module1);
  REQUIRE(module1->id() == 1);
  REQUIRE(module1->stringParam("label") == "1");
  REQUIRE(module1->type() == "ADSR");

  auto module2 = system->getModule(2);
  REQUIRE(module2);
  REQUIRE(module2->id() == 2);
  REQUIRE(module2->stringParam("label") == "2");
  REQUIRE(module2->type() == "Multiply");

  // No connections
  auto cx = system->getConnections();
  REQUIRE(cx.size() == 0);
}

TEST_CASE("Parser input test 03-single-connection", "[Parser][03]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "03-single-connection.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  // because there's no modules !
  auto cx = system->getConnections();
  REQUIRE(cx.size() == 0);
}

TEST_CASE("Parser input test 04-module-then-connection", "[Parser][04]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "04-module-then-connection.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module = system->getModule(1);
  REQUIRE(module);
  REQUIRE(module->id() == 1);
  REQUIRE(module->stringParam("label") == "1");
  REQUIRE(module->type() == "Multiply");

  // Zero because there's no System output defined
  auto cx = system->getConnections();
  REQUIRE(cx.size() == 0);
}

TEST_CASE("Parser input test 05-module-then-module-connection", "[Parser][05]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "05-module-then-module-connection.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module1 = system->getModule(1);
  REQUIRE(module1);
  REQUIRE(module1->id() == 1);
  REQUIRE(module1->stringParam("label") == "1");
  REQUIRE(module1->type() == "Multiply");

  auto module2 = system->getModule(2);
  REQUIRE(module2);
  REQUIRE(module2->id() == 2);
  REQUIRE(module2->stringParam("label") == "2");
  REQUIRE(module2->type() == "Multiply");

  // Zero because there's no System output defined
  auto cx = system->getConnections();
  REQUIRE(cx.size() == 0);
}

TEST_CASE("Parser input test 05-module-module-connection-duplicate", "[Parser][05]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "05-module-module-connection-duplicate.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module1 = system->getModule(1);
  REQUIRE(module1);
  REQUIRE(module1->id() == 1);
  REQUIRE(module1->stringParam("label") == "1");
  REQUIRE(module1->type() == "Multiply");

  auto module2 = system->getModule(2);
  REQUIRE(module2);
  REQUIRE(module2->id() == 2);
  REQUIRE(module2->stringParam("label") == "2");
  REQUIRE(module2->type() == "Multiply");

  // (duplicate connections in input, but only unique connections in System)
  // Zero because there's no System output defined
  auto cx = system->getConnections();
  REQUIRE(cx.size() == 0);
}

TEST_CASE("Parser input test 06-system", "[Parser][06]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "06-system-2.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module = system->getModule(1);
  REQUIRE(module);
  REQUIRE(module->id() == 1);
  REQUIRE(module->stringParam("label") == "1");
  REQUIRE(module->type() == "Sine");

  auto cx = system->getConnections();

  const auto cxInfo = inspectConnections(cx);
  REQUIRE(cxInfo.levels == LevelList{2, 5});
  REQUIRE(cxInfo.names.at(2) == NameList{
                                    "Multiply[3]::v",
                                });
  REQUIRE(cxInfo.names.at(5) == NameList{
                                    "Square[2]::v",
                                    "Sine[1]::v",
                                });
}

TEST_CASE("Parser input test 07-pings", "[Parser][07]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "07-pings.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto cx = system->getConnections();

  const auto cxInfo = inspectConnections(cx);
  REQUIRE(cxInfo.levels == LevelList{2, 5, 8, 9, 11});
  REQUIRE(cxInfo.names.at(2) == NameList{
                                    "Mixer[12]::v",
                                });
  REQUIRE(cxInfo.names.at(5) == NameList{
                                    "Multiply[3]::v",
                                    "Multiply[8]::v",
                                    "Multiply[10]::v",
                                });
  REQUIRE(cxInfo.names.at(8) == NameList{
                                    "ADSR[2]::v",
                                    "ADSR[2]::v",
                                    "Sine[7]::v",
                                    "Sine[9]::v",
                                    "Sine[11]::v",
                                });
  REQUIRE(cxInfo.names.at(9) == NameList{
                                    // Got promoted, so that concurrent level processing works
                                    "ADSR[2]::v",
                                });
  REQUIRE(cxInfo.names.at(11) == NameList{
                                     "MidiNoteHz[4]::f",
                                     "MidiNoteHz[5]::f",
                                     "MidiNoteHz[6]::f",
                                     "Square[1]::v",
                                 });
}

TEST_CASE("Parser input test 11-output-wavfile", "[Parser][11]")
{
  connoise::System::shared_ptr system = std::make_shared<connoise::System>();
  connoise::grammar::ParserData pd{system};

  const std::string fn(testDataPath + "11-output-wavfile.cno");
  connoise::grammar::p::file_input in(fn);
  connoise::grammar::p::parse<
      connoise::grammar::grammar,
      connoise::grammar::action>(in, pd);

  auto module = system->getModule(2);
  REQUIRE(module);

  REQUIRE(module->id() == 2);
  REQUIRE(module->stringParam("label") == "2");
  REQUIRE(module->type() == "WavFile");

  connoise::outputs::WavFile::shared_ptr w = std::static_pointer_cast<connoise::outputs::WavFile>(module);

  REQUIRE(w->stringParam("path") == "/tmp/output.wav");

  auto cx = system->getConnections();

  const auto cxInfo = inspectConnections(cx);
  REQUIRE(cxInfo.levels == LevelList{2});
  REQUIRE(cxInfo.names.at(2) == NameList{
                                    "Sine[1]::v",
                                });
}
