/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>
#include <system.h>
#include <transport.h>

using namespace connoise::modules;
using Buffer = connoise::Buffer;

/**
 *
 * 1.0 ----------\
 * in_a{} -> [Multiply]
 *               |
 *    0.5 -> [Multiply] -> out_b{}
 *
 */
TEST_CASE("System with serially connected modules - Multiply->Multiply", "[System][Module][Multiply]")
{
  auto s = std::make_shared<connoise::System>();
  auto t = std::make_shared<connoise::Transport>(s);

  auto a = s->createModule(0, "Multiply", {{"b", 1.0}});
  auto b = s->createModule(1, "Multiply", {{"b", 0.5}});

  // c is irrelevant in this test, just being used to pull data through b
  auto c = s->createModule(2, "NullOutput");

  REQUIRE(a);
  REQUIRE(b);
  REQUIRE(c);

  s->createConnection(a, "v", b, "a");
  s->createConnection(b, "v", c, "u");

  auto aa = a->getInput("a");
  auto ab = a->getInput("b");
  auto av = a->getOutput("v");

  auto ba = b->getInput("a");
  auto bb = b->getInput("b");
  auto bv = b->getOutput("v");

  REQUIRE(aa->buf == Buffer{});
  REQUIRE(ab->buf == Buffer{});
  REQUIRE(av->buf == Buffer{});

  REQUIRE(ba->buf == Buffer{});
  REQUIRE(bb->buf == Buffer{});
  REQUIRE(bv->buf == Buffer{});

  t->prepare(1, 10, false);

  REQUIRE(aa->buf == Buffer{1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
  REQUIRE(ab->buf == Buffer{1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
  REQUIRE(av->buf == Buffer{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

  REQUIRE(ba->buf == Buffer{1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
  REQUIRE(bb->buf == Buffer{0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5});
  REQUIRE(bv->buf == Buffer{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

  aa->buf = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  t->step();

  REQUIRE(aa->buf == Buffer{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
  REQUIRE(ab->buf == Buffer{1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
  REQUIRE(av->buf == Buffer{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});

  REQUIRE(ba->buf == Buffer{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
  REQUIRE(bb->buf == Buffer{0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5});
  REQUIRE(bv->buf == Buffer{0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5});
}

/**
 *
 * [Square] -> [Differential]
 *
 */
TEST_CASE("System with serially connected modules - Square->Differential", "[System][Module][Square][Differential]")
{
  auto s = std::make_shared<connoise::System>();
  auto t = std::make_shared<connoise::Transport>(s);

  auto a = s->createModule(0, "Square", {
                                            {"frequency", 1.0},
                                            {"width", 0.5},
                                        });
  auto b = s->createModule(1, "Differential");

  // c is irrelevant in this test, just being used to pull data through b
  auto c = s->createModule(2, "NullOutput");

  REQUIRE(a);
  REQUIRE(b);
  REQUIRE(c);

  s->createConnection(a, "v", b, "u");
  s->createConnection(b, "v", c, "u");

  auto av = a->getOutput("v");

  auto bu = b->getInput("u");
  auto bv = b->getOutput("v");

  REQUIRE(av->buf == Buffer{});

  REQUIRE(bu->buf == Buffer{});
  REQUIRE(bv->buf == Buffer{});

  t->prepare(10, 30, false);

  REQUIRE(av->buf == Buffer{
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0});

  REQUIRE(bu->buf == Buffer{
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0});
  REQUIRE(bv->buf == Buffer{
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                         0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0});

  t->step();

  REQUIRE(av->buf == Buffer{
                         1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
                         1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
                         1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0});

  REQUIRE(bu->buf == Buffer{
                         1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
                         1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
                         1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0});
  REQUIRE(bv->buf == Buffer{
                         1.0, 0.0, 0.0, 0.0, 0.0, -2.0, 0.0, 0.0, 0.0, 0.0,
                         2.0, 0.0, 0.0, 0.0, 0.0, -2.0, 0.0, 0.0, 0.0, 0.0,
                         2.0, 0.0, 0.0, 0.0, 0.0, -2.0, 0.0, 0.0, 0.0, 0.0});
}
