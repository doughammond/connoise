/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <modules.h>
#include <system.h>
#include <transport.h>

using namespace connoise::modules;
using Buffer = connoise::Buffer;

/**
 *
 * 1.0 ----------\     1.0 ----\
 * in_a{} -> [Multiply] -> [Multiply] ----\
 *               |                        |
 *    0.5 -> [Multiply] ------------> [Multiply] -> out_d{}
 */
TEST_CASE("System with parallel connected modules - Multiply->[Multiply->Multiply]->Multiply", "[System][Module][Multiply]")
{
  auto s = std::make_shared<connoise::System>();
  auto t = std::make_shared<connoise::Transport>(s);

  auto a = s->createModule(0, "Multiply", {{"b", 1.0}});
  auto b = s->createModule(1, "Multiply", {{"b", 0.5}});
  auto c = s->createModule(2, "Multiply", {{"b", 1.0}});
  auto d = s->createModule(3, "Multiply");

  // e is irrelevant in this test, just being used to pull data through d
  auto e = s->createModule(4, "NullOutput");

  REQUIRE(a);
  REQUIRE(b);
  REQUIRE(c);
  REQUIRE(d);
  REQUIRE(e);

  s->createConnection(a, "v", b, "a");
  s->createConnection(a, "v", c, "a");
  s->createConnection(b, "v", d, "a");
  s->createConnection(c, "v", d, "b");
  s->createConnection(d, "v", e, "u");

  auto aa = a->getInput("a");
  auto ab = a->getInput("b");
  auto av = a->getOutput("v");

  auto ba = b->getInput("a");
  auto bb = b->getInput("b");
  auto bv = b->getOutput("v");

  auto ca = c->getInput("a");
  auto cb = c->getInput("b");
  auto cv = c->getOutput("v");

  auto da = d->getInput("a");
  auto db = d->getInput("b");
  auto dv = d->getOutput("v");

  t->prepare(1, 5, false);

  REQUIRE(aa->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(ab->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(av->buf == Buffer{0, 0, 0, 0, 0});

  REQUIRE(ba->buf == Buffer{1.0, 1.0, 1.0, 1.0, 1.0});
  REQUIRE(bb->buf == Buffer{0.5, 0.5, 0.5, 0.5, 0.5});
  REQUIRE(bv->buf == Buffer{0.0, 0.0, 0.0, 0.0, 0.0});

  REQUIRE(ca->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(cb->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(cv->buf == Buffer{0, 0, 0, 0, 0});

  REQUIRE(da->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(db->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(dv->buf == Buffer{0, 0, 0, 0, 0});

  aa->buf = {0, 1, 2, 3, 4};

  t->step();

  REQUIRE(aa->buf == Buffer{0, 1, 2, 3, 4});
  REQUIRE(ab->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(av->buf == Buffer{0, 1, 2, 3, 4});

  REQUIRE(ba->buf == Buffer{0.0, 1.0, 2.0, 3.0, 4.0});
  REQUIRE(bb->buf == Buffer{0.5, 0.5, 0.5, 0.5, 0.5});
  REQUIRE(bv->buf == Buffer{0.0, 0.5, 1.0, 1.5, 2.0});

  REQUIRE(ca->buf == Buffer{0, 1, 2, 3, 4});
  REQUIRE(cb->buf == Buffer{1, 1, 1, 1, 1});
  REQUIRE(cv->buf == Buffer{0, 1, 2, 3, 4});

  REQUIRE(da->buf == Buffer{0.0, 0.5, 1.0, 1.5, 2.0});
  REQUIRE(db->buf == Buffer{0.0, 1.0, 2.0, 3.0, 4.0});
  REQUIRE(dv->buf == Buffer{0.0, 0.5, 2.0, 4.5, 8.0});
}
