/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <system.h>

TEST_CASE("System create module then remove it", "[System]")
{
  connoise::System s;

  auto a = s.createModule(0, "Add");
  auto modules = s.getModules();
  REQUIRE(modules.size() == 1);

  s.deleteModule(a->id());
  modules = s.getModules();
  REQUIRE(modules.size() == 0);
}

TEST_CASE("System create connected modules then remove one", "[System]")
{
  connoise::System s;

  auto a = s.createModule(0, "Add");
  auto b = s.createModule(1, "NullOutput");
  s.createConnection(a, "v", b, "u");

  auto modules = s.getModules();
  REQUIRE(modules.size() == 2);
  auto cx = s.getConnections();
  REQUIRE(cx.size() == 1);

  s.deleteModule(a->id());
  modules = s.getModules();
  REQUIRE(modules.size() == 1);
  cx = s.getConnections();
  REQUIRE(cx.size() == 0);
}

// TODO: test deleting just connections
