/* SPDX-License-Identifier: GPL-3.0-only */
/* Copyright © 2020 Doug Hammond */

#include <catch.hpp>

#include <util.h>

using namespace connoise::util;

TEST_CASE("lerp - equal range, at start", "[util][lerp]")
{
  const auto v = lerp(0, 1, 0, 1, 0);
  REQUIRE(v == 0);
}
TEST_CASE("lerp - equal range, in middle", "[util][lerp]")
{
  const auto v = lerp(0, 1, 0, 1, 0.5);
  REQUIRE(v == 0.5);
}
TEST_CASE("lerp - equal range, at end", "[util][lerp]")
{
  const auto v = lerp(0, 1, 0, 1, 1.0);
  REQUIRE(v == 1.0);
}

TEST_CASE("lerp - offset src range, at start", "[util][lerp]")
{
  const auto v = lerp(0, 1, 1, 2, 1.0);
  REQUIRE(v == 0.0);
}
TEST_CASE("lerp - offset src range, in middle", "[util][lerp]")
{
  const auto v = lerp(0, 1, 1, 2, 1.5);
  REQUIRE(v == 0.5);
}
TEST_CASE("lerp - offset src range, at end", "[util][lerp]")
{
  const auto v = lerp(0, 1, 1, 2, 2.0);
  REQUIRE(v == 1.0);
}

TEST_CASE("lerp - offset dst range, at start", "[util][lerp]")
{
  const auto v = lerp(1, 2, 0, 1, 0);
  REQUIRE(v == 1.0);
}
TEST_CASE("lerp - offset dst range, in middle", "[util][lerp]")
{
  const auto v = lerp(1, 2, 0, 1, 0.5);
  REQUIRE(v == 1.5);
}
TEST_CASE("lerp - offset dst range, at end", "[util][lerp]")
{
  const auto v = lerp(1, 2, 0, 1, 1.0);
  REQUIRE(v == 2.0);
}

TEST_CASE("lerp - offset both range, at start", "[util][lerp]")
{
  const auto v = lerp(1, 2, 3, 4, 3);
  REQUIRE(v == 1.0);
}
TEST_CASE("lerp - offset both range, in middle", "[util][lerp]")
{
  const auto v = lerp(1, 2, 3, 4, 3.5);
  REQUIRE(v == 1.5);
}
TEST_CASE("lerp - offset both range, at end", "[util][lerp]")
{
  const auto v = lerp(1, 2, 3, 4, 4.0);
  REQUIRE(v == 2.0);
}

TEST_CASE("lerp - inverted dst range, at start", "[util][lerp]")
{
  const auto v = lerp(-1, 0, 0, 1, 0);
  REQUIRE(v == -1.0);
}
TEST_CASE("lerp - inverted dst range, in middle", "[util][lerp]")
{
  const auto v = lerp(-1, 0, 0, 1, 0.5);
  REQUIRE(v == -0.5);
}
TEST_CASE("lerp - inverted dst range, at end", "[util][lerp]")
{
  const auto v = lerp(-1, 0, 0, 1, 1);
  REQUIRE(v == 0.0);
}